package white_box_test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import app.OperationNotAllowedException;
import app.PlannerApp;
import domain.Project;
import persistence.InMemoryRepository;

public class AddActivityWBT {
    /* Written
    * Oscar J�rgensen s173470
    */
	private PlannerApp plannerApp;
	private Project project;
	private InMemoryRepository repo = new InMemoryRepository();
	
	//precondition
	public AddActivityWBT() {
		try {
			plannerApp = new PlannerApp(repo, repo);
			plannerApp.adminLogin();
			plannerApp.newProject("TEST");
			project = plannerApp.getProjectsFromName("TEST").get(0);
			project.addNewActivity("Holiday");
		} catch(OperationNotAllowedException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = Exception.class)
	public void testInputDataSetA1() {
		project.addNewActivity("Holiday");
	}
	
	@Test
	public void testInputDataSetB() {
		project.addNewActivity("Implement add Activity feature");
		assertEquals(project.getActivityFromName("Implement add Activity feature"), project.getActivities().get(1));
	}
}
