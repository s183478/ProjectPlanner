package white_box_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import app.OperationNotAllowedException;
import app.PlannerApp;
import domain.Activity;
import domain.Project;
import persistence.InMemoryRepository;

public class AssignEmployeeToActivityWBT {
	/* Written
	* Oscar J�rgensen s173470
	*/
	private PlannerApp plannerApp;
	private Project project;
	private InMemoryRepository repo = new InMemoryRepository();
	private Activity activity;
	
	public AssignEmployeeToActivityWBT() {
		try {
			plannerApp = new PlannerApp(repo, repo);
			plannerApp.adminLogin();
			plannerApp.newProject("TEST");
			project = plannerApp.getProjectsFromName("TEST").get(0);
			project.addNewActivity("Create test");
			plannerApp.registerEmployee("NAWE");
			plannerApp.registerEmployee("LEAD");
			plannerApp.appointProjectLeader(project, plannerApp.getEmployeeFromId("LEAD"));
			activity = project.getActivities().get(0);
			activity.assignEmployee(plannerApp.getEmployeeFromId("NAWE"));
			plannerApp.adminLogout();
		} catch (OperationNotAllowedException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testInputDataSetA() {
		try {
			activity.assignEmployee(plannerApp.getEmployeeFromId("NAWE"));
			assertTrue(false);
		} catch (OperationNotAllowedException e) {
			assertEquals(e.getMessage(), "Employee is already assigned to this activity");
		}
	}
	
	@Test
	public void testInputDataSetB() {
		try {
			activity.assignEmployee(plannerApp.getEmployeeFromId("LEAD"));
			assertEquals(activity.getEmployeeFromID("LEAD"), plannerApp.getEmployeeFromId("LEAD"));
		} catch (OperationNotAllowedException e) {
			assertTrue(false);
		}
	}
}