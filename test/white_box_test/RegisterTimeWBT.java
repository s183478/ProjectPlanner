package white_box_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import app.DateTime;
import app.Interval;
import app.PlannerApp;
import domain.Activity;
import domain.Employee;
import domain.Project;
import persistence.InMemoryRepository;

public class RegisterTimeWBT {
	/* Written
	* Oscar J�rgensen s173470
	*/
	private PlannerApp plannerApp;
	private Project project;
	private InMemoryRepository repo = new InMemoryRepository();
	private Activity activity;
	private Interval interval;
	Employee TCM;
	
	public RegisterTimeWBT() {
		try {
			plannerApp = new PlannerApp(repo, repo);
			plannerApp.adminLogin();
			plannerApp.newProject("TEST");
			plannerApp.registerEmployee("ARAM");
			plannerApp.registerEmployee("TCM");
			plannerApp.registerEmployee("RP");
			project = plannerApp.getProjectsFromName("TEST").get(0);
			project.addNewActivity("Test Activity");
			activity = project.getActivityFromName("Test Activity");
			activity.assignEmployee(plannerApp.getEmployeeFromId("ARAM"));
			activity.assignEmployee(plannerApp.getEmployeeFromId("TCM"));
			DateTime startTime = new DateTime.Builder()
					.year(2020)
					.week(5)
					.dayOfWeek(1)
					.hour(8)
					.minute(0)
					.build();
			DateTime endTime = new DateTime.Builder()
					.year(2020)
					.week(5)
					.dayOfWeek(1)
					.hour(14)
					.minute(0)
					.build();
			interval = new Interval(startTime, endTime);
			plannerApp.getEmployeeFromId("ARAM").registerTime(activity, interval);
		} catch(Exception e) {
			System.out.println(e);
		}
	}
	
	@Test
	public void testInputDataSetA() {
			try {
				plannerApp.getEmployeeFromId("RP").registerTime(activity, interval);
				assertTrue(false);
			} catch (Exception e) {
				assertEquals(e.getMessage(), "You cannot register time on an activity which you are not registered to");
			}
	}
	
	@Test
	public void testInputDataSetB() {
			try {
				DateTime startTime = new DateTime.Builder()
						.year(2020)
						.week(5)
						.dayOfWeek(1)
						.hour(8)
						.minute(0)
						.build();
				DateTime endTime = new DateTime.Builder()
						.year(2020)
						.week(5)
						.dayOfWeek(1)
						.hour(20)
						.minute(0)
						.build();
				Interval newInterval = new Interval(startTime, endTime);
				plannerApp.getEmployeeFromId("TCM").registerTime(activity, newInterval);
				assertTrue(false);
			} catch (Exception e) {
				assertEquals(e.getMessage(), "You cannot register more than 12 hours at once. Please split up the registration in smaller parts.");
			}
	}
	
	@Test
	public void testInputDataSetC() {
		try {
			plannerApp.getEmployeeFromId("ARAM").registerTime(activity, interval);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "You have already registered time spent on another activity during this timeslot.");
		}
	}
	
	@Test
	public void testInputDataSetD() {
		try {
			DateTime startTime = new DateTime.Builder()
					.year(2020)
					.week(5)
					.dayOfWeek(1)
					.hour(15)
					.minute(0)
					.build();
			DateTime endTime = new DateTime.Builder()
					.year(2020)
					.week(5)
					.dayOfWeek(1)
					.hour(17)
					.minute(0)
					.build();
			Interval newInterval = new Interval(startTime, endTime);
			plannerApp.getEmployeeFromId("ARAM").registerTime(activity, newInterval);
			assertEquals(plannerApp.getEmployeeFromId("ARAM").getRegisteredTimeForActivity(activity).get(1), newInterval);
		} catch (Exception e) {
			assertTrue(false);
		}
		
	}
	
	@Test
	public void testInputDataSetE() {
		try {
			plannerApp.getEmployeeFromId("TCM").registerTime(activity, interval);
			assertEquals(plannerApp.getEmployeeFromId("TCM").getRegisteredTimeForActivity(activity).get(0), interval);
		} catch (Exception e) {
			assertTrue(false);
		}
	}
	
}