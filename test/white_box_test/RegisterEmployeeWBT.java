package white_box_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import app.OperationNotAllowedException;
import app.PlannerApp;
import persistence.InMemoryRepository;

public class RegisterEmployeeWBT {
	/* Written
	*  Oscar J�rgensen s173470
	*/
	private PlannerApp plannerApp;
	private InMemoryRepository repo = new InMemoryRepository();
	
	public RegisterEmployeeWBT() {
		try {
			plannerApp = new PlannerApp(repo, repo);
			plannerApp.adminLogin();
			plannerApp.registerEmployee("EM");
			plannerApp.adminLogout();
		} catch (OperationNotAllowedException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testInputDataSetA() {
		try {
			plannerApp.registerEmployee("NAWE");
			assertTrue(false);
		} catch (OperationNotAllowedException e) {
			assertEquals(e.getMessage(), "Administrator login required");
		}
	}
	
	@Test
	public void testInputDataSetB() {
		try {
			plannerApp.adminLogin();
			plannerApp.registerEmployee("EM");
			assertTrue(false);
		} catch (OperationNotAllowedException e) {
			assertEquals(e.getMessage(), "The employee is already registered");
		}
		
	}
	
	@Test
	public void testInputDataSetC() {
		try {
			plannerApp.adminLogin();
			plannerApp.registerEmployee("NAWE");
			assertEquals(plannerApp.getEmployeeFromId("NAWE").getId(), "NAWE");
		} catch (OperationNotAllowedException e) {
			e.printStackTrace();
		}
		
	}

}