package white_box_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import app.OperationNotAllowedException;
import app.PlannerApp;
import domain.Employee;
import domain.Project;
import persistence.InMemoryRepository;

public class AppointProjectLeaderWBT {
	/* Written
	* Oscar J�rgensen s173470
	*/
	private PlannerApp plannerApp;
	private Project project;;
	private InMemoryRepository repo = new InMemoryRepository();
	private Employee emptyEmployee;

	public AppointProjectLeaderWBT() {
		try {
			plannerApp = new PlannerApp(repo, repo);
			plannerApp.adminLogin();
			plannerApp.registerEmployee("NAWE");
			plannerApp.newProject("TEST");
			project = plannerApp.getProjectsFromName("TEST").get(0);
			emptyEmployee = new Employee("ARAM");
			plannerApp.adminLogout();
		} catch (OperationNotAllowedException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testInputDataSetA() {
		try {
			plannerApp.appointProjectLeader(project, plannerApp.getEmployeeFromId("NAWE"));
			assertTrue(false);
		} catch (OperationNotAllowedException e) {
			assertEquals(e.getMessage(), "Administrator login required");
		}
	}
	
	@Test
	public void testInputDataSetB() {
		try {	
			plannerApp.adminLogin();
			plannerApp.appointProjectLeader(project, emptyEmployee);
			assertTrue(false);
		} catch (OperationNotAllowedException e) {
			assertEquals(e.getMessage(), "Employee does not exist!");
		}
	}
	
	@Test
	public void testInputDataSetC() {
		try {
			plannerApp.adminLogin();
			plannerApp.appointProjectLeader(project, plannerApp.getEmployeeFromId("NAWE"));
			assertEquals(project.getProjectLeader(), plannerApp.getEmployeeFromId("NAWE"));
		} catch(OperationNotAllowedException e) {
			assertTrue(false);
		}
	}
}