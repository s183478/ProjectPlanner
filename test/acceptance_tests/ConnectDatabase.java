package acceptance_tests;

import app.PlannerApp;
import io.cucumber.java.Before;
import persistence.InMemoryRepository;

public class ConnectDatabase {
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */
	PlannerApp plannerApp;
	
	public ConnectDatabase(PlannerApp plannerApp) {
		this.plannerApp = plannerApp;
	}
	
	@Before
	public void connectDatabase() {
		InMemoryRepository repo = new InMemoryRepository();
		plannerApp.setRepositories(repo, repo);
		plannerApp.clearDatabase();
	}
}
