package acceptance_tests;

import app.DateServer;
import app.PlannerApp;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class MockDateHolder {
	/*
	 * Written by:
	 * 
	 */

	DateServer dateServer = mock(DateServer.class);
	
	public MockDateHolder(PlannerApp plannerApp) {
		GregorianCalendar calendar = new GregorianCalendar();
		setDate(calendar);
		plannerApp.setDateServer(dateServer);
	}

	public void setDate(Calendar calendar) {
		Calendar c = new GregorianCalendar(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
		when(this.dateServer.getDate()).thenReturn(c);
	}

}
