package acceptance_tests;

import persistence.*;
import java.util.Calendar;

import domain.Activity;
import domain.Employee;
import domain.Project;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import app.*;
import static org.junit.Assert.*;

public class CheckAvailabilitySteps {
    
    /* Written by:
    / L.Noer s15590
    /
    */
	
	private PlannerApp plannerApp;
	private ErrorMessageHolder errorMessageHolder;
	private Project project;
	private Employee employee;
	
	public CheckAvailabilitySteps(PlannerApp plannerApp, ErrorMessageHolder errorMessageHolder) {
		this.plannerApp = plannerApp;
		this.errorMessageHolder = errorMessageHolder;
	}
	
	@Given("that the list of employees is not empty")
	public void thatTheListOfEmployeesIsNotEmpty() throws OperationNotAllowedException {
		plannerApp.adminLogin();
		employee = plannerApp.registerEmployee("JON");
	    assertTrue(plannerApp.getEmployeeRepository().getNumberOfEmployees() != 0);
	}
	

	@Given("some are available")
	public void someAreAvailable() {
	    assertTrue(plannerApp.checkForAvailableWorkers().size() != 0);
	}

	@When("the project leader {string} checks for available workers")
	public void theProjectLeaderChecksForAvailableWorkers(String string1) throws OperationNotAllowedException {
		plannerApp.adminLogin();
		project = plannerApp.newProject("Test Project");
		
		employee = plannerApp.registerEmployee(string1);
		project.appointLeader(employee);
		assertTrue(project.getProjectLeader().getId() == string1);
	    plannerApp.checkForAvailableWorkers();
	}

	@Then("a list of available employees is returned")
	public void aListOfAvailableEmployeesIsReturned() {
	   assertTrue( plannerApp.checkForAvailableWorkers().contains(employee) );
	}

}
