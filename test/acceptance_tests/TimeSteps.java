package acceptance_tests;

import java.util.Calendar;
import java.util.GregorianCalendar;

import io.cucumber.java.en.Given;

public class TimeSteps {
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */
	
	MockDateHolder dateHolder;

	
	public TimeSteps(MockDateHolder dateHolder) {
		this.dateHolder = dateHolder;
	}
	
	
	@Given("the date is {int}-{int}-{int}")
	public void theDateIs(Integer day, Integer month, Integer year) {
		Calendar cal = new GregorianCalendar(year, month-1, day);
	    dateHolder.setDate(cal);
	}
	
}
