package acceptance_tests;

import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.GregorianCalendar;

import app.OperationNotAllowedException;
import app.PlannerApp;
import domain.Activity;
import domain.Employee;
import domain.Project;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ProjectSteps {
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */
	
	private Project project;
	private PlannerApp plannerApp;
	private ErrorMessageHolder errorMessageHolder;
	private MockDateHolder dateHolder;

	
	public ProjectSteps(PlannerApp plannerApp, ErrorMessageHolder errorMessageHolder, MockDateHolder dateHolder) {
		this.plannerApp = plannerApp;
		this.errorMessageHolder = errorMessageHolder;
		this.dateHolder = dateHolder;
	}
		
	
	@Given("that a project exists")
	public void thatAProjectExists() {
		aProjectWithNameAndIdIsRegisteredInThePlanner("Example Project", "010101");
	}
	
	
	@Given("a project with name {string} and id {string} is registered in the planner")
	public void aProjectWithNameAndIdIsRegisteredInThePlanner(String name, String id) {
	    plannerApp.adminLogin();    
	    int month = Integer.parseInt(id.substring(0, 2));
	    int year =  Integer.parseInt("20" + id.substring(2, 4));
	    theDateIs(1, month, year);
	    theUserCreatesANewProjectWithName(name);
	    project = plannerApp.getProjectsFromName(name).get(0);
	    plannerApp.adminLogout();
	}
	
	public void theDateIs(Integer day, Integer month, Integer year) {
		Calendar cal = new GregorianCalendar(year, month-1, day);
	    dateHolder.setDate(cal);
	}
	
	
	
	@Given("that the project with id {string} has a registered activity with name {string}")
	public void thatTheProjectWithIdHasARegisteredActivityWithName(String projectId, String activityName) {
	    project = plannerApp.getProjectFromId(projectId);
	    project.addNewActivity(activityName);	    
	}


	@When("the user creates a new Project with name {string}")
	public void theUserCreatesANewProjectWithName(String projectName) {
		try {
			plannerApp.newProject(projectName);
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}
	
	
	@When("the user creates a new Project with no name")
	public void theUserCreatesANewProjectWithNoName() throws OperationNotAllowedException {
			plannerApp.newProject();
	}
	
	
	@When("the user appoints the employee with id {string} as project leader")
	public void theUserAppointsTheEmployeeWithIdAsProjectLeader(String id) throws OperationNotAllowedException {
	    try {
	    	Employee e = new Employee(id);
			plannerApp.appointProjectLeader(project, e);
	    } catch	( OperationNotAllowedException e1) {
	    	errorMessageHolder.setErrorMessage(e1.getMessage());
	    }
	}

	
	@Then("a new project with id {string} is created")
	public void aNewProjectWithIdIsCreated(String projectId) {
		project = plannerApp.getProjectFromId(projectId);
		assertTrue(project.getId().equals(projectId));
	}

	
	@Then("a new project with name {string} and id {string} is created")
	public void aNewProjectWithNameAndIdIsCreated(String name, String projectId) {
		project = plannerApp.getProjectFromId(projectId);
		assertTrue(project.getName().equals(name));
		assertTrue(project.getId().equals(projectId));
	}
	
	@Then("the employee with id {string} is appointed as project leader")
	public void theEmployeeWithIdIsAppointedAsProjectLeader(String employeeId) {
	    assertTrue(project.getProjectLeader().getId().equals(employeeId));
	}

}
