package acceptance_tests;

import static org.junit.Assert.assertTrue;

import io.cucumber.java.en.Then;

public class ErrorMessageSteps {
	/*
	 * Written by:
	 * 
	 */

	private ErrorMessageHolder errorMessageHolder;
	
	public ErrorMessageSteps(ErrorMessageHolder errorMessageHolder) {
		this.errorMessageHolder = errorMessageHolder;
	}
	
	@Then("the error message {string} is given")
	public void theErrorMessageIsGiven(String errMsg) {
	    assertTrue(errorMessageHolder.getErrorMessage().equals(errMsg));
	}
	
}
