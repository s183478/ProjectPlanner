package acceptance_tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import app.OperationNotAllowedException;
import app.PlannerApp;
import domain.Employee;
import domain.Project;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class EmployeeSteps {
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */

	private PlannerApp plannerApp;
	private Project project;
	private Employee employee;
	private ErrorMessageHolder errorMessageHolder;
	
	
	public EmployeeSteps(ErrorMessageHolder errorMessageHolder, PlannerApp plannerApp) {
		this.errorMessageHolder = errorMessageHolder;
		this.plannerApp = plannerApp;
	}
	
	
	@Given("an employee with id {string} is already registered")
	public void anEmployeeWithIdIsAlreadyRegistered(String id) {
	    plannerApp.adminLogin();
	    theUserRegistersAnEmployeeWithId(id);
	    plannerApp.adminLogout();
	}
	
	@Given("an employee with id {string} is not registered")
	public void anEmployeeWithIdIsNotRegistered(String id) {
		employee = plannerApp.getEmployeeFromId(id);
		assertTrue(employee == null);
	}
	
	@Given("that an employee {string} who is also a project leader for the project with id {string} is logged in")
	public void thatAnEmployeeWhoIsAlsoAProjectLeaderForTheProjectWithIdIsLoggedIn(String employeeId, String projectId) {
	    project = plannerApp.getProjectFromId(projectId);
	    Employee leader;
		try {
			leader = new Employee(employeeId);
		    project.appointLeader(leader);
		    plannerApp.employeeLogin(leader);
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}
	
		
	@Given("that an employee {string} who is not the project leader for the project with id {string} is logged in")
	public void thatAnEmployeeWhoIsNotTheProjectLeaderForTheProjectWithIdIsLoggedIn(String employeeId, String projectId) {
		Employee newEmployee;
		try {
			newEmployee = new Employee(employeeId);
			plannerApp.employeeLogin(newEmployee);
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
		
		project = plannerApp.getProjectFromId(projectId);
	    assertFalse(employeeId.equals(project.getProjectLeader()));
	}

	
	@When("the user registers an employee with id {string}")
	public void theUserRegistersAnEmployeeWithId(String id) {
	    try {
			plannerApp.registerEmployee(id);
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}


	@Then("the employee with id {string} is registered")
	public void theEmployeeWithIdIsRegistered(String id) {
		employee = plannerApp.getEmployeeFromId(id);
		assertTrue(employee != null);
	}
	
}
