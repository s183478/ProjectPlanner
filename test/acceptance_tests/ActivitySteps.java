package acceptance_tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertEquals;

import app.DateTime;
import app.Interval;
import app.OperationNotAllowedException;
import app.PlannerApp;
import domain.Project;
import domain.Activity;
import domain.Employee;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ActivitySteps {
    
	/*
	 * Written by:
	 * s190187, Mads Kring Jakobsen
	 * s173906, Jonas Weile
	 */

	private Activity activity;
	private Employee employee;
	private PlannerApp planner;
	private Project project;
	private ErrorMessageHolder errorMessageHolder;
	
	
	public ActivitySteps(PlannerApp planner, ErrorMessageHolder errorMessageHolder) {
		this.planner = planner;
		this.errorMessageHolder = errorMessageHolder;
	}
	
	@Given("that there is a project with an assigned project leader")
	public void that_there_is_a_project_with_an_assigned_project_leader() {
		
		try {
			planner.adminLogin();
			project = planner.newProject("Test Project");
			employee = planner.registerEmployee("NAWE");
			project.appointLeader(employee);
			planner.adminLogout();
			planner.employeeLogin(employee);
			
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}
	
	@Given("that the allocated hours for the activity {string} in project with id {string} is {int} hours")
	public void thatTheAllocatedHoursForTheActivityInProjectWithIdIsHours(String activityName, String projectId, Integer expectedHours) {
	    project = planner.getProjectFromId(projectId);
	    activity = project.getActivityFromName(activityName);
	    
	    try {
	    	activity.setExpectedTimeUsage(expectedHours);
	    } catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	    
	}
	
	@Given("that the start and end week for the activity {string} in project with id {string} is {int} and {int}")
	public void thatTheStartAndEndWeekForTheActivityInProjectWithIdIsAnd(String activityName, String projectId, Integer startWeek, Integer endWeek) {
		project = planner.getProjectFromId(projectId);
	    activity = project.getActivityFromName(activityName);
	    
	    DateTime start = null;
		try {
			start = new DateTime.Builder()
											.week(startWeek)
											.build();
		} catch (OperationNotAllowedException e1) {
			errorMessageHolder.setErrorMessage(e1.getMessage());
		}
	   
	    DateTime end = null;
		try {
			end = new DateTime.Builder()
										.week(endWeek)
										.build();
		} catch (OperationNotAllowedException e1) {
			errorMessageHolder.setErrorMessage(e1.getMessage());
		}
	    
	    
	    try {
	    	activity.setRunningTime(start, end);
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}
	
	@When("the user updates the allocated hours for the activity to {int} hours")
	public void theUserUpdatesTheAllocatedHoursForTheActivityToHours(Integer hours) {
	    try {
	    	activity.setExpectedTimeUsage(hours);
	    } catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}
	
	@When("the user updates the start and end week to {int} and {int}")
	public void theUserUpdatesTheStartAndEndWeekToAnd(Integer startWeek, Integer endWeek) {
		
		try {
			DateTime start = new DateTime.Builder()
										.week(startWeek)
										.build();
			
			DateTime end = new DateTime.Builder()
										.week(endWeek)
										.build();
	
			Interval interval = new Interval(start, end);
	    	planner.changeActivityInterval(activity, interval);
	 	} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}
	
	
	@When("the project leader adds an activity with name {string}")
	public void theProjectLeaderAddsAnActivityWithName(String activityName) {
		assertEquals(this.employee, this.project.getProjectLeader());
		
		try {
			project.addNewActivity(activityName);
		} catch (IllegalArgumentException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}

	
	@Then("the allocated hours for the activity is updated to {int} hours")
	public void theAllocatedHoursForTheActivityIsUpdatedToHours(Integer hours) {
	    Integer expectedHours = activity.getExpectedTimeUsage();   
	    assertEquals(expectedHours, hours);	    
	}
	
	@Then("the start and end week are updated to {int} and {int}")
	public void theStartAndEndWeekAreUpdatedToAnd(int startWeek, int endWeek) {
	    assertEquals(activity.getRunningTime().getStartTime().getWeek(), startWeek);
	    assertEquals(activity.getRunningTime().getEndTime().getWeek(), endWeek);
	}
	
	@Then("the activity is added to the project")
	public void the_activity_is_added_to_the_project() {
	    assertEquals(project.getActivities().size(),1);
	}
	
	@Given("there is an activity called {string}")
	public void thereIsAnActivityCalled(String activityName) {
		try {
			planner.newProject("Test Project");
			project = planner.getProjectsFromName("Test Project").get(0);
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
		
		project.addNewActivity(activityName);
		activity = project.getActivityFromName(activityName);
		activity.setType("Example Type");
	}
}
