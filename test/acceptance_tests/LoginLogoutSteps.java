package acceptance_tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import app.PlannerApp;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginLogoutSteps {
	/*
	 * Written by:
	 * 
	 */

	private PlannerApp planner;
	
	public LoginLogoutSteps(PlannerApp planner) {
		this.planner = planner;
	}

	
	
	@Given("the administrator is logged in")
	public void theAdministratorIsLoggedIn() {
	    planner.adminLogin();
	}
	
	
	@Given("the administrator is not logged in")
	public void theAdministratorIsNotLoggedIn() {
	    planner.adminLogout();
	}
}
