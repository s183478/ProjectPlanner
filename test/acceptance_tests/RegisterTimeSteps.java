package acceptance_tests;

import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import app.DateTime;
import app.Interval;
import app.OperationNotAllowedException;
import app.PlannerApp;
import domain.Activity;
import domain.Employee;
import domain.Project;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class RegisterTimeSteps {
	/*
	 * Written by:
	 * Oscar J�rgensen s173470
	 * s173906, Jonas Weile
	 */
	private PlannerApp plannerApp;
	private ErrorMessageHolder errorMessageHolder;
	
	private Activity activity = new Activity("Test Activity", new Project("Test Project", Calendar.getInstance(), 0));
	private Employee employee;
	private DateTime date;
	private Interval interval;
	

	public RegisterTimeSteps(PlannerApp plannerApp, ErrorMessageHolder errorMessageHolder) {
		this.plannerApp = plannerApp;
		this.errorMessageHolder = errorMessageHolder;
	}

	@Given("the employee {string} is assigned to an activity")
	public void theEmployeeIsAssignedToAnActivity(String id) {
		try {
			employee = new Employee(id);
			activity.assignEmployee(employee);
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}
	
	@Given("employee {string} is helping {string} on an activity")
	public void employeeIsHelpingOnAnActivity(String helperID, String id) {
		try {
			employee = new Employee(helperID);
			Employee employee2 = new Employee(id);
			activity.assignEmployee(employee2);
			activity.assignHelper(employee2, employee);
		} catch (OperationNotAllowedException e) {
			e.printStackTrace();
		}
	}


	@Given("the employee {string} is not assigned to an activity")
	public void theEmployeeIsNotAssignedToAnActitivy(String id) {
		try {
			employee = new Employee(id);
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}

	@When("the user registers time spent between {int} and {int} on that activity on day {int} of week {int} of year {int}")
	public void theUserRegistersTimeSpentBetweenAndOnThatActivityOnDayOfWeekOfYear
					(int startHour, int endHour, int dayOfWeek, int week, int year) {
		try {
			this.date = new DateTime.Builder()
										.year(year)
										.week(week)
										.dayOfWeek(dayOfWeek)
										.build();
			
			DateTime startTime = new DateTime.Builder()
												.year(year)
												.week(week)
												.dayOfWeek(dayOfWeek)
												.hour(startHour)
												.minute(0)
												.build();
			
			DateTime endTime = new DateTime.Builder()
												.year(year)
												.week(week)
												.dayOfWeek(dayOfWeek)
												.hour(endHour)
												.minute(0)
												.build();
					
			Interval workingInterval = new Interval(startTime, endTime);
			this.interval = workingInterval;
			
			employee.registerTime(activity, workingInterval);
		} catch(Exception e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}

	
	@When("the user registers time spent between {int} and {int} on another activity on the same day")
	public void theUserRegistersTimeSpentAndOnAnotherActivityOnThe(int startHour, int endHour) {
		try {
			Activity activity2 = new Activity("Sample Activity", null);
			activity2.assignEmployee(employee);
			
			
			DateTime startTime = new DateTime.Builder()
												.year(date.getYear())
												.week(date.getWeek())
												.dayOfWeek(date.getDayOfWeek())
												.hour(startHour)
												.minute(0)
												.build();
			
			DateTime endTime = new DateTime.Builder()
												.year(date.getYear())
												.week(date.getWeek())
												.dayOfWeek(date.getDayOfWeek())
												.hour(endHour)
												.minute(0)
												.build();
			
			
			Interval workingInterval = new Interval(startTime, endTime);
			interval = workingInterval;
			employee.registerTime(activity2, workingInterval);
			
		} catch(Exception e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}

	@When("the user alters the registered time spent to between {int} and {int} on that day")
	public void theUserAltersTheRegisteredTimeSpentToBetweenAndOnThatDay(Integer startHour, Integer endHour) {
		try {
			DateTime startTime = new DateTime.Builder()
							.year(date.getYear())
							.week(date.getWeek())
							.dayOfWeek(date.getDayOfWeek())
							.hour(startHour)
							.minute(0)
							.build();
		
			DateTime endTime = new DateTime.Builder()
							.year(date.getYear())
							.week(date.getWeek())
							.dayOfWeek(date.getDayOfWeek())
							.hour(endHour)
							.minute(0)
							.build();
			
			
			Interval newInterval = new Interval(startTime, endTime);
			employee.alterRegisteredTime(activity, interval, newInterval);
			
			interval = newInterval;
			
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}
	
	@Then("the user is registered as having worked between {int} and {int} on that activity on the given date")
	public void theUserIsRegisteredAsHavingWorkedBetweenAndOnThatActivityOnTheGivenDate(Integer int1, Integer int2) {
		assertTrue(employee.getRegisteredTimeForActivity(activity).contains(interval));
	}

}
