package acceptance_tests;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import app.OperationNotAllowedException;
import app.PlannerApp;
import domain.Activity;
import domain.Employee;
import domain.Project;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AssignToActivitySteps {
	/*
	 * Written by:
	 * Oscar J�rgensen s173470
	 * s173006, Jonas Weile
	 */
	
	private PlannerApp plannerApp;
	private ErrorMessageHolder errorMessageHolder;
	private Activity activity;
	private Employee employee;
	private Employee helper;
	private Project project;
	
	public AssignToActivitySteps(PlannerApp plannerApp, ErrorMessageHolder errorMessageHolder) {
		this.plannerApp = plannerApp;
		this.errorMessageHolder = errorMessageHolder;
	}
	
	@Given("that a project with name {string} and activity exist")
	public void thatAProjectWithNameAndActivityExist(String projectName) {
		try {
			plannerApp.adminLogin();
			project = plannerApp.newProject(projectName);
			activity = project.addNewActivity("Test Activity");
			plannerApp.adminLogout();
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	@Given("that an administrator is logged in")
	public void thatAnAdministratorIsLoggedIn() {
		plannerApp.adminLogin();
	}
	
	@Given("that an employee {string} who is also a project leader is logged in")
	public void thatAnEmployeeWhoIsAlsoAProjectLeaderIsLoggedIn(String id) {
		try {
			plannerApp.adminLogin();
			this.employee = plannerApp.registerEmployee(id);
			plannerApp.employeeLogin(employee);
			plannerApp.appointProjectLeader(project, employee);
			plannerApp.adminLogout();
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
			System.out.println(e.getMessage());
		}
		
	}
	
	@Given("that neither the administrator nor the relevant project leader is logged in")
	public void thatNeitherTheAdministratorNorTheRelevantProjectLeaderIsLoggedIn() {
		thereIsAnEmployeeWithID("NPL");
		plannerApp.employeeLogin(employee);
		assertFalse(plannerApp.getCurrentUser().equals(project.getProjectLeader()));
		assertFalse(plannerApp.adminLoggedIn());
	}
	
	@Given("there is an employee with ID {string}")
	public void thereIsAnEmployeeWithID(String id) {
		try {
			plannerApp.adminLogin();
			employee = plannerApp.registerEmployee(id);
			plannerApp.adminLogout();
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
			System.out.println(e.getMessage());
		}
	}
	
	@Given("an employee {string} is already added to an activity")
	public void anEmployeeIsAlreadyAddedToAnActivity(String id) {
		try {
			plannerApp.adminLogin();
			employee = plannerApp.registerEmployee(id);
			plannerApp.assignEmployeeToActivity(employee, activity);
			plannerApp.adminLogout();
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
			System.out.println(e.getMessage());
		}
	}
	
	@Given("that the project leader is logged in")
	public void thatTheProjectLeaderIsLoggedIn() {
		assertFalse(employee == null || project == null);
		project.appointLeader(employee);
		plannerApp.employeeLogin(employee);
	}
	
	@Given("an employee with ID {string} is already helping on activity")
	public void anEmployeeWithIDIsAlreadyHelpingOnActivity(String id) {
		try {
			plannerApp.adminLogin();
			helper = plannerApp.registerEmployee(id);
			
			Employee leader = plannerApp.registerEmployee("LEAD");
			project.appointLeader(leader);
			plannerApp.employeeLogin(leader);
			
			plannerApp.assignEmployeeToActivity(employee, activity);
			plannerApp.assignHelperToActivity(helper, activity);
			
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
			System.out.println(e.getMessage());
		}
		
	}
	
	
	@When("the user adds the employee to an activity")
	public void theUserAddsTheEmployeeToAnActivity() {
		try {
			assertFalse(employee == null);
			assertFalse(activity == null);
			
			plannerApp.assignEmployeeToActivity(employee, activity);
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
			System.out.println(e.getMessage());
		}
	}
	
	@When("the employee adds another employee with ID {string} as a helper to an activity")
	public void theEmployeeAddsAnotherEmployeeWithIDAsAHelpterToAnActivity(String id) {
		
		try {
			plannerApp.adminLogin();
			plannerApp.registerEmployee(id);
			helper = plannerApp.getEmployeeFromId(id);
			plannerApp.adminLogout();
			plannerApp.employeeLogin(employee);
			plannerApp.assignHelperToActivity(helper, activity);
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
			System.out.println(e.getMessage());
		}
		
	}
	
	@When("the employee is added as a helper again")
	public void theEmployeeIsAddedAsAHelperAgain() {
		try {
			plannerApp.assignHelperToActivity(helper, activity);
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
			System.out.println(e.getMessage());
		}
	}
	
	@Then("the employee with ID {string} is added to the activity")
	public void theEmployeeWithIDIsAddedToTheActivity(String id) {
		assertTrue(activity.getEmployeeFromID(id) != null);
	}
	
	@Then("the employee with ID {string} is helping on the activity")
	public void theEmployeeWithIDIsHelpingOnTheActivity(String id) {
		assertEquals(activity.getHelperFromID(id), helper);
	}
}