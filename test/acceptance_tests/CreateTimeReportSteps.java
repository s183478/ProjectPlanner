package acceptance_tests;

import java.util.Calendar;

import app.OperationNotAllowedException;
import app.PlannerApp;
import domain.Activity;
import domain.Project;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

	public class CreateTimeReportSteps{
	    
	    /* Written by:
	    * L.Noer s155490
	    *
	    */

	private Project project;
	private Activity activity = new Activity("Test Activity", new Project("Test Project", Calendar.getInstance(), 0));
	private PlannerApp plannerApp;
	private ErrorMessageHolder errorMessageHolder;

	public CreateTimeReportSteps(PlannerApp plannerApp, ErrorMessageHolder errorMessageHolder) {
		this.plannerApp = plannerApp;
		this.errorMessageHolder = errorMessageHolder;
	}

	@Given("a project with id {string} and name {string} exists with a list of activities")
	public void aProjectExistsWithAListOfActivities(String id, String name) {
		try {
			plannerApp.adminLogin();
			plannerApp.newProject(name);
			plannerApp.adminLogout();
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
		this.project = plannerApp.getProjectsFromName(name).get(0);
	}

	@When("the project leader generates a report")
	public void theProjectLeaderGeneratesAReport() {
		project.createReport();
	}

	@Then("a report containing total time spent on the project is returned.")
	public void aReportContainingTotalTimeSpentOnTheProjectIsReturned() {
		project.getReport();
	}
}
