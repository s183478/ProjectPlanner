package acceptance_tests;

public class ErrorMessageHolder {
	/*
	 * Written by:
	 * 
	 */
	private String errorMessage = "";

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
