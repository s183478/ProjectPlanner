package controllers;

import presentation.EmployeeMenu;
import presentation.ProjectManagementMenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import app.DateTime;
import app.Interval;
import app.OperationNotAllowedException;
import app.PlannerApp;
import domain.Activity;
import domain.Employee;
import domain.Project;

public class EmployeeButtonController implements ActionListener {
	/*
	 * Written by:
	 * s183478, Tobias Munch
	 * s173906, Jonas Weile
	 */
	private EmployeeMenu view;
	private ProjectManagementMenu proManMenu;
	private PlannerApp plannerApp;
	private Employee loggedInEmployee;
	
	private JPanel currentActivePanel;

	private boolean projectSelected, activitySelected, employeeSelected, intervalSelected;
	private Project selectedProject;
	private Employee selectedEmployee;
	private Activity selectedActivity;
	private Interval selectedInterval;

	public EmployeeButtonController(EmployeeMenu view, PlannerApp plannerApp, Employee loggedInEmployee) {
		this.plannerApp = plannerApp;
		this.view = view;
		this.loggedInEmployee = loggedInEmployee;
		currentActivePanel = view.getPanel("introductionPanel");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.getButton("btnRegisterTime")) {
			resetAll();
			view.getPanel("projectSelectionPanel").setVisible(true);
			view.getPanel("activitySelectionPanel").setVisible(false);
			view.getPanel("editRegisteredTimeTextfieldPanel").setVisible(false);
			view.getPanel("registerTimePanel").setVisible(true);
			currentActivePanel = view.getPanel("registerTimePanel");
			
		}
		else if (e.getSource() == view.getButton("btnEditRegisteredTime")) {
			resetAll();
			view.getPanel("projectSelectionPanel").setVisible(true);
			view.getPanel("activitySelectionPanel").setVisible(false);
			view.getPanel("intervalSelectionPanel").setVisible(false);
			view.getPanel("editRegisteredTimeTextfieldPanel").setVisible(false);
			view.getPanel("editRegisteredTimePanel").setVisible(true);
			currentActivePanel = view.getPanel("editRegisteredTimePanel");
			
		}
		else if (e.getSource() == view.getButton("btnAskForHelp")) {
			resetAll();
			view.getPanel("projectSelectionPanel").setVisible(true);
			view.getPanel("activitySelectionPanel").setVisible(false);
			view.getPanel("editRegisteredTimeTextfieldPanel").setVisible(false);
			view.getPanel("askForHelpPanel").setVisible(true);
			currentActivePanel = view.getPanel("askForHelpPanel");
		}
		else if (e.getSource() == view.getButton("btnManageProject")) {
			resetAll();
			view.getPanel("projectSelectionPanel").setVisible(true);
			view.getPanel("activitySelectionPanel").setVisible(false);
			view.getPanel("editRegisteredTimeTextfieldPanel").setVisible(false);
			view.getPanel("manageProjectPanel").setVisible(true);
			currentActivePanel = view.getPanel("manageProjectPanel");
		}
		else if (e.getSource() == view.getButton("btnGetPersonalTimecard")) {
			resetAll();
			view.getPanel("getPersonalTimecardPanel").setVisible(true);
			view.getPanel("timecardPanel").setVisible(true);
			currentActivePanel = view.getPanel("getPersonalTimecardPanel");
		}
		else if (e.getSource() == view.getButton("btnLogout")) {
			plannerApp.employeeLogOut();
			resetAll();
			JOptionPane.showMessageDialog(view.getFrame("Employee"), "Logout succesful");
			view.getFrame("Main").setVisible(true);
			view.getFrame("Employee").setVisible(false);
			currentActivePanel = view.getPanel("introductionPanel");
		}
		else if (e.getSource() == view.getButton("btnConfirmMPP")) {
			Project project = view.getProjectList().getChosenProject();
			if (project.getProjectLeader().getId().equals(loggedInEmployee.getId())) {
				proManMenu = new ProjectManagementMenu(view.getFrame("Employee"), plannerApp, loggedInEmployee, project, false);
				proManMenu.getFrame("ProMan").setVisible(true);
				view.getFrame("Employee").setVisible(false);							
			} else {
				JOptionPane.showMessageDialog(view.getFrame("Employee"), "Error: You are not the project leader of this project");
			}
		}
		else if (e.getSource() == view.getButton("btnConfirmRTP")) {
			if (!projectSelected) {
				selectedProject = view.getProjectList().getChosenProject();
				if (selectedProject == null) { return; }
				
				projectSelected = true;
				view.getActivityList().filterForProject(plannerApp, selectedProject);
				view.getPanel("projectSelectionPanel").setVisible(false);
				view.getPanel("activitySelectionPanel").setVisible(true);
				view.getLabel("lblExplainationRTP").setText("Please select an activity");
				
			}
			else if (!activitySelected) {
				selectedActivity = view.getActivityList().getChosenActivity();
				if (selectedActivity == null) { return; }
				
				activitySelected = true;
				view.getPanel("activitySelectionPanel").setVisible(false);
				view.getPanel("editRegisteredTimeTextfieldPanel").setVisible(true);
				view.getButton("btnConfirmRTP").setText("Confirm");
				view.getLabel("lblExplainationRTP").setText("Please input the interval to register");
			}
			else {
				Interval i = checkValidInputsForEditRegisteredTime();
				if (i != null && this.parseRegisteredTime(i) ) {
					JOptionPane.showMessageDialog(view.getFrame("Employee"), "Time succesfully registered");
				}
			}
		}
		else if (e.getSource() == view.getButton("btnGoBackRTP")) {
			if (activitySelected) {
				activitySelected = false;
				selectedActivity = null;
				view.setRegisterTimeTextPanelValuesToNow();
				view.getPanel("activitySelectionPanel").setVisible(true);
				view.getPanel("editRegisteredTimeTextfieldPanel").setVisible(false);
				view.getButton("btnConfirmRTP").setText("Next");
				view.getLabel("lblExplainationRTP").setText("Please select an activity");
			}
			else if (projectSelected) {
				projectSelected = false;
				view.getPanel("projectSelectionPanel").setVisible(true);
				view.getPanel("activitySelectionPanel").setVisible(false);
				selectedProject = null;
				view.getLabel("lblExplainationRTP").setText("Please select a project");
			}
		}
		else if (e.getSource() == view.getButton("btnConfirmERTP")) {
			if (!projectSelected) {
				selectedProject = view.getProjectList().getChosenProject();
				if (selectedProject == null) { return; }
				
				projectSelected = true;
				view.getPanel("projectSelectionPanel").setVisible(false);
				view.getActivityList().filterForProject(plannerApp, selectedProject);
				view.getPanel("activitySelectionPanel").setVisible(true);
				view.getLabel("lblExplainationERTP").setText("Please select an activity");
			}
			else if (!activitySelected) {
				selectedActivity = view.getActivityList().getChosenActivity();
				if (selectedActivity == null) { return; }
				
				activitySelected = true;
				view.getIntervalList().filterGetRegisteredIntervalsFromActivity(plannerApp, selectedActivity);
				view.getPanel("activitySelectionPanel").setVisible(false);
				view.getPanel("intervalSelectionPanel").setVisible(true);
				view.getLabel("lblExplainationERTP").setText("Please select an interval you wish to edit");
			}
			else if (!intervalSelected) {
				selectedInterval = view.getIntervalList().getChosenInterval();	
				if (selectedInterval == null) { return; }
				
				intervalSelected = true;			
				view.getPanel("intervalSelectionPanel").setVisible(false);
				view.setDefaultEditRegisteredTimeTextPanelValues(selectedInterval);
				view.getPanel("editRegisteredTimeTextfieldPanel").setVisible(true);
				view.getButton("btnConfirmERTP").setText("Confirm");
				view.getLabel("lblExplainationERTP").setText("Please input the new interval");
			}
			else {
				Interval i = checkValidInputsForEditRegisteredTime();
				if (i != null && this.parseEditRegisteredTime(i) ) {
					JOptionPane.showMessageDialog(view.getFrame("Employee"), "Time succesfully registered");
					projectSelected = false;
					activitySelected = false;
					intervalSelected = false;	
					view.resetTextFields();
					view.getPanel("editRegisteredTimeTextfieldPanel").setVisible(false);
					view.getPanel("projectSelectionPanel").setVisible(true);
				}
			}
		}
		else if (e.getSource() == view.getButton("btnGoBackERTP")) {
			if (intervalSelected) {
				view.getIntervalList().filterGetRegisteredIntervalsFromActivity(plannerApp, selectedActivity);
				intervalSelected = false;
				selectedInterval = null;
				view.getPanel("intervalSelectionPanel").setVisible(true);
				view.getPanel("editRegisteredTimeTextfieldPanel").setVisible(false);
				view.getButton("btnConfirmERTP").setText("Next");
				view.getLabel("lblExplainationERTP").setText("Please select an interval you wish to edit");
			}
			else if (activitySelected) {
				activitySelected = false;
				selectedActivity = null;
				view.getPanel("activitySelectionPanel").setVisible(true);
				view.getButton("btnConfirmERTP").setText("Next");
				view.getLabel("lblExplainationERTP").setText("Please select an activity");
			}
			else if (projectSelected) {
				projectSelected = false;
				view.getPanel("projectSelectionPanel").setVisible(true);
				view.getPanel("activitySelectionPanel").setVisible(false);
				selectedProject = null;
				view.getLabel("lblExplainationERTP").setText("Please select a project");
			}
		}
		else if (e.getSource() == view.getButton("btnConfirmAFH")) {
			if (!projectSelected) {
				selectedProject = view.getProjectList().getChosenProject();
				if (selectedProject == null) { return; }
				
				projectSelected = true;
				view.getActivityList().filterForProject(plannerApp, selectedProject);
				view.getPanel("projectSelectionPanel").setVisible(false);
				view.getPanel("activitySelectionPanel").setVisible(true);
				view.getLabel("lblExplainationAFH").setText("Please select an activity");
			}
			else if (!activitySelected) {
				selectedActivity = view.getActivityList().getChosenActivity();
				if (selectedActivity == null) { return; }
				
				activitySelected = true;
				view.getPanel("activitySelectionPanel").setVisible(false);
				view.getPanel("employeeSelectionPanel").setVisible(true);
				view.getButton("btnConfirmAFH").setText("Confirm");
				view.getLabel("lblExplainationAFH").setText("Please select the coworker you would like ask");
			}
			else {
				selectedEmployee = view.getEmployeeList().getChosenEmployee();
				if (selectedEmployee == null) { return; }
				
				employeeSelected = true;
				
				
				try {
					plannerApp.assignHelperToActivity(selectedEmployee, selectedActivity);
					JOptionPane.showMessageDialog(view.getFrame("Employee"), "Coworker succesfully added to activity");
				} catch (OperationNotAllowedException e1) {
					JOptionPane.showMessageDialog(view.getFrame("Employee"), e1.getMessage());
				}
			}
		}
		else if (e.getSource() == view.getButton("btnGoBackAFH")) {
			if (activitySelected) {
				activitySelected = false;
				selectedActivity = null;
				view.getPanel("activitySelectionPanel").setVisible(true);
				view.getPanel("employeeSelectionPanel").setVisible(false);
				view.getButton("btnConfirmAFH").setText("Next");
				view.getLabel("lblExplainationAFH").setText("Please select an activity");
			}
			else if (projectSelected) {
				projectSelected = false;
				view.getPanel("projectSelectionPanel").setVisible(true);
				view.getPanel("activitySelectionPanel").setVisible(false);
				selectedProject = null;
				view.getLabel("lblExplainationAFH").setText("Please select a project");
			}
		}

		else if (e.getSource() == view.getButton("btnGoBackAFH")) {
			if (projectSelected) {
				projectSelected = false;
				selectedProject = null;
				view.setRegisterTimeTextPanelValuesToNow();
				view.getPanel("projectSelectionPanel").setVisible(true);
				view.getPanel("editRegisteredTimeTextfieldPanel").setVisible(false);
				view.getButton("btnConfirmAFH").setText("Next");
			}
		}
	}
	
	
	private Interval checkValidInputsForEditRegisteredTime() {
		String message = "";
		
		String sYear = view.getTextField("txtfldEditYearSD").getText();
		String sWeek = view.getTextField("txtfldEditWeekSD").getText();
		String sHour = view.getTextField("txtfldEditHourSD").getText();
		String sMinute = view.getTextField("txtfldEditMinuteSD").getText();
		
		int year = sYear.equals("") ? -1 : Integer.parseInt(sYear);
		int week = sWeek.equals("") ? -1 : Integer.parseInt(sWeek);
		int dayOfWeek = view.getComboBox("cmbBoxEditWeekdaySD").getSelectedIndex()+1;
		int hour = sHour.equals("") ? -1 : Integer.parseInt(sHour);
		int minute = sMinute.equals("") ? -1 : Integer.parseInt(sMinute);
		
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		
		if (year < currentYear - 10 || year > currentYear + 10) {
			message = "Please input a start year within 10 years from current year.";
			JOptionPane.showMessageDialog(view.getFrame("Employee"), message);
			return null;
		}
		
		
		DateTime startTime = null;
		try {
			startTime = new DateTime.Builder()
												.year(year)
												.week(week)
												.dayOfWeek(dayOfWeek)
												.hour(hour)
												.minute(minute)
												.build();
		} catch (OperationNotAllowedException e1) {
			JOptionPane.showMessageDialog(view.getFrame("Employee"), "Invalid start time. " + e1.getMessage() +
					"\nPlease input a valid start time");
		}
		
		
		sYear = view.getTextField("txtfldEditYearED").getText();
		sWeek = view.getTextField("txtfldEditWeekED").getText();
		sHour = view.getTextField("txtfldEditHourED").getText();
		sMinute = view.getTextField("txtfldEditMinuteED").getText();
		
		year = sYear.equals("") ? -1 : Integer.parseInt(sYear);
		week = sWeek.equals("") ? -1 : Integer.parseInt(sWeek);
		dayOfWeek = view.getComboBox("cmbBoxEditWeekdayED").getSelectedIndex()+1;
		hour = sHour.equals("") ? -1 : Integer.parseInt(sHour);
		minute = sMinute.equals("") ? -1 : Integer.parseInt(sMinute);
		
		if (year < currentYear - 10 || year > currentYear + 10) {
			message = "Please input an end year within 10 years from current year.";
			JOptionPane.showMessageDialog(view.getFrame("Employee"), message);
			return null;
		}
		
		DateTime endTime = null;
		try {
			endTime = new DateTime.Builder()
											.year(year)
											.week(week)
											.dayOfWeek(dayOfWeek)
											.hour(hour)
											.minute(minute)
											.build();
		} catch (OperationNotAllowedException e1) {
			JOptionPane.showMessageDialog(view.getFrame("Employee"), "Invalid end time. " + e1.getMessage() +
					"\nPlease input a valid end time");
			return null;
		}
		
		Interval interval;
		try {
			interval = new Interval(startTime, endTime);
		} catch (OperationNotAllowedException e) {
			JOptionPane.showMessageDialog(view.getFrame("Employee"), e.getMessage());
			return null;
		}
		
		return interval;
	}
	
	private boolean parseEditRegisteredTime(Interval newInterval) {
		try {
			loggedInEmployee.alterRegisteredTime(selectedActivity, selectedInterval, newInterval);
		} catch (OperationNotAllowedException e) {
			JOptionPane.showMessageDialog(view.getFrame("Employee"), e.getMessage());
			return false;
		}
		
		return true;
	}
	
	
	private boolean parseRegisteredTime(Interval i) {
		try {
			loggedInEmployee.registerTime(selectedActivity, i);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(view.getFrame("Employee"), e.getMessage());
			return false;
		}
		
		return true;
	}
	
	
	private void resetAll() {
		currentActivePanel.setVisible(false);
		view.resetTextFields();
		view.getPanel("projectSelectionPanel").setVisible(false);
		view.getPanel("activitySelectionPanel").setVisible(false);
		view.getPanel("intervalSelectionPanel").setVisible(false);
		view.getPanel("employeeSelectionPanel").setVisible(false);
		view.getPanel("editRegisteredTimeTextfieldPanel").setVisible(false);
		view.getPanel("timecardPanel").setVisible(false);
		projectSelected = false;
		selectedProject = null;
		activitySelected = false;
		selectedActivity = null;
		intervalSelected = false;
		selectedInterval = null;
	}

}
