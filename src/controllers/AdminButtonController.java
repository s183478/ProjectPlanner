package controllers;

import domain.Employee;
import domain.Project;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import app.OperationNotAllowedException;
import app.PlannerApp;
import presentation.AdminMenu;
import presentation.ProjectManagementMenu;
import presentation.DeletionWarningFrame;

public class AdminButtonController implements ActionListener {
	/*
	 * Written by
	 * s183478, Tobias Munch
	 * s190187, Mads Kring Jakobsen
	 */
	
	private AdminMenu view;
	private PlannerApp plannerApp;
	private ProjectManagementMenu proManMenu;
	private DeletionWarningFrame delWarFrame;

	private JPanel currentActivePanel;

	public AdminButtonController(AdminMenu view, PlannerApp plannerApp) {
		super();
		this.view = view;
		this.plannerApp = plannerApp;
		currentActivePanel = view.getPanel("introductionPanel");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.getButton("btnEditProject")) {
			resetAll();
			view.getPanel("projectSelectionPanel").setVisible(true);
			view.getPanel("employeeSelectionPanel").setVisible(false);
			view.getPanel("editProjectPanel").setVisible(true);
			currentActivePanel = view.getPanel("editProjectPanel");

		}
		else if (e.getSource() == view.getButton("btnAppointProjectLeader")) {
			resetAll();
			view.getPanel("appointProjectLeaderPanel").setVisible(true);
			view.getPanel("projectSelectionPanel").setVisible(true);
			view.getPanel("employeeSelectionPanel").setVisible(false);
			currentActivePanel = view.getPanel("appointProjectLeaderPanel");

		}
		else if (e.getSource() == view.getButton("btnAddNewProject")) {
			resetAll();
			view.getPanel("addProjectPanel").setVisible(true);
			view.getPanel("projectSelectionPanel").setVisible(false);
			view.getPanel("employeeSelectionPanel").setVisible(false);
			currentActivePanel = view.getPanel("addProjectPanel");

		}
		else if (e.getSource() == view.getButton("btnRemoveProject")) {
			resetAll();
			view.getPanel("removeProjectPanel").setVisible(true);
			view.getPanel("projectSelectionPanel").setVisible(true);
			view.getPanel("employeeSelectionPanel").setVisible(false);
			currentActivePanel = view.getPanel("removeProjectPanel");

		}
		else if (e.getSource() == view.getButton("btnAddNewEmployee")) {
			resetAll();
			view.getPanel("addEmployeePanel").setVisible(true);
			view.getPanel("projectSelectionPanel").setVisible(false);
			view.getPanel("employeeSelectionPanel").setVisible(false);
			currentActivePanel = view.getPanel("addEmployeePanel");

		}
		else if (e.getSource() == view.getButton("btnRemoveEmployee")) {
			resetAll();
			view.getPanel("removeEmployeePanel").setVisible(true);
			view.getPanel("projectSelectionPanel").setVisible(false);
			view.getPanel("employeeSelectionPanel").setVisible(true);
			currentActivePanel = view.getPanel("removeEmployeePanel");

		}
		else if (e.getSource() == view.getButton("btnLogout")) {
			resetAll();
			JOptionPane.showMessageDialog(view.getFrame("Admin"), "Logout succesful");
			plannerApp.adminLogout();
			view.getFrame("Main").setVisible(true);
			view.getPanel("introductionPanel").setVisible(true);
			currentActivePanel = view.getPanel("introductionPanel");
			view.getFrame("Admin").setVisible(false);

		}
		else if (e.getSource() == view.getButton("btnConfirmREP")) {
			Employee employee;
			try {
				employee = new Employee("IBID");
				delWarFrame = new DeletionWarningFrame(this, employee);
				delWarFrame.getFrame().setVisible(true);
			} catch (OperationNotAllowedException exception) {
				JOptionPane.showMessageDialog(view.getFrame("Admin"),exception.getMessage());
				returnToIntroPanel();
			}


		}
		else if (e.getSource() == view.getButton("btnConfirmRPP")) {
			Project project = view.getProjectList().getChosenProject();
			delWarFrame = new DeletionWarningFrame(this, project);
			delWarFrame.getFrame().setVisible(true);

		}
		else if (e.getSource() == view.getButton("btnConfirmAEP")) {
			String id = view.getTextField("Employee").getText();
			try {
				plannerApp.registerEmployee(id);
				JOptionPane.showMessageDialog(view.getFrame("Admin"),"Employee " + id + " succesfully created");
			} catch (OperationNotAllowedException exception) {
				JOptionPane.showMessageDialog(view.getFrame("Admin"),exception.getMessage());
				returnToIntroPanel();
			}

		}
		else if (e.getSource() == view.getButton("btnConfirmAPLP")) {
			if (view.getPanel("projectSelectionPanel").isVisible()) {
				view.getPanel("employeeSelectionPanel").setVisible(true);
				view.getPanel("projectSelectionPanel").setVisible(false);
			} else {
				view.getPanel("employeeSelectionPanel").setVisible(false);
				view.getPanel("projectSelectionPanel").setVisible(true);
				
				Employee employee = view.getEmployeeList().getChosenEmployee();
				Project project = view.getProjectList().getChosenProject();
				try {
					plannerApp.appointProjectLeader(project, employee);
					JOptionPane.showMessageDialog(view.getFrame("Admin"),"Project leader "+ employee.getId() +" succesfully appointed");
				} catch (OperationNotAllowedException e1) {
					JOptionPane.showMessageDialog(view.getFrame("Admin"),e1.getMessage());
					returnToIntroPanel();
				}
			}
		}
		else if (e.getSource() == view.getButton("btnReturnToProjectList")) {
			if (!view.getPanel("projectSelectionPanel").isVisible()) {
				view.getPanel("employeeSelectionPanel").setVisible(false);
				view.getPanel("projectSelectionPanel").setVisible(true);
			}
		}
		else if (e.getSource() == view.getButton("btnConfirmAPP")) {
			if (view.getTextField("Project").getText().equals("")) {
				try {
					plannerApp.newProject();
					JOptionPane.showMessageDialog(view.getFrame("Admin"),"Unnamed project successfully created");
					view.getPanel("addProjectPanel").setVisible(false);
					currentActivePanel = view.getPanel("introductionPanel");
					currentActivePanel.setVisible(true);
				} catch (OperationNotAllowedException e1) {
					JOptionPane.showMessageDialog(view.getFrame("Admin"),e1.getMessage());
					returnToIntroPanel();
				}
			} else {
				try {
					plannerApp.newProject(view.getTextField("Project").getText());
					JOptionPane.showMessageDialog(view.getFrame("Admin"),
							"Project \"" + view.getTextField("Project").getText() + "\" successfully created");
					view.getPanel("addProjectPanel").setVisible(false);
					currentActivePanel = view.getPanel("introductionPanel");
					currentActivePanel.setVisible(true);
				} catch (OperationNotAllowedException e1) {
					JOptionPane.showMessageDialog(view.getFrame("Admin"),e1.getMessage());
					returnToIntroPanel();
				}
			}
		}
		else if (e.getSource() == view.getButton("btnConfirmEPP")) {

			Project project = view.getProjectList().getChosenProject();
			proManMenu = new ProjectManagementMenu(view.getFrame("Admin"), plannerApp, null, project, true);
			proManMenu.getFrame("ProMan").setVisible(true);
			view.getFrame("Admin").setVisible(false);

		}
		else if (e.getSource() == delWarFrame.getButton()) {
			try {
				if (delWarFrame.isProjectToBeDeleted()) {
					Project selected = view.getProjectList().getChosenProject();
					if (delWarFrame.getTextfield("txtfldProjectName").getText().equals(selected.getName()) &&
						delWarFrame.getTextfield("txtfldProjectID").getText().equals(selected.getId()	)) {
						plannerApp.deleteProject(selected);						
					} else {
						throw new OperationNotAllowedException("Project Name or Project ID does not match");
					}
				} else {
					Employee selected = view.getEmployeeList().getChosenEmployee();
					if (delWarFrame.getTextfield("txtfldEmployeeInitials").getText().equals(selected.getId())) {				
							plannerApp.deleteEmployee(selected);
					} else {
						throw new OperationNotAllowedException("Employee initials does not match");
					}
				}
			} catch (OperationNotAllowedException e1) {
				JOptionPane.showMessageDialog(delWarFrame.getFrame(),e1.getMessage());
			}
			delWarFrame.getFrame().setVisible(false);
		}

	}
	public void resetAll() {
		view.getPanel("projectSelectionPanel").setVisible(false);
		view.getPanel("employeeSelectionPanel").setVisible(false);
		currentActivePanel.setVisible(false);
		view.getTextField("Employee").setText("");
		view.getTextField("Project").setText("");;
	}
	private void returnToIntroPanel() {
		resetAll();
		currentActivePanel = view.getPanel("introductionPanel");
		currentActivePanel.setVisible(true);
	}

}
