package controllers;

import presentation.AdminMenu;
import presentation.EmployeeMenu;
import presentation.MainPlannerView;
import app.OperationNotAllowedException;
import app.PlannerApp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class MainButtonController implements ActionListener {
	/*
	 * Written by:
	 * s183478, Tobias Munch
	 */
	private MainPlannerView view;
	private EmployeeMenu employeeMenu;
	private AdminMenu adminMenu;
    private PlannerApp plannerApp;
	
	private String adminPassword = "123";
	
	public MainButtonController(MainPlannerView view, PlannerApp plannerApp) {
		super();
		this.view = view;
		this.plannerApp = plannerApp;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.getButton("btnEmployee")) {
			view.getPanel("employeeAdminPanel").setVisible(false);
			view.getPanel("employeeListPanel").setVisible(true);
			
		} else if (e.getSource() == view.getButton("btnAdmin")) {
			view.getPanel("employeeAdminPanel").setVisible(false);
			view.getPanel("adminLoginPanel").setVisible(true);
			
		} else if (e.getSource() == view.getButton("btnGoBackELP")) {
			view.getPanel("employeeAdminPanel").setVisible(true);
			view.getPanel("employeeListPanel").setVisible(false);
			
		} else if (e.getSource() == view.getButton("btnGoBackALP")) {
			view.getPanel("employeeAdminPanel").setVisible(true);
			view.getPanel("adminLoginPanel").setVisible(false);
			
		} else if (e.getSource() == view.getButton("btnConfirm")) {
			plannerApp.employeeLogin(view.getEmployeeList().getChosenEmployee());
			
			try {
				employeeMenu = new EmployeeMenu(view.getFrame(), plannerApp, view.getEmployeeList().getChosenEmployee());
			} catch (OperationNotAllowedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			employeeMenu.getFrame("Employee").setVisible(true);
			view.getPanel("employeeListPanel").setVisible(false);
			view.getFrame().setVisible(false);
			
		} else if (e.getSource() == view.getButton("btnLogin")) {
			String enteredPassword = view.getPasswordField().getText();
			if (enteredPassword.equals(adminPassword)) {
				view.getPasswordField().setText("");
				plannerApp.adminLogin();
				adminMenu = new AdminMenu(view.getFrame(), plannerApp);
				adminMenu.getFrame("Admin").setVisible(true);
				view.getPanel("adminLoginPanel").setVisible(false);
				view.getFrame().setVisible(false);
			} else {
				JOptionPane.showMessageDialog(view.getFrame(), "Invalid password");
			}
		}
		
	}
	
}
