package controllers;

import presentation.ProjectManagementMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import app.DateTime;
import app.OperationNotAllowedException;
import app.PlannerApp;
import domain.Activity;
import domain.Employee;
import domain.Project;

public class ProManButtonController implements ActionListener {
	/*
	 * Written by:
	 * s183478, Tobias Munch
	 */
	
	private ProjectManagementMenu view;
	private JPanel currentActivePanel;
	private Project parentProject;
	private Employee loggedInEmployee;
	private PlannerApp plannerApp;
	
	private boolean employeeSelected, activitySelected;
	private Activity selectedActivity;
	private Employee selectedEmployee;
	
	public ProManButtonController(ProjectManagementMenu view, PlannerApp plannerApp, Employee loggedInEmployee, Project parentProject) {
		this.view = view;
		this.plannerApp = plannerApp;
		this.loggedInEmployee = loggedInEmployee;
		this.parentProject = parentProject;
		currentActivePanel = view.getPanel("introductionPanel");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.getButton("btnCreateTimeUsageReport")) {
			resetAll();
			view.createNewReport();
			view.refreshTimeReportJList();
			view.getPanel("createTimeUsageReportPanel").setVisible(true);
			currentActivePanel = view.getPanel("createTimeUsageReportPanel");
			
		}
		else if (e.getSource() == view.getButton("btnAddExpectedTime")) {
			resetAll();
			view.getPanel("addExpectedTimePanel").setVisible(true);
			view.getPanel("activitySelectionPanel").setVisible(true);
			currentActivePanel = view.getPanel("addExpectedTimePanel");
			
		}
		else if (e.getSource() == view.getButton("btnAddActivity")) {
			resetAll();
			view.getPanel("addActivityPanel").setVisible(true);
			view.getPanel("activityTimePanel").setVisible(true);
			currentActivePanel = view.getPanel("addActivityPanel");
			
		}
		else if (e.getSource() == view.getButton("btnCheckAvailableEmployees")) {
			resetAll();
			view.refreshAvailableEmployeeJList();
			view.getPanel("checkAvailabilityPanel").setVisible(true);
			currentActivePanel = view.getPanel("checkAvailabilityPanel");
			
		}
		else if (e.getSource() == view.getButton("btnAddEmployeeToActivity")) {
			resetAll();
			view.getPanel("addEmployeeToActivityPanel").setVisible(true);
			view.getPanel("employeeSelectionPanel").setVisible(true);
			currentActivePanel = view.getPanel("addEmployeeToActivityPanel");
			
		}
		else if (e.getSource() == view.getButton("btnEditDetails")) {
			resetAll();
			view.getPanel("editDetailsPanel").setVisible(true);
			view.getTextField("txtfldProjectName").setText(parentProject.getName());
			view.getTextField("txtfldProjectID").setText(parentProject.getId());
			currentActivePanel = view.getPanel("editDetailsPanel");
			
		}
		else if (e.getSource() == view.getButton("btnGoBack")) {
			view.getFrame("Parent").setVisible(true);
			view.getFrame("ProMan").setVisible(false);
			
		}
		else if (e.getSource() == view.getButton("btnConfirmEDP")) {
			String newName = view.getTextField("txtfldProjectName").getText();
			String newID = view.getTextField("txtfldProjectID").getText();
			parentProject.setName(newName);
			parentProject.setId(newID);
			JOptionPane.showMessageDialog(view.getFrame("ProMan"), "Project details successfully changed");
			
		}
		else if (e.getSource() == view.getButton("btnConfirmAAP")) {
			if (view.getTextField("txtfldNameAAP").getText().equals("")) {
				JOptionPane.showMessageDialog(view.getFrame("ProMan"), "Error: No name was entered");
			} else {
				Activity newActivity = new Activity(view.getTextField("txtfldNameAAP").getText(), parentProject);
				if (view.getCheckBox("chckbxAllocatedHoursAAP").isSelected()) {
					try {
						newActivity.setExpectedTimeUsage(
								Integer.parseInt(view.getTextField("txtfldAllocatedHours").getText())
								);
					} catch (NumberFormatException | OperationNotAllowedException e1) {
						JOptionPane.showMessageDialog(view.getFrame("ProMan"), e1.getMessage());
						returnToIntroPanel();
						return;
					}
				}
				if (view.getCheckBox("chckbxRunningTimeAAP").isSelected()) {
					DateTime startDate = assignDateTimeFromTextfields(true);
					DateTime endDate = assignDateTimeFromTextfields(false);
					try {
						newActivity.setRunningTime(startDate, endDate);
					} catch (OperationNotAllowedException e1) {
						JOptionPane.showMessageDialog(view.getFrame("ProMan"), e1.getMessage());
						returnToIntroPanel();
						return;
					}
				}
				parentProject.addActivity(newActivity);
				JOptionPane.showMessageDialog(view.getFrame("ProMan"), "Activity " + view.getTextField("txtfldNameAAP").getText() + " succesfully created");
			}
			
		}
		else if (e.getSource() == view.getButton("btnConfirmAET")) {
			if (!activitySelected) {
				activitySelected = true;
				selectedActivity = view.getActivityList().getChosenActivity();
				view.getPanel("activitySelectionPanel").setVisible(false);
				view.getPanel("activityTimePanel").setVisible(true);
				view.getButton("btnConfirmAET").setText("Confirm");
			} else {
				if (view.getCheckBox("chckbxAllocatedHoursAAP").isSelected()) {
					try {
						selectedActivity.setExpectedTimeUsage(
								Integer.parseInt(view.getTextField("txtfldAllocatedHours").getText())
								);
					} catch (NumberFormatException | OperationNotAllowedException e1) {
						JOptionPane.showMessageDialog(view.getFrame("ProMan"), e1.getMessage());
						returnToIntroPanel();
						return;
					}
				}
				if (view.getCheckBox("chckbxRunningTimeAAP").isSelected()) {
					DateTime startDate = assignDateTimeFromTextfields(true);
					DateTime endDate = assignDateTimeFromTextfields(false);
					try {
						selectedActivity.setRunningTime(startDate, endDate);
					} catch (OperationNotAllowedException e1) {
						JOptionPane.showMessageDialog(view.getFrame("ProMan"), e1.getMessage());
						returnToIntroPanel();
						return;
					}
				}
				JOptionPane.showMessageDialog(view.getFrame("ProMan"), "Expected time successfully added");
				
			}
		}
		else if (e.getSource() == view.getButton("btnGoBackAET")) {
			if (activitySelected) {
				activitySelected = false;
				selectedActivity = null;
				view.getPanel("activitySelectionPanel").setVisible(true);
				view.getPanel("activityTimePanel").setVisible(false);
				view.getButton("btnConfirmAET").setText("Select");
			}
			
		}
		else if (e.getSource() == view.getButton("btnConfirmAETA")) {
			if (!employeeSelected) {
				employeeSelected = true;
				selectedEmployee = view.getEmployeeList().getChosenEmployee();
				view.getPanel("activitySelectionPanel").setVisible(true);
				view.getPanel("employeeSelectionPanel").setVisible(false);
				view.getButton("btnConfirmAETA").setText("Confirm");
			} else {
				selectedActivity = view.getActivityList().getChosenActivity();
				try {
					selectedActivity.assignEmployee(selectedEmployee);
				} catch (OperationNotAllowedException e1) {
					JOptionPane.showMessageDialog(view.getFrame("ProMan"), e1.getMessage());
					return;
				}
				JOptionPane.showMessageDialog(view.getFrame("ProMan"), 
						"Employee " + selectedEmployee.getId() +
						" successfully added to " + selectedActivity.getName() +
						" activity");
			}
			
		}
		else if (e.getSource() == view.getButton("btnGoBackAETA")) {
			if (employeeSelected) {
				employeeSelected = false;
				selectedEmployee = null;
				view.getPanel("activitySelectionPanel").setVisible(false);
				view.getPanel("employeeSelectionPanel").setVisible(true);
				view.getButton("btnConfirmAETA").setText("Select");
			}
			
		}
	}
	private DateTime assignDateTimeFromTextfields(boolean isStartDate) {
		DateTime date = null;
		String sYear = view.getTextField("txtfldYear" + (isStartDate? "SD" : "ED")).getText();
		String sWeek = view.getTextField("txtfldWeek" + (isStartDate? "SD" : "ED")).getText();
		String sHour = view.getTextField("txtfldHour" + (isStartDate? "SD" : "ED")).getText();
		String sMinute = view.getTextField("txtfldMinute" + (isStartDate? "SD" : "ED")).getText();
		
		int year = sYear.equals("") ? -1 : Integer.parseInt(sYear);
		int week = sWeek.equals("") ? -1 : Integer.parseInt(sWeek);
		int dayOfWeek = view.getComboBox("cmbBoxWeekdaySD").getSelectedIndex()+1;
		int hour = sHour.equals("") ? -1 : Integer.parseInt(sHour);
		int minute = sMinute.equals("") ? -1 : Integer.parseInt(sMinute);
		
		try {
			date = new DateTime.Builder()
					.year(year)
					.week(week)
					.dayOfWeek(dayOfWeek)
					.hour(hour)
					.minute(minute)
					.build();
		} catch (OperationNotAllowedException e) {
			JOptionPane.showMessageDialog(view.getFrame("ProMan"), e.getMessage());
		}
		
		return date;
		
	}

	private void resetAll() {
		currentActivePanel.setVisible(false);
		view.resetAllTextFields();
		view.untickAllCheckboxes();
		view.getPanel("activitySelectionPanel").setVisible(false);
		view.getPanel("employeeSelectionPanel").setVisible(false);
		view.getPanel("activityTimePanel").setVisible(false);
		activitySelected = false;
		employeeSelected = false;
		selectedActivity = null;
		selectedEmployee = null;
	}
	private void returnToIntroPanel() {
		resetAll();
		currentActivePanel = view.getPanel("introductionPanel");
		currentActivePanel.setVisible(true);
	}

}
