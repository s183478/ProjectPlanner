package domain;
import domain.Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.Interval;
import app.OperationNotAllowedException;

public class Employee {
	/*
	 * Written by:
	 * L.Noer s155490
	 * Oscar J�rgensen s173470
	 * s173906, Jonas Weile
	 */

	private String employeeId;
	private HashMap<Activity, ArrayList<Interval>> timeCard;
		
	
	public Employee(String id) throws OperationNotAllowedException {
		id = toCorrectFormat(id);
		checkValidId(id);
		
		this.employeeId = id;
		timeCard = new HashMap<Activity, ArrayList<Interval>>();
	}
	
	
	// Getters
	public String getId() {
		return employeeId;
	}
	
	
	// Setters
	public void setId(String employeeId) {
		this.employeeId = employeeId;
	}

	
	// Methods
	private void checkValidId(String id) throws OperationNotAllowedException {
		if (id.length() == 0 || id.length() > 4) {
			throw new OperationNotAllowedException("The employee id is not valid");
		}
	}
	
	// TODO : test?
	public Activity getActivityFromInterval(Interval interval) {
		
		for ( Map.Entry<Activity, ArrayList<Interval>> entry : timeCard.entrySet() ) {
			if ( entry.getValue().stream()
					.anyMatch(i -> i.Overlaps(interval))
			   )
			{ 
				return entry.getKey();
			}
		}
		
		return null;
	}
	
	
	public ArrayList<Interval> getRegisteredTimeForActivity(Activity activity) {
		return timeCard.get(activity);
	}
	

	private boolean intervalOverlapsAlreadyRegisteredInterval(Interval workedInterval)
			throws OperationNotAllowedException {
		
		for ( Map.Entry<Activity, ArrayList<Interval>> entry : timeCard.entrySet() ) {
			if ( entry.getValue().stream()
					.anyMatch(interval -> interval.Overlaps(workedInterval))
			   )
			{ 
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isAvailable() {
		// TODO : Update this badboy
		if (timeCard.size() < 10) {
			return true;
		}
		return false;
	}
	
	public void registerTime(Activity activity, Interval workedInterval) throws Exception {	
		assert activity.isEmployeeAssignedToActivity(this) == true || activity.isEmployeeHelpingOnActivity(this) == true: "Pre: You cannot register time on an activity which you are not registered to";
		assert intervalOverlapsAlreadyRegisteredInterval(workedInterval) == false : "Pre: You have already registered on another activity during this timeslot";
		if ( !activity.isEmployeeAssignedToActivity(this) &&
			 !activity.isEmployeeHelpingOnActivity(this)) {
			throw new OperationNotAllowedException("You cannot register time on an activity which you are not registered to");
		}
		
		if (workedInterval.lengthInWholeHours() >= 12) {
			throw new OperationNotAllowedException("You cannot register more than 12 hours at once. Please split up the registration in smaller parts.");
		}
		
		if ( intervalOverlapsAlreadyRegisteredInterval(workedInterval) ) {
			throw new OperationNotAllowedException("You have already registered time spent on another activity during this timeslot.");
		}
		
		if (timeCard.containsKey(activity)) {
			timeCard.get(activity).add(workedInterval);
			
		} else {
			ArrayList<Interval> intervalList = new ArrayList<Interval>();
			intervalList.add(workedInterval);
			timeCard.put(activity, intervalList);
		}
		assert timeCard.get(activity).indexOf(workedInterval) != -1 : "Post: interval not added successfully";
	}
	
	@Override
	public String toString() {
		return employeeId;
	}
	
	private String toCorrectFormat(String id) {
		id = id.replace(" " , "");
		id.toUpperCase();
		
		return id;
	}


	public HashMap<Activity, ArrayList<Interval>> getTimeCard() {
		return timeCard;
	}


	public void alterRegisteredTime(Activity selectedActivity, Interval oldInterval, Interval newInterval)
									throws OperationNotAllowedException {
		try {
			timeCard.get(selectedActivity).remove(oldInterval);
			registerTime(selectedActivity, newInterval);
		} catch (Exception e) {
			timeCard.get(selectedActivity).add(oldInterval);
			throw new OperationNotAllowedException(e.getMessage());
		}
		
	}
}
