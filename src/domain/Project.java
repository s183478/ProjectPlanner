package domain;

import java.util.ArrayList;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;


public class Project {
	/*
	 * Written by:
	 * L.Noer s155490
	 * Oscar J�rgensen s173470
	 * s173906, Jonas Weile
	 */
	
	// Implementing the observer pattern
	PropertyChangeSupport support = new PropertyChangeSupport(this);
	public void addObserver(PropertyChangeListener l) {
		support.addPropertyChangeListener(l);
	}


	private ArrayList<Activity> activities;
	private String id;
	private String name;
	private Employee projectLeader;
	private HashMap<Activity, ArrayList<Integer>> report;


	public Project(Calendar date, int numberOfExistingProjects) {
		SimpleDateFormat formatter = new SimpleDateFormat("MMyy");
		String serialNumberString = String.format("%02d", numberOfExistingProjects+1);

		this.id = formatter.format(date.getTime()) + serialNumberString;
		this.activities = new ArrayList<Activity>();
	}

	public Project(String name, Calendar date, int numberOfExistingProjects) {
		this(date, numberOfExistingProjects);
		this.name = name;
	}


	// Getters
	public ArrayList<Activity> getActivities() {
		return this.activities;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Employee getProjectLeader () {
		return projectLeader;
	}
	
	// Setters
	public void setName(String name) {
		this.name = name;
	}


	// Methods
	public void appointLeader(Employee employee) {
		this.projectLeader = employee;
	}

	public void addActivity(Activity activity) {
		for (Activity act: activities) {
			assert act.getName() != activity.getName() : "Pre: Activity already exists";
		}
		for (Activity a : activities) {
			if (a.getName().equals(activity.getName())) {
				throw new IllegalArgumentException("An activity with the same name already exists in this project");
			}
		}
		
		activities.add(activity);
		support.firePropertyChange("NEW ACTIVITY", null, activity);
		assert activities.indexOf(activity) != -1 : "Post: Activity not added successfully";
	}
	
	public Activity addNewActivity(String activityName) {
		boolean existingProjectWithSameName =  activities.stream()
													.anyMatch(a -> a.getName().equals(activityName));

	    if (existingProjectWithSameName) {
	    	throw new IllegalArgumentException("An activity with the same name already exists in this project");
	    }

		Activity a = new Activity(activityName, this);
		activities.add(a);
		support.firePropertyChange("NEW ACTIVITY", null, a);

		return a;
	}


	public void createReport() {
		ArrayList time = new ArrayList<Integer>();
		report = new HashMap<Activity, ArrayList<Integer>>();
		for(int i = 0; i < activities.size(); i++) {
			Activity act = activities.get(i);
			if (act.getRunningTime() != null) {
				time.add(act.getStartTime());
				time.add(act.getEndTime());
				report.put(activities.get(i),time);
			}
		}
	}
	
	public HashMap getReport() {
		return report;
	}

	public Activity getActivityFromName(String activityName) {
		Activity activity = activities.stream()
								.filter(a -> (a.getName().equals(activityName)))
								.findAny().orElse(null);

		return activity;
	}


	@Override
	public String toString() {
		if (name != null) {
			return name + " - " + id;
		} else {
			return id;
		}
	}

	public void setId(String newID) {
		this.id = newID;
	}
}
