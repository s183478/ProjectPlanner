package domain;

import java.util.ArrayList;

import app.DateTime;
import app.Interval;
import app.OperationNotAllowedException;
import domain.Employee;

public class Activity {
	/*
	 * Written by:
	 * s190187, Mads Kring Jakobsen
	 * s173906, Jonas Weile
	 * s173470, Oscar Jrgensen
	 * s155490, L.Noer
	 */

	private String name;
	private String type;
	private Project parentProject;

	private ArrayList<Employee> assignedEmployeesList = new ArrayList<Employee>();
	private ArrayList<Employee> helpersList = new ArrayList<Employee>();

	private Interval runningTime;
	private int expectedTimeUsage = 0;


	public Activity(String name, Project parentProject) {
		this.parentProject = parentProject;
		this.name = name;
	}


	// Getters
	public DateTime getEndTime() {
		return runningTime.getEndTime();
	}

	public int getExpectedTimeUsage() {
		return this.expectedTimeUsage;
	}

	public String getName() {
		return this.name;
	}

	public Project getParentProject() {
		return parentProject;
	}

	public Interval getRunningTime() {
		return this.runningTime;
	}

	public DateTime getStartTime() {
		return runningTime.getStartTime();
	}

	public String getType() {
		return this.type;
	}


	// Setters
	public void setExpectedTimeUsage(int hours) throws OperationNotAllowedException {
		checkLegalTimeUsage(hours);
		this.expectedTimeUsage = hours;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRunningTime(DateTime startTime, DateTime endTime) throws OperationNotAllowedException {
		Interval runningTime = new Interval(startTime, endTime);
		this.runningTime = runningTime;
	}

	public void setRunningTime(Interval runningTime) throws OperationNotAllowedException {
		this.runningTime = runningTime;
	}

	public void setType(String type) {
		this.type = type;
	}


	// Methods
	public void assignEmployee(Employee employee) throws OperationNotAllowedException {
		assert assignedEmployeesList.indexOf(employee) == -1 : "Pre: Employee is already assigned to activity";
		if (isEmployeeAssignedToActivity(employee)) {
			throw new OperationNotAllowedException("Employee is already assigned to this activity");
		}

		assignedEmployeesList.add(employee);
		assert assignedEmployeesList.indexOf(employee) != -1 : "Post: Employee not added successfully";
	}


	public void assignHelper(Employee currentUser, Employee helper) throws OperationNotAllowedException {

		if (isEmployeeHelpingOnActivity(helper)) {
			throw new OperationNotAllowedException("Employee is already helping on this activity");
		}

		if ( !isEmployeeAssignedToActivity(currentUser) && (currentUser != parentProject.getProjectLeader()) ) {
			throw new OperationNotAllowedException("You are not assigned to this activity, and thus cannot add helpers");
		}

		helpersList.add(helper);
	}


	public void checkLegalTimeUsage(int expectedTimeUsage) throws OperationNotAllowedException {
		if (expectedTimeUsage < 0) {
			throw new OperationNotAllowedException("The expected time cannot be negative!");
		}
	}


	public Employee getEmployeeFromID(String id){
		return assignedEmployeesList.stream()
					.filter(em -> em.getId().equals(id))
					.findAny().orElse(null);
	}


	public Employee getHelperFromID(String id) {
		return helpersList.stream()
					.filter(em -> em.getId().equals(id))
					.findAny().orElse(null);
	}


	public boolean isEmployeeAssignedToActivity(Employee employee) {
		return assignedEmployeesList.stream()
							.anyMatch(e -> e.equals(employee));
	}


	public boolean isEmployeeHelpingOnActivity(Employee employee ) {
		return helpersList.stream()
				.anyMatch(e -> e.equals(employee));
	}

	@Override
	public String toString() {
		return name;
	}


	public String getParentProjectDescription() {
		if (parentProject == null) { return "General"; }

		return parentProject.getId();
	}
}
