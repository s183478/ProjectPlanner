package persistence;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import app.EmployeeRepository;
import app.ProjectRepository;
import domain.Employee;
import domain.Project;

public class InMemoryRepository implements EmployeeRepository, ProjectRepository {
	
	/*
	 * Written by:
	 * s190187, Mads Kring Jakobsen
	 */

	ArrayList<Project> projects = new ArrayList<>();
	ArrayList<Employee> employees = new ArrayList<>();
	
	
	// Employee related methods
	@Override
	public void addEmployee(Employee employee) {
		employees.add(employee);
	}
	
	@Override
	public void clearEmployeeDatabase() {
		employees.clear();
		
	}
	
	@Override
	public void deleteEmployee(Employee employee) {
		employees.remove(employee);
	}
	
	@Override
	public Object[] getAllEmployeesArray() {
		return employees.toArray();
	}
	
	@Override
	public ArrayList getAllEmployeesList() {
		return employees;
	}
	
	@Override
	public Stream<Employee> getAllEmployeesStream() {
		return employees.stream();
	}
	
	@Override
	public Employee getEmployeeFromId(String employeeId) {
		return employees.stream()
						.filter(e -> e.getId().equals(employeeId))
						.findAny().orElse(null);

	}
	
	@Override
	public int getNumberOfEmployees() {
		return employees.size();
	}
	
	
	
	// Project related methods
	@Override
	public void addProject(Project project) {
		projects.add(project);
	}
	
	@Override
	public void clearProjectDatabase() {
		projects.clear();
		
	}
	
	@Override
	public void deleteProject(Project project) {
		projects.remove(project);
	}

	@Override
	public Object[] getAllProjectsArray() {
		return projects.toArray();
	}
	
	@Override
	public ArrayList getAllProjectsList() {
		return projects;
	}
	
	@Override
	public Stream<Project> getAllProjectsStream() {
		return projects.stream();
	}
	
	@Override
	public int getNumberOfProjects() {
		return projects.size();
	}

	@Override
	public Project getProjectFromId(String projectId) {
		return projects.stream().filter(p -> p.getId().equals(projectId)).findFirst().orElse(null);
	}	

	@Override
	public ArrayList<Project> getProjectsFromName(String name) {
		ArrayList<Project> list = 
				new ArrayList<Project>(	projects.stream()
												.filter(p -> p.getName().equals(name))
												.collect(Collectors.toList())
				);
		
		return list;
	}
}

