package presentation;

import controllers.MainButtonController;
import app.PlannerApp;
import javax.swing.JFrame;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JButton;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;


public class MainPlannerView {
	/*
	 * Written by:
	 * s183478, Tobias Munch
	 */

	private static JFrame frameMain;

	private JPanel employeeAdminPanel, employeeListPanel, adminLoginPanel;

	private JButton btnEmployee, btnAdmin, btnGoBackELP, btnConfirm, btnGoBackALP, btnLogin;
	private JPasswordField passwordField;

	private EmployeeList employeeList;
	private MainButtonController controller;

	public MainPlannerView(PlannerApp plannerApp) {
		
		// TODO : add observer
		employeeList = new ObjectList().newEmployeeList(plannerApp);
		
		initialize();
		controller = new MainButtonController(this, plannerApp);
		addActionListeners();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		initFrame();
		initPanels();
	}

	private void addActionListeners() {
		btnEmployee.addActionListener(controller);
		btnAdmin.addActionListener(controller);
		btnConfirm.addActionListener(controller);
		btnGoBackELP.addActionListener(controller);
		btnGoBackALP.addActionListener(controller);
		btnLogin.addActionListener(controller);

	}

	private void initFrame() {
		frameMain = new JFrame();
		frameMain.setTitle("Project Planner");
		frameMain.setBounds(100, 100, 720, 480);
		frameMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameMain.getContentPane().setLayout(new CardLayout(0, 0));
	}

	private void initPanels() {
		employeeAdminPanel = new JPanel();
		employeeListPanel = new JPanel();
		adminLoginPanel = new JPanel();

		frameMain.getContentPane().add(employeeAdminPanel, "name_685797912294200");
		frameMain.getContentPane().add(employeeListPanel, "name_685811215538400");
		frameMain.getContentPane().add(adminLoginPanel, "name_686981135771700");

		employeeAdminPanel.setLayout(null);
		employeeListPanel.setLayout(null);
		adminLoginPanel.setLayout(null);

		initEmployeeAdminPanel();
		initEmployeeListPanel();
		initAdminLoginPanel();
	}

	private void initEmployeeAdminPanel() {
		btnEmployee = new JButton("Employee");
		btnAdmin = new JButton("Administrator");
		JLabel selectJobLabel = new JLabel("Please select your job");

		btnEmployee.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnAdmin.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnEmployee.setBounds(102, 170, 198, 82);
		btnAdmin.setBounds(402, 170, 198, 82);

		employeeAdminPanel.add(btnEmployee);
		employeeAdminPanel.add(btnAdmin);

		selectJobLabel.setHorizontalAlignment(SwingConstants.CENTER);
		selectJobLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		selectJobLabel.setBounds(193, 86, 316, 71);
		employeeAdminPanel.add(selectJobLabel);
	}

	private void initEmployeeListPanel() {
		JPanel employeeSelectionPanel = new JPanel();
		btnGoBackELP = new JButton("Go back");
		btnConfirm = new JButton("Confirm");
		JScrollPane employeeScrollPane = employeeList.getScroller();
		JLabel lblSelectYourself = new JLabel("Please select yourself");

		employeeSelectionPanel.setBounds(66, 76, 569, 241);
		employeeListPanel.add(employeeSelectionPanel);
		employeeSelectionPanel.setLayout(new CardLayout(0, 0));

		lblSelectYourself.setHorizontalAlignment(SwingConstants.CENTER);
		lblSelectYourself.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblSelectYourself.setBounds(12, 13, 678, 73);
		employeeListPanel.add(lblSelectYourself);

		employeeScrollPane.setBounds(0, 0, 10, 10);
		employeeSelectionPanel.add(employeeScrollPane);

		btnGoBackELP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnGoBackELP.setBounds(100, 355, 201, 40);
		employeeListPanel.add(btnGoBackELP);

		btnConfirm.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnConfirm.setBounds(401, 355, 201, 40);
		employeeListPanel.add(btnConfirm);
	}

	private void initAdminLoginPanel() {
		JLabel enterAdminPasswordLabel = new JLabel("Please enter the administrator password");
		passwordField = new JPasswordField();
		btnGoBackALP = new JButton("Go back");
		btnLogin = new JButton("Log in");

		enterAdminPasswordLabel.setHorizontalAlignment(SwingConstants.CENTER);
		enterAdminPasswordLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		enterAdminPasswordLabel.setBounds(12, 80, 678, 108);

		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		passwordField.setBounds(132, 201, 444, 51);

		btnGoBackALP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnGoBackALP.setBounds(132, 265, 201, 40);

		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnLogin.setBounds(375, 265, 201, 40);

		adminLoginPanel.add(enterAdminPasswordLabel);
		adminLoginPanel.add(passwordField);
		adminLoginPanel.add(btnGoBackALP);
		adminLoginPanel.add(btnLogin);
	}

	public JPanel getPanel(String type) {
		if (type.equals("employeeAdminPanel")) {
			return employeeAdminPanel;
		} else if (type.equals("employeeListPanel")) {
			return employeeListPanel;
		} else if (type.equals("adminLoginPanel")) {
			return adminLoginPanel;
		} else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JButton getButton(String type) {
		if (type.equals("btnEmployee")) {
			return btnEmployee;
		} else if (type.equals("btnAdmin")) {
			return btnAdmin;
		} else if (type.equals("btnGoBackELP")) {
			return btnGoBackELP;
		} else if (type.equals("btnConfirm")) {
			return btnConfirm;
		} else if (type.equals("btnGoBackALP")) {
			return btnGoBackALP;
		} else if (type.equals("btnLogin")) {
			return btnLogin;
		} else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JFrame getFrame() {
		return frameMain;
	}

	public EmployeeList getEmployeeList() {
		return employeeList;
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}
}
