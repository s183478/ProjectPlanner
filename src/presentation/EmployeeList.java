package presentation;

import javax.swing.JScrollPane;

import app.PlannerApp;
import domain.Employee;

public interface EmployeeList {
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */
	
	public ObjectList newEmployeeList(PlannerApp planner);
	
	public String getChosenEmployeeId();
	
	public Employee getChosenEmployee();
	
	public JScrollPane getScroller();
}
