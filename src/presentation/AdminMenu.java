package presentation;

import controllers.AdminButtonController;
import app.PlannerApp;
import java.awt.Font;
import java.awt.CardLayout;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.border.BevelBorder;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class AdminMenu {
	/*
	 * Written by:
	 * s183478, Tobias Munch
	 */

	private JFrame frameMain, frameAdmin;

	private JPanel contentPanel, buttonPanel, introductionPanel, addProjectPanel, editProjectPanel,
			appointProjectLeaderPanel, addEmployeePanel, removeEmployeePanel, removeProjectPanel, projectSelectionPanel,
			employeeSelectionPanel;
	private JButton btnEditProject, btnAppointProjectLeader, btnAddNewProject, btnRemoveProject, btnAddNewEmployee,
			btnRemoveEmployee, btnLogout, btnConfirmREP, btnConfirmRPP, btnConfirmAEP, btnConfirmAPLP,
			btnReturnToProjectList, btnConfirmAPP, btnConfirmEPP, btnGoBackAETA, btnConfirmAETA;

	private JTextField txtfldProjectName, txtfldEmployeeName;

	private AdminButtonController controller;
	private ProjectList projectList;
	private EmployeeList employeeList;

	public AdminMenu(JFrame main, PlannerApp plannerApp) {
		this.frameMain = main;

		// Add observer here
		projectList = new ObjectList().newProjectList(plannerApp);
		employeeList = new ObjectList().newEmployeeList(plannerApp);

		initialize();
		controller = new AdminButtonController(this, plannerApp);
		addActionListeners();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		initFrame();
		initAuxiliaryPanels();
		initPanels();
		initMenuButtons();
	}

	private void addActionListeners() {
		btnEditProject.addActionListener(controller);
		btnAppointProjectLeader.addActionListener(controller);
		btnAddNewProject.addActionListener(controller);
		btnRemoveProject.addActionListener(controller);
		btnAddNewEmployee.addActionListener(controller);
		btnRemoveEmployee.addActionListener(controller);
		btnLogout.addActionListener(controller);

		btnConfirmREP.addActionListener(controller);
		btnConfirmRPP.addActionListener(controller);
		btnConfirmAEP.addActionListener(controller);
		btnConfirmAPLP.addActionListener(controller);
		btnConfirmAPP.addActionListener(controller);
		btnConfirmEPP.addActionListener(controller);
		btnReturnToProjectList.addActionListener(controller);

	}

	private void initAuxiliaryPanels() {
		projectSelectionPanel = new JPanel();
		employeeSelectionPanel = new JPanel();
		JScrollPane projectScrollPane = projectList.getScroller();
		JScrollPane employeeScrollPane = employeeList.getScroller();

		projectScrollPane.setBounds(0, 0, 408, 342);
		projectSelectionPanel.setBounds(272, 110, 406, 243);
		projectSelectionPanel.setLayout(new CardLayout(0, 0));
		projectSelectionPanel.add(projectScrollPane);

		employeeScrollPane.setBounds(0, 0, 408, 342);
		employeeSelectionPanel.setBounds(272, 110, 406, 243);
		employeeSelectionPanel.setLayout(new CardLayout(0, 0));
		employeeSelectionPanel.add(employeeScrollPane);

		frameAdmin.getContentPane().add(projectSelectionPanel);
		frameAdmin.getContentPane().add(employeeSelectionPanel);
		employeeSelectionPanel.setVisible(false);
		projectSelectionPanel.setVisible(false);

	}

	private void initMenuButtons() {
		btnEditProject = new JButton("Edit project");
		btnAppointProjectLeader = new JButton("Appoint Project Leader");
		btnAddNewProject = new JButton("Add new project");
		btnRemoveProject = new JButton("Remove project");
		btnAddNewEmployee = new JButton("Add new employee");
		btnRemoveEmployee = new JButton("Remove employee");
		btnLogout = new JButton("Log out");

		btnEditProject.setBounds(35, 13, 166, 43);
		btnAppointProjectLeader.setBounds(35, 69, 166, 43);
		btnAddNewProject.setBounds(35, 125, 166, 43);
		btnRemoveProject.setBounds(35, 181, 166, 43);
		btnAddNewEmployee.setBounds(35, 237, 166, 43);
		btnRemoveEmployee.setBounds(35, 293, 166, 43);
		btnLogout.setBounds(65, 359, 106, 35);

		buttonPanel.add(btnEditProject);
		buttonPanel.add(btnAppointProjectLeader);
		buttonPanel.add(btnAddNewProject);
		buttonPanel.add(btnRemoveProject);
		buttonPanel.add(btnAddNewEmployee);
		buttonPanel.add(btnRemoveEmployee);
		buttonPanel.add(btnLogout);

	}

	private void initPanels() {
		buttonPanel = new JPanel();
		buttonPanel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		buttonPanel.setBounds(12, 13, 236, 407);
		buttonPanel.setLayout(null);
		frameAdmin.getContentPane().add(buttonPanel);

		contentPanel = new JPanel();
		contentPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		contentPanel.setBounds(260, 13, 430, 407);
		contentPanel.setLayout(new CardLayout(0, 0));
		frameAdmin.getContentPane().add(contentPanel);

		initIntroductionPanel();
		initAddProjectPanel();
		initEditProjectPanel();
		initAppointProjectLeaderPanel();
		initAddEmployeePanel();
		initRemoveProjectPanel();
		initRemoveEmployeePanel();

	}

	private void initRemoveEmployeePanel() {
		removeEmployeePanel = new JPanel();
		removeEmployeePanel.setLayout(null);

		btnConfirmREP = new JButton("Confirm");
		JLabel lblRemoveEmployee = new JLabel("Remove employee");
		JLabel lblExplainer = new JLabel("Please select an employee to remove");

		lblRemoveEmployee.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblRemoveEmployee.setHorizontalAlignment(SwingConstants.CENTER);
		lblRemoveEmployee.setBounds(126, 13, 174, 16);

		lblExplainer.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainer.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainer.setBounds(100, 46, 225, 20);

		btnConfirmREP.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnConfirmREP.setBounds(149, 350, 127, 40);

		removeEmployeePanel.add(lblRemoveEmployee);
		removeEmployeePanel.add(lblExplainer);
		removeEmployeePanel.add(btnConfirmREP);

		contentPanel.add(removeEmployeePanel, "name_469953349954700");
	}

	private void initRemoveProjectPanel() {
		removeProjectPanel = new JPanel();
		removeProjectPanel.setLayout(null);

		JLabel lblRemoveProject = new JLabel("Remove project");
		JLabel lblExplainer = new JLabel("Please select a project to remove");
		btnConfirmRPP = new JButton("Confirm");

		lblRemoveProject.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblRemoveProject.setHorizontalAlignment(SwingConstants.CENTER);
		lblRemoveProject.setBounds(141, 13, 143, 16);

		lblExplainer.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainer.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainer.setBounds(100, 46, 225, 20);

		btnConfirmRPP.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnConfirmRPP.setBounds(149, 350, 127, 40);

		removeProjectPanel.add(lblRemoveProject);
		removeProjectPanel.add(lblExplainer);
		removeProjectPanel.add(btnConfirmRPP);

		contentPanel.add(removeProjectPanel, "name_469930052442800");
	}

	private void initAddEmployeePanel() {
		addEmployeePanel = new JPanel();
		addEmployeePanel.setLayout(null);

		JLabel lblAddEmployee = new JLabel("Add Employee");
		JLabel lblExplainerInitials = new JLabel("Please input the initials of the new employee, up to four letters.");
		JLabel lblExplainerID = new JLabel("ID will be assigned automatically");
		JLabel lblInitials = new JLabel("Initials: ");
		btnConfirmAEP = new JButton("Confirm");
		txtfldEmployeeName = new JTextField();

		lblAddEmployee.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddEmployee.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAddEmployee.setBounds(138, 13, 149, 16);

		lblExplainerInitials.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainerInitials.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainerInitials.setBounds(12, 42, 402, 16);

		lblExplainerID.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainerID.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainerID.setBounds(72, 65, 282, 16);

		lblInitials.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblInitials.setBounds(125, 145, 72, 16);

		txtfldEmployeeName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtfldEmployeeName.setBounds(198, 139, 89, 28);
		txtfldEmployeeName.setColumns(10);

		btnConfirmAEP.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnConfirmAEP.setBounds(143, 306, 140, 33);

		addEmployeePanel.add(lblAddEmployee);
		addEmployeePanel.add(lblExplainerInitials);
		addEmployeePanel.add(lblExplainerID);
		addEmployeePanel.add(lblInitials);
		addEmployeePanel.add(txtfldEmployeeName);
		addEmployeePanel.add(btnConfirmAEP);

		contentPanel.add(addEmployeePanel, "name_130107902676800");
	}

	private void initAppointProjectLeaderPanel() {
		appointProjectLeaderPanel = new JPanel();
		appointProjectLeaderPanel.setLayout(null);

		btnConfirmAPLP = new JButton("Confirm");
		btnReturnToProjectList = new JButton("Return to project list");
		JLabel lblAppointProjectLeader = new JLabel("Appoint Project Leader");
		JLabel lblExplaination = new JLabel("Please select a project, and then an employee");

		lblExplaination.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplaination.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplaination.setBounds(64, 46, 297, 16);

		lblAppointProjectLeader.setBounds(118, 13, 189, 16);
		lblAppointProjectLeader.setFont(new Font("Tahoma", Font.BOLD, 16));

		btnConfirmAPLP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnConfirmAPLP.setBounds(249, 350, 165, 44);

		btnReturnToProjectList.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnReturnToProjectList.setBounds(12, 350, 185, 44);

		appointProjectLeaderPanel.add(lblExplaination);
		appointProjectLeaderPanel.add(lblAppointProjectLeader);
		appointProjectLeaderPanel.add(btnReturnToProjectList);
		appointProjectLeaderPanel.add(btnConfirmAPLP);

		contentPanel.add(appointProjectLeaderPanel, "name_130078544616300");
	}

	private void initAddProjectPanel() {
		addProjectPanel = new JPanel();
		addProjectPanel.setLayout(null);

		JLabel lblAddProjec = new JLabel("Add new project");
		JLabel lblEnterProjectName = new JLabel("Please enter the name of the new project");
		JLabel lblProjectNumberExplaination = new JLabel("Project number will be assigned automatically");
		btnConfirmAPP = new JButton("Confirm");
		txtfldProjectName = new JTextField();

		lblAddProjec.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddProjec.setBounds(142, 13, 142, 16);
		lblAddProjec.setFont(new Font("Tahoma", Font.BOLD, 16));

		lblEnterProjectName.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnterProjectName.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblEnterProjectName.setBounds(63, 85, 300, 28);

		txtfldProjectName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtfldProjectName.setBounds(12, 115, 402, 37);
		txtfldProjectName.setColumns(10);

		btnConfirmAPP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnConfirmAPP.setBounds(134, 324, 157, 44);

		lblProjectNumberExplaination.setFont(new Font("Tahoma", Font.ITALIC, 14));
		lblProjectNumberExplaination.setHorizontalAlignment(SwingConstants.CENTER);
		lblProjectNumberExplaination.setBounds(63, 190, 300, 37);

		addProjectPanel.add(lblProjectNumberExplaination);
		addProjectPanel.add(btnConfirmAPP);
		addProjectPanel.add(txtfldProjectName);
		addProjectPanel.add(lblAddProjec);
		addProjectPanel.add(lblEnterProjectName);

		contentPanel.add(addProjectPanel, "name_128683802233200");
	}

	private void initIntroductionPanel() {
		introductionPanel = new JPanel();
		introductionPanel.setLayout(null);

		JLabel lblWelcome = new JLabel("Welcome Administrator");
		JLabel lblSelectAction = new JLabel("Please select an action to the left");

		lblSelectAction.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSelectAction.setHorizontalAlignment(SwingConstants.CENTER);
		lblSelectAction.setBounds(12, 46, 402, 26);

		lblWelcome.setBounds(117, 13, 192, 16);
		lblWelcome.setFont(new Font("Tahoma", Font.BOLD, 16));

		introductionPanel.add(lblSelectAction);
		introductionPanel.add(lblWelcome);

		contentPanel.add(introductionPanel, "name_129256310322500");
	}

	private void initEditProjectPanel() {
		editProjectPanel = new JPanel();
		editProjectPanel.setLayout(null);

		JLabel lblEditProject = new JLabel("Edit project");
		JLabel lblExplaination = new JLabel("Please select a project to edit");
		btnConfirmEPP = new JButton("Confirm");

		btnConfirmEPP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnConfirmEPP.setBounds(132, 350, 165, 44);

		lblEditProject.setHorizontalAlignment(SwingConstants.CENTER);
		lblEditProject.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblEditProject.setBounds(137, 13, 151, 16);

		lblExplaination.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplaination.setBounds(122, 42, 181, 16);

		editProjectPanel.add(lblExplaination);
		editProjectPanel.add(lblEditProject);
		editProjectPanel.add(btnConfirmEPP);

		contentPanel.add(editProjectPanel, "name_128686731998100");
	}

	private void initFrame() {
		frameAdmin = new JFrame();
		frameAdmin.setTitle("Project Planner");
		frameAdmin.setBounds(100, 100, 720, 480);
		frameAdmin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameAdmin.getContentPane().setLayout(null);
	}

	public JFrame getFrame(String type) {
		if (type.equals("Main"))
			return frameMain;
		else if (type.equals("Admin"))
			return frameAdmin;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JPanel getPanel(String panel) {
		if (panel.equals("contentPanel"))
			return contentPanel;
		else if (panel.equals("buttonPanel"))
			return buttonPanel;
		else if (panel.equals("introductionPanel"))
			return introductionPanel;
		else if (panel.equals("addProjectPanel"))
			return addProjectPanel;
		else if (panel.equals("editProjectPanel"))
			return editProjectPanel;
		else if (panel.equals("appointProjectLeaderPanel"))
			return appointProjectLeaderPanel;
		else if (panel.equals("addEmployeePanel"))
			return addEmployeePanel;
		else if (panel.equals("removeEmployeePanel"))
			return removeEmployeePanel;
		else if (panel.equals("removeProjectPanel"))
			return removeProjectPanel;
		else if (panel.equals("projectSelectionPanel"))
			return projectSelectionPanel;
		else if (panel.equals("employeeSelectionPanel"))
			return employeeSelectionPanel;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}

	}

	public JTextField getTextField(String type) {
		if (type.equals("Project"))
			return txtfldProjectName;
		else if (type.equals("Employee"))
			return txtfldEmployeeName;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JButton getButton(String button) {
		if (button.equals("btnEditProject"))
			return btnEditProject;
		else if (button.equals("btnAppointProjectLeader"))
			return btnAppointProjectLeader;
		else if (button.equals("btnAddNewProject"))
			return btnAddNewProject;
		else if (button.equals("btnRemoveProject"))
			return btnRemoveProject;
		else if (button.equals("btnAddNewEmployee"))
			return btnAddNewEmployee;
		else if (button.equals("btnRemoveEmployee"))
			return btnRemoveEmployee;
		else if (button.equals("btnLogout"))
			return btnLogout;
		else if (button.equals("btnConfirmREP"))
			return btnConfirmREP;
		else if (button.equals("btnConfirmRPP"))
			return btnConfirmRPP;
		else if (button.equals("btnConfirmAEP"))
			return btnConfirmAEP;
		else if (button.equals("btnConfirmAPLP"))
			return btnConfirmAPLP;
		else if (button.equals("btnReturnToProjectList"))
			return btnReturnToProjectList;
		else if (button.equals("btnConfirmAPP"))
			return btnConfirmAPP;
		else if (button.equals("btnConfirmEPP"))
			return btnConfirmEPP;
		else if (button.equals("btnConfirmAETA"))
			return btnConfirmAETA;
		else if (button.equals("btnGoBackAETA"))
			return btnGoBackAETA;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public ProjectList getProjectList() {
		return projectList;
	}

	public EmployeeList getEmployeeList() {
		return employeeList;
	}
}
