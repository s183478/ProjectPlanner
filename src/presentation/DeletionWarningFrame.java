package presentation;

import controllers.AdminButtonController;
import domain.Project;
import domain.Employee;

import javax.swing.JFrame;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;

public class DeletionWarningFrame {
	/*
	 * Written by:
	 * s183478, Tobias Munch
	 */

	private AdminButtonController controller;

	private JFrame frameDeletionWarning;
	private JTextField txtfldProjectName, txtfldProjectID, txtfldEmployeeInitials;

	private JPanel panelProjectWarning, panelEmployeeWarning;

	private JButton btnConfirmDeletion;
	private Project project;
	private Employee employee;

	private boolean projectToBeDeleted;

	public DeletionWarningFrame(AdminButtonController controller, Project project) {
		projectToBeDeleted = true;
		initializeProject();
		this.controller = controller;
		btnConfirmDeletion.addActionListener(controller);
	}
	public DeletionWarningFrame(AdminButtonController controller, Employee employee) {
		projectToBeDeleted = false;
		initializeEmployee();
		this.controller = controller;
		btnConfirmDeletion.addActionListener(controller);
	}

	private void initializeProject() {
		initFrame();
		initProjectWarningPanel();
		initButtons(panelProjectWarning);
	}

	private void initializeEmployee() {
		initFrame();
		initEmployeeWarningPanel();
		initButtons(panelEmployeeWarning);

	}

	private void initEmployeeWarningPanel() {
		panelEmployeeWarning = new JPanel();
		panelEmployeeWarning.setLayout(null);
		frameDeletionWarning.getContentPane().add(panelEmployeeWarning, "name_823442664755400");

		JLabel lblWarningEmployee = new JLabel("Warning!");
		JLabel lblExplainerEmployee = new JLabel("You are about to delete an employee");
		JLabel lblConfirmMessage = new JLabel("Please type the initials of the employee you wish to delete");
		JLabel lblEmployeeInitials = new JLabel("Initials:");
		txtfldEmployeeInitials = new JTextField();

		lblWarningEmployee.setHorizontalAlignment(SwingConstants.CENTER);
		lblWarningEmployee.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblWarningEmployee.setBounds(90, 13, 251, 29);

		lblExplainerEmployee.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainerEmployee.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblExplainerEmployee.setBounds(20, 48, 391, 16);

		lblConfirmMessage.setHorizontalAlignment(SwingConstants.CENTER);
		lblConfirmMessage.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblConfirmMessage.setBounds(20, 77, 399, 16);

		lblEmployeeInitials.setFont(new Font("Tahoma", Font.ITALIC, 15));
		lblEmployeeInitials.setBounds(90, 152, 79, 16);

		txtfldEmployeeInitials.setBounds(194, 150, 146, 22);
		txtfldEmployeeInitials.setColumns(10);

		panelEmployeeWarning.add(lblWarningEmployee);
		panelEmployeeWarning.add(lblExplainerEmployee);
		panelEmployeeWarning.add(lblConfirmMessage);
		panelEmployeeWarning.add(lblEmployeeInitials);
		panelEmployeeWarning.add(txtfldEmployeeInitials);
	}

	private void initProjectWarningPanel() {
		panelProjectWarning = new JPanel();
		panelProjectWarning.setLayout(null);
		frameDeletionWarning.getContentPane().add(panelProjectWarning, "name_823440747242300");

		JLabel lblWarningProject = new JLabel("Warning!");
		JLabel lblExplainer = new JLabel("You are about to delete a project");
		JLabel lblConfirmMessage_1 = new JLabel("Please type the name and the serial number of the project");
		JLabel lblName = new JLabel("Name:");
		JLabel lblConfirmMessage_2 = new JLabel("you want to delete");
		JLabel lblSerialNumber = new JLabel("Serial number:");
		txtfldProjectName = new JTextField();
		txtfldProjectID = new JTextField();

		lblWarningProject.setHorizontalAlignment(SwingConstants.CENTER);
		lblWarningProject.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblWarningProject.setBounds(90, 13, 251, 29);

		lblExplainer.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainer.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblExplainer.setBounds(20, 48, 391, 16);

		lblConfirmMessage_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblConfirmMessage_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblConfirmMessage_1.setBounds(20, 77, 391, 16);

		lblName.setFont(new Font("Tahoma", Font.ITALIC, 15));
		lblName.setBounds(69, 137, 114, 16);

		lblConfirmMessage_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblConfirmMessage_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblConfirmMessage_2.setBounds(20, 93, 391, 16);

		lblSerialNumber.setFont(new Font("Tahoma", Font.ITALIC, 15));
		lblSerialNumber.setBounds(69, 166, 114, 16);

		txtfldProjectName.setBounds(180, 135, 186, 22);
		txtfldProjectName.setColumns(10);

		txtfldProjectID.setBounds(180, 164, 186, 22);
		txtfldProjectID.setColumns(10);

		panelProjectWarning.add(lblWarningProject);
		panelProjectWarning.add(lblExplainer);
		panelProjectWarning.add(lblConfirmMessage_1);
		panelProjectWarning.add(lblName);
		panelProjectWarning.add(lblConfirmMessage_2);
		panelProjectWarning.add(lblSerialNumber);
		panelProjectWarning.add(txtfldProjectName);
		panelProjectWarning.add(txtfldProjectID);
	}

	private void initButtons(JPanel panel) {
		btnConfirmDeletion = new JButton("Confirm");

		btnConfirmDeletion.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnConfirmDeletion.setBounds(149, 211, 134, 29);

		panel.add(btnConfirmDeletion);
	}

	private void initFrame() {
		frameDeletionWarning = new JFrame();
		frameDeletionWarning.setBounds(100, 100, 450, 300);
		frameDeletionWarning.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frameDeletionWarning.getContentPane().setLayout(new CardLayout(0, 0));
	}

	public JButton getButton() {
		return btnConfirmDeletion;
	}

	public JFrame getFrame() {
		return frameDeletionWarning;
	}

	public boolean isProjectToBeDeleted() {
		return projectToBeDeleted;
	}
	public JTextField getTextfield(String type) {
		if (type.equals("txtfldProjectName"))
			return txtfldProjectName;
		else if (type.equals("txtfldProjectID"))
			return txtfldProjectID;
		else if (type.equals("txtfldEmployeeInitials"))
			return txtfldEmployeeInitials;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}
}
