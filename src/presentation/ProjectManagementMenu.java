package presentation;

import controllers.ProManButtonController;
import domain.Activity;
import domain.Employee;
import domain.Project;
import java.awt.CardLayout;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import app.DateTime;
import app.PlannerApp;

import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.ListSelectionModel;

public class ProjectManagementMenu {
	/*
	 * Written by: s183478, Tobias Munch
	 */

	private JFrame frameProMan, frameParent; // Either frameEmployee or frameAdmin

	private JPanel buttonPanel, contentPanel, employeeSelectionPanel, introductionPanel, createTimeUsageReportPanel,
			editDetailsPanel, addExpectedTimePanel, addActivityPanel, checkAvailabilityPanel,
			addEmployeeToActivityPanel, activitySelectionPanel, activityTimePanel;

	private JTextField txtfldNameAAP, txtfldAllocatedHours, txtfldProjectName, txtfldProjectID, txtfldYearSD,
			txtfldWeekSD, txtfldHourSD, txtfldMinuteSD, txtfldYearED, txtfldWeekED, txtfldHourED, txtfldMinuteED;

	private JButton btnCreateTimeUsageReport, btnAddExpectedTime, btnAddActivity, btnCheckAvailableEmployees,
			btnAddEmployeeToActivity, btnGoBack, btnEditDetails, btnConfirmEDP, btnConfirmAAP, btnConfirmAETP,
			btnGoBackAETP, btnGoBackAETA, btnConfirmAETA;

	private JList availableEmployeeJList, timeReportJList;

	private JComboBox cmbBoxWeekdaySD, cmbBoxWeekdayED;

	private JCheckBox chckbxAllocatedHoursAAP, chckbxRunningTimeAAP, chckbxAllocatedHoursAETP, chckbxRunningTimeAETP;

	private boolean adminLoggedIn;
	private PlannerApp plannerApp;
	private ObjectList employeeList, activityList;
	private Project parentProject;
	private ProManButtonController controller;

	private JScrollPane timeReportScrollPane;

	public ProjectManagementMenu(JFrame frameParent, PlannerApp plannerApp, Employee loggedInEmployee, Project project,
			boolean adminLoggedIn) {
		this.frameParent = frameParent;
		this.adminLoggedIn = adminLoggedIn;
		this.plannerApp = plannerApp;
		this.parentProject = project;

		employeeList = new ObjectList().newEmployeeList(plannerApp);
		activityList = new ObjectList().newActivityListForProject(project);

		initialize();
		controller = new ProManButtonController(this, plannerApp, loggedInEmployee, project);
		addActionListeners();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		initFrame();
		initAuxiliaryPanels();
		initPanels();
		initMenuButtons();
	}

	private void initAuxiliaryPanels() {
		initActivityTimePanel();
		activitySelectionPanel = new JPanel();
		employeeSelectionPanel = new JPanel();
		JScrollPane activityScrollPane = activityList.getScroller();
		JScrollPane employeeScrollPane = employeeList.getScroller();

		employeeScrollPane.setBounds(0, 0, 408, 342);
		employeeSelectionPanel.setBounds(272, 110, 406, 243);
		employeeSelectionPanel.setLayout(new CardLayout(0, 0));
		employeeSelectionPanel.add(employeeScrollPane);

		activityScrollPane.setBounds(0, 0, 408, 342);
		activitySelectionPanel.setBounds(272, 110, 406, 243);
		activitySelectionPanel.setLayout(new CardLayout(0, 0));
		activitySelectionPanel.add(activityScrollPane);

		frameProMan.getContentPane().add(activitySelectionPanel);
		frameProMan.getContentPane().add(employeeSelectionPanel);
		employeeSelectionPanel.setVisible(false);
		activitySelectionPanel.setVisible(false);

	}

	private void initActivityTimePanel() {
		activityTimePanel = new JPanel();
		activityTimePanel.setLayout(null);
		activityTimePanel.setBounds(275, 171, 400, 182);

		JLabel lblRunningTime = new JLabel("Running time");
		JLabel lblAllocatedHours = new JLabel("Allocated hours:");
		JLabel lblYear = new JLabel("Year");
		JLabel lblWeek = new JLabel(" Week*");
		JLabel lblDayOfWeek = new JLabel("Weekday");
		JLabel lblHour = new JLabel("Hour");
		JLabel lblMinute = new JLabel("Minute");
		cmbBoxWeekdaySD = new JComboBox();
		cmbBoxWeekdayED = new JComboBox();
		txtfldAllocatedHours = new JTextField();
		txtfldYearSD = new JTextField();
		txtfldWeekSD = new JTextField();
		txtfldHourSD = new JTextField();
		txtfldMinuteSD = new JTextField();
		txtfldYearED = new JTextField();
		txtfldWeekED = new JTextField();
		txtfldHourED = new JTextField();
		txtfldMinuteED = new JTextField();

		txtfldAllocatedHours.setBounds(227, 0, 79, 22);
		txtfldAllocatedHours.setColumns(10);

		cmbBoxWeekdaySD.setModel(new DefaultComboBoxModel(
				new String[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" }));
		cmbBoxWeekdaySD.setSelectedIndex(0);
		cmbBoxWeekdaySD.setBounds(55, 110, 116, 22);

		cmbBoxWeekdayED.setModel(new DefaultComboBoxModel(
				new String[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" }));
		cmbBoxWeekdayED.setSelectedIndex(0);
		cmbBoxWeekdayED.setBounds(231, 110, 116, 22);

		txtfldYearSD.setBounds(55, 60, 116, 22);
		txtfldYearSD.setColumns(10);

		txtfldYearED.setColumns(10);
		txtfldYearED.setBounds(231, 60, 116, 22);

		txtfldWeekSD.setBounds(55, 85, 116, 22);
		txtfldWeekSD.setColumns(10);

		txtfldWeekED.setColumns(10);
		txtfldWeekED.setBounds(231, 85, 116, 22);

		txtfldHourSD.setBounds(55, 135, 116, 22);
		txtfldHourSD.setColumns(10);

		txtfldHourED.setColumns(10);
		txtfldHourED.setBounds(231, 135, 116, 22);

		txtfldMinuteSD.setBounds(55, 160, 116, 22);
		txtfldMinuteSD.setColumns(10);

		txtfldMinuteED.setColumns(10);
		txtfldMinuteED.setBounds(231, 160, 116, 22);

		lblRunningTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblRunningTime.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblRunningTime.setBounds(124, 35, 152, 16);

		lblAllocatedHours.setHorizontalAlignment(SwingConstants.TRAILING);
		lblAllocatedHours.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAllocatedHours.setBounds(93, 0, 116, 22);

		lblYear.setHorizontalAlignment(SwingConstants.CENTER);
		lblYear.setBounds(172, 60, 56, 22);

		lblWeek.setHorizontalAlignment(SwingConstants.CENTER);
		lblWeek.setBounds(172, 85, 56, 22);

		lblDayOfWeek.setHorizontalAlignment(SwingConstants.CENTER);
		lblDayOfWeek.setBounds(172, 110, 56, 22);

		lblHour.setHorizontalAlignment(SwingConstants.CENTER);
		lblHour.setBounds(172, 135, 56, 22);

		lblMinute.setHorizontalAlignment(SwingConstants.CENTER);
		lblMinute.setBounds(172, 160, 56, 22);

		activityTimePanel.add(txtfldAllocatedHours);
		activityTimePanel.add(txtfldYearSD);
		activityTimePanel.add(txtfldWeekSD);
		activityTimePanel.add(cmbBoxWeekdaySD);
		activityTimePanel.add(txtfldHourSD);
		activityTimePanel.add(txtfldMinuteSD);
		activityTimePanel.add(txtfldYearED);
		activityTimePanel.add(txtfldWeekED);
		activityTimePanel.add(cmbBoxWeekdayED);
		activityTimePanel.add(txtfldHourED);
		activityTimePanel.add(txtfldMinuteED);
		activityTimePanel.add(lblRunningTime);
		activityTimePanel.add(lblAllocatedHours);
		activityTimePanel.add(lblYear);
		activityTimePanel.add(lblWeek);
		activityTimePanel.add(lblDayOfWeek);
		activityTimePanel.add(lblHour);
		activityTimePanel.add(lblMinute);

		frameProMan.getContentPane().add(activityTimePanel);
		activityTimePanel.setVisible(false);

	}

	private void initMenuButtons() {
		btnCreateTimeUsageReport = new JButton("Create time usage report");
		btnAddExpectedTime = new JButton("<html>Add expected time <br />usage to activity<html>");
		btnAddActivity = new JButton("Add activity");
		btnCheckAvailableEmployees = new JButton("<html>Check availability<br \\>of employees<html>");
		btnAddEmployeeToActivity = new JButton("Add employee to activity");
		btnGoBack = new JButton("Go back");
		btnEditDetails = new JButton("Edit details");

		btnCreateTimeUsageReport.setBounds(19, 13, 197, 40);
		btnAddExpectedTime.setBounds(19, 66, 197, 42);
		btnAddActivity.setBounds(19, 121, 197, 40);
		btnCheckAvailableEmployees.setBounds(19, 174, 197, 40);
		btnAddEmployeeToActivity.setBounds(19, 227, 197, 40);
		btnGoBack.setBounds(53, 354, 130, 40);

		if (adminLoggedIn) {
			btnEditDetails.setBounds(19, 280, 197, 40);
			buttonPanel.add(btnEditDetails);
		}

		buttonPanel.add(btnGoBack);
		buttonPanel.add(btnCheckAvailableEmployees);
		buttonPanel.add(btnAddActivity);
		buttonPanel.add(btnCreateTimeUsageReport);
		buttonPanel.add(btnAddExpectedTime);
		buttonPanel.add(btnAddEmployeeToActivity);
	}

	private void addActionListeners() {

		btnCreateTimeUsageReport.addActionListener(controller);
		btnAddExpectedTime.addActionListener(controller);
		btnAddActivity.addActionListener(controller);
		btnCheckAvailableEmployees.addActionListener(controller);
		btnAddEmployeeToActivity.addActionListener(controller);
		btnGoBack.addActionListener(controller);
		btnConfirmAAP.addActionListener(controller);
		btnConfirmAETP.addActionListener(controller);
		btnGoBackAETP.addActionListener(controller);
		btnGoBackAETA.addActionListener(controller);
		btnConfirmAETA.addActionListener(controller);
		if (adminLoggedIn) {
			btnEditDetails.addActionListener(controller);
			btnConfirmEDP.addActionListener(controller);
		}
	}

	private void initPanels() {
		buttonPanel = new JPanel();
		contentPanel = new JPanel();

		buttonPanel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		buttonPanel.setBounds(9, 13, 239, 407);
		buttonPanel.setLayout(null);
		frameProMan.getContentPane().add(buttonPanel);

		contentPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		contentPanel.setBounds(260, 13, 430, 407);
		contentPanel.setLayout(new CardLayout(0, 0));
		frameProMan.getContentPane().add(contentPanel);

		initIntroductionPanel();
		initCreateTimeUsageReportPanel();
		initAddExpectedTimePanel();
		initAddActivityPanel();
		initCheckAvailabilityPanel();
		initAddEmployeeToActivityPanel();
		if (adminLoggedIn) {
			initEditDetailsPanel();
		}
	}

	private void initAddEmployeeToActivityPanel() {
		addEmployeeToActivityPanel = new JPanel();
		addEmployeeToActivityPanel.setLayout(null);
		contentPanel.add(addEmployeeToActivityPanel, "name_300928571857000");

		JLabel lblAddEmployeeToActivity = new JLabel("Add employee to an activity");
		JLabel lblExplainerAETA = new JLabel("Please select an employee, and then an activity to add to");
		btnGoBackAETA = new JButton("Go back");
		btnConfirmAETA = new JButton("Select");

		lblAddEmployeeToActivity.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAddEmployeeToActivity.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddEmployeeToActivity.setBounds(68, 13, 289, 16);

		lblExplainerAETA.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainerAETA.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainerAETA.setBounds(35, 42, 355, 16);

		btnGoBackAETA.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnGoBackAETA.setBounds(52, 355, 122, 35);

		btnConfirmAETA.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnConfirmAETA.setBounds(239, 355, 122, 35);

		addEmployeeToActivityPanel.add(lblExplainerAETA);
		addEmployeeToActivityPanel.add(lblAddEmployeeToActivity);
		addEmployeeToActivityPanel.add(btnGoBackAETA);
		addEmployeeToActivityPanel.add(btnConfirmAETA);
	}

	private void initIntroductionPanel() {
		introductionPanel = new JPanel();
		introductionPanel.setLayout(null);

		JLabel lblProManMenu = new JLabel("Project Management Menu");
		JLabel lblSelectAction = new JLabel("Please select an action to the left");

		lblProManMenu.setBounds(90, 13, 245, 16);
		lblProManMenu.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblProManMenu.setHorizontalAlignment(SwingConstants.CENTER);

		lblSelectAction.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSelectAction.setHorizontalAlignment(SwingConstants.CENTER);
		lblSelectAction.setBounds(12, 42, 402, 16);

		introductionPanel.add(lblSelectAction);
		introductionPanel.add(lblProManMenu);

		contentPanel.add(introductionPanel, "name_648835188077500");

	}

	private void initEditDetailsPanel() {
		editDetailsPanel = new JPanel();
		editDetailsPanel.setLayout(null);

		JLabel lblEditDetails = new JLabel("Edit details of project");
		JLabel lblProjectName = new JLabel("Project name:");
		JLabel lblProjectID = new JLabel("Project ID:");
		txtfldProjectName = new JTextField();
		txtfldProjectID = new JTextField();

		lblEditDetails.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblEditDetails.setHorizontalAlignment(SwingConstants.CENTER);
		lblEditDetails.setBounds(108, 13, 209, 16);

		lblProjectName.setFont(new Font("Tahoma", Font.ITALIC, 15));
		lblProjectName.setBounds(65, 97, 99, 16);

		lblProjectID.setFont(new Font("Tahoma", Font.ITALIC, 15));
		lblProjectID.setBounds(65, 126, 89, 16);

		txtfldProjectName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtfldProjectName.setBounds(176, 95, 188, 22);
		txtfldProjectName.setColumns(10);
		txtfldProjectName.setText(parentProject.getName());

		txtfldProjectID.setBounds(176, 124, 188, 22);
		txtfldProjectID.setColumns(10);
		txtfldProjectID.setText(parentProject.getId());

		btnConfirmEDP = new JButton("Confirm");
		btnConfirmEDP.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnConfirmEDP.setBounds(135, 334, 155, 35);

		editDetailsPanel.add(lblProjectID);
		editDetailsPanel.add(lblProjectName);
		editDetailsPanel.add(lblEditDetails);
		editDetailsPanel.add(txtfldProjectName);
		editDetailsPanel.add(txtfldProjectID);
		editDetailsPanel.add(btnConfirmEDP);

		contentPanel.add(editDetailsPanel, "name_300925214273200");

	}

	private void initCheckAvailabilityPanel() {
		checkAvailabilityPanel = new JPanel();
		checkAvailabilityPanel.setLayout(null);

		JLabel lblCheckAvailability = new JLabel("Check availability of employees");
		JScrollPane availableEmployeeScrollPane = new JScrollPane();
		refreshAvailableEmployeeJList();

		availableEmployeeJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		lblCheckAvailability.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCheckAvailability.setHorizontalAlignment(SwingConstants.CENTER);
		lblCheckAvailability.setBounds(67, 13, 291, 16);

		availableEmployeeScrollPane.setBounds(67, 95, 291, 295);
		availableEmployeeScrollPane.setViewportView(availableEmployeeJList);

		checkAvailabilityPanel.add(lblCheckAvailability);
		checkAvailabilityPanel.add(availableEmployeeScrollPane);

		contentPanel.add(checkAvailabilityPanel, "name_300926659669100");

		JLabel lblExplainer = new JLabel("List of all employees who are involved in 10 or less activities");
		lblExplainer.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainer.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainer.setBounds(12, 42, 402, 16);
		checkAvailabilityPanel.add(lblExplainer);
	}

	private void initAddActivityPanel() {
		addActivityPanel = new JPanel();
		addActivityPanel.setLayout(null);

		JLabel lblAddActivity = new JLabel("Add a new activity");
		JLabel lblExplainer = new JLabel("Please fill out the forms below to add an activity");
		JLabel lblStarExplainer = new JLabel("* == Obligatory");
		JLabel lblName = new JLabel("Name*:");
		txtfldNameAAP = new JTextField();
		btnConfirmAAP = new JButton("Confirm");

		chckbxRunningTimeAAP = new JCheckBox("Add running time");
		chckbxAllocatedHoursAAP = new JCheckBox("Add allocated hours:");

		lblAddActivity.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAddActivity.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddActivity.setBounds(108, 13, 209, 16);

		lblExplainer.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainer.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainer.setBounds(12, 42, 402, 16);

		btnConfirmAAP.setBounds(152, 355, 122, 35);
		btnConfirmAAP.setFont(new Font("Tahoma", Font.PLAIN, 15));

		lblName.setBounds(108, 103, 70, 16);

		lblStarExplainer.setBounds(154, 71, 117, 16);
		lblStarExplainer.setHorizontalAlignment(SwingConstants.CENTER);
		lblStarExplainer.setFont(new Font("Tahoma", Font.ITALIC, 14));

		txtfldNameAAP.setBounds(159, 100, 163, 22);
		txtfldNameAAP.setColumns(10);

		chckbxRunningTimeAAP.setHorizontalAlignment(SwingConstants.CENTER);
		chckbxRunningTimeAAP.setBounds(225, 128, 161, 25);

		chckbxAllocatedHoursAAP.setHorizontalAlignment(SwingConstants.TRAILING);
		chckbxAllocatedHoursAAP.setBounds(39, 128, 147, 25);

		addActivityPanel.add(lblAddActivity);
		addActivityPanel.add(lblExplainer);
		addActivityPanel.add(lblStarExplainer);
		addActivityPanel.add(lblName);
		addActivityPanel.add(btnConfirmAAP);
		addActivityPanel.add(txtfldNameAAP);
		addActivityPanel.add(chckbxRunningTimeAAP);
		addActivityPanel.add(chckbxAllocatedHoursAAP);

		contentPanel.add(addActivityPanel, "name_300925214273200");

	}

	private void initAddExpectedTimePanel() {
		addExpectedTimePanel = new JPanel();
		addExpectedTimePanel.setLayout(null);

		JLabel lblAddExpectedTime = new JLabel("Add expected time usage to an activity");
		JLabel lblExplainer = new JLabel("Select an activity, and then add expected time usage");
		chckbxAllocatedHoursAETP = new JCheckBox("Add allocated hours:");
		chckbxRunningTimeAETP = new JCheckBox("Add running time");
		btnGoBackAETP = new JButton("Go back");
		btnConfirmAETP = new JButton("Select");

		lblAddExpectedTime.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAddExpectedTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddExpectedTime.setBounds(28, 13, 370, 16);

		lblExplainer.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainer.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainer.setBounds(33, 42, 360, 16);

		chckbxAllocatedHoursAETP.setHorizontalAlignment(SwingConstants.TRAILING);
		chckbxAllocatedHoursAETP.setBounds(39, 128, 147, 25);
		chckbxRunningTimeAETP.setHorizontalAlignment(SwingConstants.CENTER);
		chckbxRunningTimeAETP.setBounds(225, 128, 161, 25);

		btnGoBackAETP.setBounds(60, 355, 122, 35);
		btnGoBackAETP.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnConfirmAETP.setBounds(242, 355, 122, 35);
		btnConfirmAETP.setFont(new Font("Tahoma", Font.PLAIN, 15));

		addExpectedTimePanel.add(lblAddExpectedTime);
		addExpectedTimePanel.add(lblExplainer);
		addExpectedTimePanel.add(chckbxAllocatedHoursAETP);
		addExpectedTimePanel.add(chckbxRunningTimeAETP);
		addExpectedTimePanel.add(btnGoBackAETP);
		addExpectedTimePanel.add(btnConfirmAETP);

		contentPanel.add(addExpectedTimePanel, "name_300923688620800");
	}

	private void initCreateTimeUsageReportPanel() {
		createTimeUsageReportPanel = new JPanel();
		createNewReport();
		createTimeUsageReportPanel.setLayout(null);

		JLabel lblCreateTimeUsageReport = new JLabel("Create time usage report of project");
		timeReportScrollPane = new JScrollPane();
		timeReportScrollPane.setSize(291, 295);
		timeReportScrollPane.setLocation(67, 95);
		refreshTimeReportJList();

		timeReportJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		timeReportScrollPane.setViewportView(timeReportJList);

		lblCreateTimeUsageReport.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCreateTimeUsageReport.setHorizontalAlignment(SwingConstants.CENTER);
		lblCreateTimeUsageReport.setBounds(60, 13, 305, 16);

		createTimeUsageReportPanel.add(lblCreateTimeUsageReport);
		createTimeUsageReportPanel.add(timeReportScrollPane);

		contentPanel.add(createTimeUsageReportPanel, "name_300917435941000");

		JLabel lblExplainerCTUR = new JLabel("Generate a time report over all activities in the project");
		lblExplainerCTUR.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainerCTUR.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainerCTUR.setBounds(12, 42, 402, 16);
		createTimeUsageReportPanel.add(lblExplainerCTUR);
	}

	private void initFrame() {
		frameProMan = new JFrame();
		frameProMan.setTitle("Project Planner");
		frameProMan.setBounds(100, 100, 720, 480);
		frameProMan.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameProMan.getContentPane().setLayout(null);
	}

	public JFrame getFrame(String type) {
		if (type.equals("ProMan"))
			return frameProMan;
		else if (type.equals("Parent"))
			return frameParent;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JButton getButton(String type) {
		if (type.equals("btnCreateTimeUsageReport"))
			return btnCreateTimeUsageReport;
		else if (type.equals("btnAddExpectedTime"))
			return btnAddExpectedTime;
		else if (type.equals("btnAddActivity"))
			return btnAddActivity;
		else if (type.equals("btnCheckAvailableEmployees"))
			return btnCheckAvailableEmployees;
		else if (type.equals("btnAddEmployeeToActivity"))
			return btnAddEmployeeToActivity;
		else if (type.equals("btnGoBack"))
			return btnGoBack;
		else if (type.equals("btnEditDetails"))
			return btnEditDetails;
		else if (type.equals("btnConfirmEDP"))
			return btnConfirmEDP;
		else if (type.equals("btnConfirmAAP"))
			return btnConfirmAAP;
		else if (type.equals("btnConfirmAET"))
			return btnConfirmAETP;
		else if (type.equals("btnGoBackAET"))
			return btnGoBackAETP;
		else if (type.equals("btnGoBackAETA"))
			return btnGoBackAETA;
		else if (type.equals("btnConfirmAETA"))
			return btnConfirmAETA;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JPanel getPanel(String type) {
		if (type.equals("buttonPanel"))
			return buttonPanel;
		else if (type.equals("contentPanel"))
			return contentPanel;
		else if (type.equals("employeeSelectionPanel"))
			return employeeSelectionPanel;
		else if (type.equals("introductionPanel"))
			return introductionPanel;
		else if (type.equals("createTimeUsageReportPanel"))
			return createTimeUsageReportPanel;
		else if (type.equals("editDetailsPanel"))
			return editDetailsPanel;
		else if (type.equals("addExpectedTimePanel"))
			return addExpectedTimePanel;
		else if (type.equals("addActivityPanel"))
			return addActivityPanel;
		else if (type.equals("checkAvailabilityPanel"))
			return checkAvailabilityPanel;
		else if (type.equals("addEmployeeToActivityPanel"))
			return addEmployeeToActivityPanel;
		else if (type.equals("activitySelectionPanel"))
			return activitySelectionPanel;
		else if (type.equals("activityTimePanel"))
			return activityTimePanel;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JTextField getTextField(String type) {
		if (type.equals("txtfldNameAAP"))
			return txtfldNameAAP;
		else if (type.equals("txtfldAllocatedHours"))
			return txtfldAllocatedHours;
		else if (type.equals("txtfldProjectName"))
			return txtfldProjectName;
		else if (type.equals("txtfldProjectID"))
			return txtfldProjectID;
		else if (type.equals("txtfldNameAAP"))
			return txtfldNameAAP;
		else if (type.equals("txtfldAllocatedHours"))
			return txtfldAllocatedHours;
		else if (type.equals("txtfldYearSD"))
			return txtfldYearSD;
		else if (type.equals("txtfldWeekSD"))
			return txtfldWeekSD;
		else if (type.equals("txtfldHourSD"))
			return txtfldHourSD;
		else if (type.equals("txtfldMinuteSD"))
			return txtfldMinuteSD;
		else if (type.equals("txtfldYearED"))
			return txtfldYearED;
		else if (type.equals("txtfldWeekED"))
			return txtfldWeekED;
		else if (type.equals("txtfldHourED"))
			return txtfldHourED;
		else if (type.equals("txtfldMinuteED"))
			return txtfldMinuteED;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}

	}

	public JComboBox getComboBox(String type) {
		if (type.equals("cmbBoxWeekdaySD"))
			return cmbBoxWeekdaySD;
		else if (type.equals("cmbBoxWeekdayED"))
			return cmbBoxWeekdayED;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JCheckBox getCheckBox(String type) {
		if (type.equals("chckbxAllocatedHoursAAP"))
			return chckbxAllocatedHoursAAP;
		else if (type.equals("chckbxRunningTimeAAP"))
			return chckbxRunningTimeAAP;
		else if (type.equals("chckbxAllocatedHoursAETP"))
			return chckbxAllocatedHoursAETP;
		else if (type.equals("chckbxRunningTimeAETP"))
			return chckbxRunningTimeAETP;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public void resetAllTextFields() {
		txtfldYearSD.setText("");
		txtfldYearED.setText("");
		txtfldWeekSD.setText("");
		txtfldWeekED.setText("");
		txtfldHourSD.setText("");
		txtfldHourED.setText("");
		txtfldMinuteSD.setText("");
		txtfldMinuteED.setText("");
		txtfldNameAAP.setText("");
		txtfldAllocatedHours.setText("");
		if (adminLoggedIn) {
			txtfldProjectName.setText("");
			txtfldProjectID.setText("");
		}
	}

	public void untickAllCheckboxes() {
		chckbxAllocatedHoursAAP.setSelected(false);
		chckbxRunningTimeAAP.setSelected(false);
		chckbxAllocatedHoursAETP.setSelected(false);
		chckbxRunningTimeAETP.setSelected(false);
	}

	public ObjectList getEmployeeList() {
		return employeeList;
	}

	public ObjectList getActivityList() {
		return activityList;
	}

	public boolean isAdminLoggedIn() {
		return adminLoggedIn;
	}

	public void refreshAvailableEmployeeJList() {
		availableEmployeeJList = new JList(plannerApp.checkForAvailableWorkers().toArray());
	}

	public void createNewReport() {
		parentProject.createReport();
	}

	public void refreshTimeReportJList() {
		HashMap<Activity, ArrayList<DateTime>> report = parentProject.getReport();
		String[] reportStringArray = new String[report.size()];
		int index = 0;
		for (Activity key : report.keySet()) {
			reportStringArray[index] = key.getName() + ": [ " + report.get(key).get(0).toString() + " ] to [ "
					+ report.get(key).get(1).toString() + " ]";
			index++;
		}
		timeReportJList = new JList(reportStringArray);
		timeReportScrollPane.setViewportView(timeReportJList);
	}
}
