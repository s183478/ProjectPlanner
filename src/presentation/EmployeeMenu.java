package presentation;

import controllers.EmployeeButtonController;
import domain.Employee;
import app.DateTime;
import app.Interval;
import app.OperationNotAllowedException;
import app.PlannerApp;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.swing.JPanel;
import java.awt.CardLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class EmployeeMenu {
	/*
	 * Written by:
	 * s183478, Tobias Munch
	 * s173906, Jonas Weile
	 */
	
	private JFrame frameEmployee;
	private JFrame frameMain;

	private EmployeeButtonController controller;
	private ActivityList activityList;
	private EmployeeList employeeList;
	private IntervalList intervalList;
	private ProjectList projectList;
	private PlannerApp plannerApp;
	private String loggedInEmployeeID;

	private JPanel buttonPanel, contentPanel, employeeSelectionPanel, introductionPanel, registerTimePanel,
			editRegisteredTimePanel, askForHelpPanel, getPersonalTimecardPanel, manageProjectPanel,
			projectSelectionPanel, activitySelectionPanel, editRegisteredTimeTextfieldPanel, timecardPanel,
			intervalSelectionPanel;

	private JButton btnRegisterTime, btnEditRegisteredTime, btnAskForHelp, btnManageProject, btnGetPersonalTimecard,
			btnLogout, btnConfirmMPP, btnConfirmRTP, btnGoBackRTP, btnConfirmAFH, btnGoBackAFH, btnConfirmERTP,
			btnGoBackERTP;

	private JTextField txtfldYearSD, txtfldYearED, txtfldWeekSD, txtfldWeekED, txtfldHourSD, txtfldHourED,
			txtfldMinuteSD, txtfldMinuteED;

	private JTextField txtfldEditYearSD, txtfldEditYearED, txtfldEditWeekSD, txtfldEditWeekED, txtfldEditHourSD,
			txtfldEditHourED, txtfldEditMinuteSD, txtfldEditMinuteED;

	private JComboBox cmbBoxWeekdaySD, cmbBoxWeekdayED, cmbBoxEditWeekdaySD, cmbBoxEditWeekdayED;

	private JLabel lblExplainationERTP, lblExplainationRTP, lblExplainationAFH;
	private JLabel lblExplainerProjectActivity;

	/**
	 * Create the application.
	 * 
	 * @throws OperationNotAllowedException
	 */
	public EmployeeMenu(JFrame main, PlannerApp plannerApp, Employee loggedInEmployee)
			throws OperationNotAllowedException {
		this.frameMain = main;
		this.plannerApp = plannerApp;
		this.loggedInEmployeeID = loggedInEmployee.getId();

		projectList = new ObjectList().newProjectList(plannerApp);
		activityList = new ObjectList().newActivityListForCurrentUser(plannerApp);
		intervalList = new ObjectList().newIntervalListForCurrentUser(plannerApp);
		employeeList = new ObjectList().newEmployeeList(plannerApp);

		initialize();
		controller = new EmployeeButtonController(this, plannerApp, loggedInEmployee);
		addActionListeners();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws OperationNotAllowedException
	 */
	private void initialize() throws OperationNotAllowedException {
		initFrame();
		initAuxiliaryPanels();
		initPanels();
		initMenuButtons();
	}

	private void initPanels() {
		buttonPanel = new JPanel();
		contentPanel = new JPanel();

		buttonPanel.setBounds(9, 13, 239, 407);
		contentPanel.setBounds(260, 13, 430, 407);
		buttonPanel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		contentPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		buttonPanel.setLayout(null);
		contentPanel.setLayout(new CardLayout(0, 0));

		frameEmployee.getContentPane().add(buttonPanel);
		frameEmployee.getContentPane().add(contentPanel);

		initIntroductionPanel();

		initRegisterTimePanel();
		initEditRegisteredTimePanel();
		initAskForHelp();
		initGetPersonalTimecardPanel();
		initManageProjectPanel();

		contentPanel.add(introductionPanel, "name_294299118331600");
		contentPanel.add(registerTimePanel, "name_294319935022400");

		lblExplainerProjectActivity = new JLabel("Activities you are not assigned to will not be shown");
		lblExplainerProjectActivity.setFont(new Font("Tahoma", Font.ITALIC, 13));
		lblExplainerProjectActivity.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainerProjectActivity.setBounds(12, 75, 402, 16);
		registerTimePanel.add(lblExplainerProjectActivity);
		contentPanel.add(editRegisteredTimePanel, "name_294363391585400");
		contentPanel.add(askForHelpPanel, "name_294403249434800");
		contentPanel.add(getPersonalTimecardPanel, "name_294453246342200");
		contentPanel.add(manageProjectPanel, "name_469040662509400");
	}

	private void initAuxiliaryPanels() throws OperationNotAllowedException {
		initEditRegisteredTimeTextfieldPanel();
		projectSelectionPanel = new JPanel();
		employeeSelectionPanel = new JPanel();
		activitySelectionPanel = new JPanel();
		intervalSelectionPanel = new JPanel();
		timecardPanel = new TimeCard(plannerApp.getCurrentUser());

		JScrollPane projectScrollPane = projectList.getScroller();
		JScrollPane activityScrollPane = activityList.getScroller();
		JScrollPane employeeScrollPane = employeeList.getScroller();
		JScrollPane intervalScrollPane = intervalList.getScroller();

		projectScrollPane.setBounds(0, 0, 408, 342);
		projectSelectionPanel.setBounds(272, 110, 406, 243);
		projectSelectionPanel.setLayout(new CardLayout(0, 0));
		projectSelectionPanel.add(projectScrollPane);

		activityScrollPane.setBounds(0, 0, 408, 342);
		activitySelectionPanel.setBounds(272, 110, 406, 243);
		activitySelectionPanel.setLayout(new CardLayout(0, 0));
		activitySelectionPanel.add(activityScrollPane);

		employeeScrollPane.setBounds(0, 0, 408, 342);
		employeeSelectionPanel.setBounds(272, 110, 406, 243);
		employeeSelectionPanel.setLayout(new CardLayout(0, 0));
		employeeSelectionPanel.add(employeeScrollPane);

		intervalScrollPane.setBounds(0, 0, 408, 342);
		intervalSelectionPanel.setBounds(272, 110, 406, 243);
		intervalSelectionPanel.setLayout(new CardLayout(0, 0));
		intervalSelectionPanel.add(intervalScrollPane);

		timecardPanel.setBounds(272, 50, 406, 342);

		frameEmployee.getContentPane().add(projectSelectionPanel);
		frameEmployee.getContentPane().add(activitySelectionPanel);
		frameEmployee.getContentPane().add(employeeSelectionPanel);
		frameEmployee.getContentPane().add(intervalSelectionPanel);
		frameEmployee.getContentPane().add(timecardPanel);
		frameEmployee.getContentPane().add(editRegisteredTimeTextfieldPanel);
		projectSelectionPanel.setVisible(false);
		activitySelectionPanel.setVisible(false);
		employeeSelectionPanel.setVisible(false);
		intervalSelectionPanel.setVisible(false);
		editRegisteredTimeTextfieldPanel.setVisible(false);
		timecardPanel.setVisible(false);

	}

	private void initManageProjectPanel() {
		manageProjectPanel = new JPanel();
		manageProjectPanel.setLayout(null);

		btnConfirmMPP = new JButton("Confirm");
		JLabel lblManageProjects = new JLabel("Manage projects");
		JLabel lblExplainer = new JLabel("Please select your project");
		JLabel lblLeaderExplainer = new JLabel(
				"<html>Note: This will not work if you are not<br />the leader of the selected project<html>");

		lblManageProjects.setHorizontalAlignment(SwingConstants.CENTER);
		lblManageProjects.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblManageProjects.setBounds(139, 13, 147, 16);

		lblExplainer.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainer.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainer.setBounds(118, 42, 192, 16);

		lblLeaderExplainer.setFont(new Font("Tahoma", Font.ITALIC, 13));
		lblLeaderExplainer.setHorizontalAlignment(SwingConstants.CENTER);
		lblLeaderExplainer.setBounds(6, 63, 414, 28);

		btnConfirmMPP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnConfirmMPP.setBounds(132, 350, 165, 44);

		manageProjectPanel.add(btnConfirmMPP);
		manageProjectPanel.add(lblManageProjects);
		manageProjectPanel.add(lblLeaderExplainer);
		manageProjectPanel.add(lblExplainer);

	}

	private void initGetPersonalTimecardPanel() {
		getPersonalTimecardPanel = new JPanel();
		getPersonalTimecardPanel.setLayout(null);
		JLabel lblPersonalTimecard = new JLabel("Personal Timecard");

		lblPersonalTimecard.setHorizontalAlignment(SwingConstants.CENTER);
		lblPersonalTimecard.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPersonalTimecard.setBounds(125, 13, 175, 16);

		getPersonalTimecardPanel.add(lblPersonalTimecard);
	}

	private void initAskForHelp() {
		askForHelpPanel = new JPanel();
		askForHelpPanel.setLayout(null);

		JLabel lblAskForHelp = new JLabel("Ask coworker for help");
		lblExplainationAFH = new JLabel("Please select the project for which you require help");
		btnConfirmAFH = new JButton("Next");
		btnGoBackAFH = new JButton("Go Back");

		btnConfirmAFH.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnConfirmAFH.setBounds(249, 350, 165, 44);

		btnGoBackAFH.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnGoBackAFH.setBounds(12, 350, 185, 44);

		lblExplainationAFH.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainationAFH.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainationAFH.setBounds(12, 42, 402, 16);

		lblAskForHelp.setHorizontalAlignment(SwingConstants.CENTER);
		lblAskForHelp.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAskForHelp.setBounds(115, 13, 196, 16);

		askForHelpPanel.add(lblAskForHelp);
		askForHelpPanel.add(lblExplainationAFH);
		askForHelpPanel.add(btnConfirmAFH);
		askForHelpPanel.add(btnGoBackAFH);
	}

	private void initEditRegisteredTimePanel() {
		editRegisteredTimePanel = new JPanel();
		editRegisteredTimePanel.setLayout(null);

		JLabel lblEditRegisteredTime = new JLabel("Edit Registered time");
		lblExplainationERTP = new JLabel("Please select a project");
		btnConfirmERTP = new JButton("Next");
		btnGoBackERTP = new JButton("Go Back");

		lblExplainationERTP.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainationERTP.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainationERTP.setBounds(12, 42, 402, 20);

		btnConfirmERTP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnConfirmERTP.setBounds(249, 350, 165, 44);

		btnGoBackERTP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnGoBackERTP.setBounds(12, 350, 185, 44);

		lblEditRegisteredTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblEditRegisteredTime.setBounds(118, 13, 189, 20);
		lblEditRegisteredTime.setFont(new Font("Tahoma", Font.BOLD, 16));

		editRegisteredTimePanel.add(lblEditRegisteredTime);
		editRegisteredTimePanel.add(lblExplainationERTP);
		editRegisteredTimePanel.add(btnConfirmERTP);
		editRegisteredTimePanel.add(btnGoBackERTP);

	}

	private void initRegisterTimePanel() {
		registerTimePanel = new JPanel();
		registerTimePanel.setLayout(null);

		JLabel lblRegisterTime = new JLabel("Register time");
		lblExplainationRTP = new JLabel("Please select a project");
		btnConfirmRTP = new JButton("Next");
		btnGoBackRTP = new JButton("Go Back");

		lblExplainationRTP.setHorizontalAlignment(SwingConstants.CENTER);
		lblExplainationRTP.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblExplainationRTP.setBounds(12, 42, 402, 20);

		btnConfirmRTP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnConfirmRTP.setBounds(249, 350, 165, 44);

		btnGoBackRTP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnGoBackRTP.setBounds(12, 350, 185, 44);

		lblRegisterTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegisterTime.setBounds(118, 13, 189, 20);
		lblRegisterTime.setFont(new Font("Tahoma", Font.BOLD, 16));

		registerTimePanel.add(lblRegisterTime);
		registerTimePanel.add(lblExplainationRTP);
		registerTimePanel.add(btnConfirmRTP);
		registerTimePanel.add(btnGoBackRTP);

	}

	public void setRegisterTimeTextPanelValuesToNow() {
		DateTime now;
		try {
			now = new DateTime.Builder().build();
			Interval interval = new Interval(now, now);
			this.setDefaultEditRegisteredTimeTextPanelValues(interval);
		} catch (OperationNotAllowedException e) {
			// This state should never be reached!
			JOptionPane.showMessageDialog(frameEmployee, "Warning: Interval may be invalid");
		}
	}

	public void setDefaultEditRegisteredTimeTextPanelValues(Interval interval) {
		DateTime startTime = interval.getStartTime();
		DateTime endTime = interval.getEndTime();

		txtfldEditYearSD.setText(String.format("%d", startTime.getYear()));
		txtfldEditYearED.setText(String.format("%d", endTime.getYear()));
		txtfldEditWeekSD.setText(String.format("%02d", startTime.getWeek()));
		txtfldEditWeekED.setText(String.format("%02d", endTime.getWeek()));
		cmbBoxEditWeekdaySD.setSelectedIndex(startTime.getDayOfWeek());
		cmbBoxEditWeekdayED.setSelectedIndex(endTime.getDayOfWeek());
		txtfldEditHourSD.setText(String.format("%02d", startTime.getHour()));
		txtfldEditHourED.setText(String.format("%02d", endTime.getHour()));
		txtfldEditMinuteSD.setText(String.format("%02d", startTime.getMinute()));
		txtfldEditMinuteED.setText(String.format("%02d", endTime.getMinute()));
	}

	private void initEditRegisteredTimeTextfieldPanel() throws OperationNotAllowedException {
		editRegisteredTimeTextfieldPanel = new JPanel();
		editRegisteredTimeTextfieldPanel.setLayout(null);

		JLabel lblStartDate = new JLabel("Start Time");
		JLabel lblEndDate = new JLabel("End Time");
		JLabel lblYear = new JLabel("Year");
		JLabel lblWeek = new JLabel("  Week*");
		JLabel lblDayOfWeek = new JLabel("Day of week");
		JLabel lblHour = new JLabel("Hour");
		JLabel lblMinute = new JLabel("Minute");
		txtfldEditYearSD = new JTextField();
		txtfldEditYearED = new JTextField();
		txtfldEditWeekSD = new JTextField();
		txtfldEditWeekED = new JTextField();
		cmbBoxEditWeekdaySD = new JComboBox();
		cmbBoxEditWeekdayED = new JComboBox();
		txtfldEditHourSD = new JTextField();
		txtfldEditHourED = new JTextField();
		txtfldEditMinuteSD = new JTextField();
		txtfldEditMinuteED = new JTextField();

		editRegisteredTimeTextfieldPanel.setBounds(272, 110, 406, 243);

		lblStartDate.setFont(new Font("Tahoma", Font.ITALIC, 13));
		lblStartDate.setHorizontalAlignment(SwingConstants.CENTER);
		lblStartDate.setBounds(20, 13, 101, 16);

		lblEndDate.setFont(new Font("Tahoma", Font.ITALIC, 13));
		lblEndDate.setHorizontalAlignment(SwingConstants.CENTER);
		lblEndDate.setBounds(284, 13, 101, 16);

		lblYear.setHorizontalAlignment(SwingConstants.CENTER);
		lblYear.setBounds(178, 42, 56, 16);

		lblWeek.setHorizontalAlignment(SwingConstants.CENTER);
		lblWeek.setBounds(178, 71, 56, 16);

		lblDayOfWeek.setBounds(168, 100, 70, 16);

		lblHour.setHorizontalAlignment(SwingConstants.CENTER);
		lblHour.setBounds(178, 129, 56, 16);

		lblMinute.setHorizontalAlignment(SwingConstants.CENTER);
		lblMinute.setBounds(178, 158, 56, 16);

		txtfldEditYearSD.setBounds(12, 39, 116, 22);
		txtfldEditYearSD.setColumns(10);

		txtfldEditYearED.setBounds(276, 39, 116, 22);
		txtfldEditYearED.setColumns(10);

		txtfldEditWeekSD.setBounds(12, 68, 116, 22);
		txtfldEditWeekSD.setColumns(10);

		txtfldEditWeekED.setBounds(276, 68, 116, 22);
		txtfldEditWeekED.setColumns(10);

		cmbBoxEditWeekdaySD.setModel(new DefaultComboBoxModel(
				new String[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" }));
		cmbBoxEditWeekdaySD.setSelectedIndex(0);
		cmbBoxEditWeekdaySD.setBounds(12, 97, 116, 19);

		cmbBoxEditWeekdayED.setModel(new DefaultComboBoxModel(
				new String[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" }));
		cmbBoxEditWeekdayED.setSelectedIndex(0);
		cmbBoxEditWeekdayED.setBounds(276, 97, 116, 19);

		txtfldEditHourSD.setBounds(12, 126, 116, 22);
		txtfldEditHourSD.setColumns(10);

		txtfldEditHourED.setBounds(276, 126, 116, 22);
		txtfldEditHourED.setColumns(10);

		txtfldEditMinuteSD.setBounds(12, 155, 116, 22);
		txtfldEditMinuteSD.setColumns(10);

		txtfldEditMinuteED.setBounds(276, 155, 116, 22);
		txtfldEditMinuteED.setColumns(10);

		editRegisteredTimeTextfieldPanel.add(lblStartDate);
		editRegisteredTimeTextfieldPanel.add(lblEndDate);
		editRegisteredTimeTextfieldPanel.add(lblYear);
		editRegisteredTimeTextfieldPanel.add(lblWeek);
		editRegisteredTimeTextfieldPanel.add(lblDayOfWeek);
		editRegisteredTimeTextfieldPanel.add(lblHour);
		editRegisteredTimeTextfieldPanel.add(lblMinute);
		editRegisteredTimeTextfieldPanel.add(txtfldEditYearSD);
		editRegisteredTimeTextfieldPanel.add(txtfldEditYearED);
		editRegisteredTimeTextfieldPanel.add(txtfldEditWeekSD);
		editRegisteredTimeTextfieldPanel.add(txtfldEditWeekED);
		editRegisteredTimeTextfieldPanel.add(cmbBoxEditWeekdaySD);
		editRegisteredTimeTextfieldPanel.add(cmbBoxEditWeekdayED);
		editRegisteredTimeTextfieldPanel.add(txtfldEditHourSD);
		editRegisteredTimeTextfieldPanel.add(txtfldEditHourED);
		editRegisteredTimeTextfieldPanel.add(txtfldEditMinuteSD);
		editRegisteredTimeTextfieldPanel.add(txtfldEditMinuteED);

		this.setRegisterTimeTextPanelValuesToNow();
	}

	private void initIntroductionPanel() {
		introductionPanel = new JPanel();
		introductionPanel.setLayout(null);

		JLabel lblWelcome = new JLabel("Welcome " + loggedInEmployeeID);
		JLabel lblSelectAction = new JLabel("Please select an action to the left");

		lblSelectAction.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSelectAction.setHorizontalAlignment(SwingConstants.CENTER);
		lblSelectAction.setBounds(12, 46, 402, 26);

		lblWelcome.setBounds(117, 13, 192, 20);
		lblWelcome.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);

		introductionPanel.add(lblSelectAction);
		introductionPanel.add(lblWelcome);
	}

	private void initMenuButtons() {
		btnRegisterTime = new JButton("Register time");
		btnEditRegisteredTime = new JButton("Edit registered time");
		btnAskForHelp = new JButton("Ask coworker for help");
		btnManageProject = new JButton("Manage project");
		btnGetPersonalTimecard = new JButton("Get personal timecard");
		btnLogout = new JButton("Log out");

		btnRegisterTime.setBounds(32, 13, 172, 45);
		btnEditRegisteredTime.setBounds(32, 71, 172, 45);
		btnAskForHelp.setBounds(32, 129, 172, 45);
		btnManageProject.setBounds(32, 245, 172, 45);
		btnGetPersonalTimecard.setBounds(32, 187, 172, 45);
		btnLogout.setBounds(70, 355, 96, 39);

		buttonPanel.add(btnRegisterTime);
		buttonPanel.add(btnEditRegisteredTime);
		buttonPanel.add(btnAskForHelp);
		buttonPanel.add(btnManageProject);
		buttonPanel.add(btnGetPersonalTimecard);
		buttonPanel.add(btnLogout);
	}

	private void addActionListeners() {
		btnRegisterTime.addActionListener(controller);
		btnEditRegisteredTime.addActionListener(controller);
		btnAskForHelp.addActionListener(controller);
		btnManageProject.addActionListener(controller);
		btnGetPersonalTimecard.addActionListener(controller);
		btnLogout.addActionListener(controller);
		btnConfirmMPP.addActionListener(controller);
		btnConfirmRTP.addActionListener(controller);
		btnGoBackRTP.addActionListener(controller);
		btnConfirmERTP.addActionListener(controller);
		btnGoBackERTP.addActionListener(controller);
		btnConfirmAFH.addActionListener(controller);
		btnGoBackAFH.addActionListener(controller);
	}

	private void initFrame() {
		frameEmployee = new JFrame();
		frameEmployee.setTitle("Project Planner");
		frameEmployee.setBounds(100, 100, 720, 480);
		frameEmployee.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameEmployee.getContentPane().setLayout(null);
	}

	public JFrame getFrame(String type) {
		if (type.equals("Employee"))
			return frameEmployee;
		else if (type.equals("Main"))
			return frameMain;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JPanel getPanel(String type) {
		if (type.equals("buttonPanel"))
			return buttonPanel;
		else if (type.equals("contentPanel"))
			return contentPanel;
		else if (type.equals("introductionPanel"))
			return introductionPanel;
		else if (type.equals("registerTimePanel"))
			return registerTimePanel;
		else if (type.equals("editRegisteredTimePanel"))
			return editRegisteredTimePanel;
		else if (type.equals("askForHelpPanel"))
			return askForHelpPanel;
		else if (type.equals("getPersonalTimecardPanel"))
			return getPersonalTimecardPanel;
		else if (type.equals("manageProjectPanel"))
			return manageProjectPanel;
		else if (type.equals("projectSelectionPanel"))
			return projectSelectionPanel;
		else if (type.equals("employeeSelectionPanel"))
			return employeeSelectionPanel;
		else if (type.equals("activitySelectionPanel"))
			return activitySelectionPanel;
		else if (type.equals("intervalSelectionPanel"))
			return intervalSelectionPanel;
		else if (type.equals("editRegisteredTimeTextfieldPanel"))
			return editRegisteredTimeTextfieldPanel;
		else if (type.equals("timecardPanel"))
			return timecardPanel;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JButton getButton(String type) {
		if (type.equals("btnRegisterTime"))
			return btnRegisterTime;
		else if (type.equals("btnEditRegisteredTime"))
			return btnEditRegisteredTime;
		else if (type.equals("btnAskForHelp"))
			return btnAskForHelp;
		else if (type.equals("btnManageProject"))
			return btnManageProject;
		else if (type.equals("btnGetPersonalTimecard"))
			return btnGetPersonalTimecard;
		else if (type.equals("btnLogout"))
			return btnLogout;
		else if (type.equals("btnConfirmMPP"))
			return btnConfirmMPP;
		else if (type.equals("btnConfirmRTP"))
			return btnConfirmRTP;
		else if (type.equals("btnGoBackRTP"))
			return btnGoBackRTP;
		else if (type.equals("btnConfirmERTP"))
			return btnConfirmERTP;
		else if (type.equals("btnGoBackERTP"))
			return btnGoBackERTP;
		else if (type.equals("btnConfirmAFH"))
			return btnConfirmAFH;
		else if (type.equals("btnGoBackAFH"))
			return btnGoBackAFH;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JTextField getTextField(String type) {
		if (type.equals("txtfldYearSD"))
			return txtfldYearSD;
		else if (type.equals("txtfldYearED"))
			return txtfldYearED;
		else if (type.equals("txtfldWeekSD"))
			return txtfldWeekSD;
		else if (type.equals("txtfldWeekED"))
			return txtfldWeekED;
		else if (type.equals("txtfldHourSD"))
			return txtfldHourSD;
		else if (type.equals("txtfldHourED"))
			return txtfldHourED;
		else if (type.equals("txtfldMinuteSD"))
			return txtfldMinuteSD;
		else if (type.equals("txtfldMinuteED"))
			return txtfldMinuteED;
		else if (type.equals("txtfldEditYearSD"))
			return txtfldEditYearSD;
		else if (type.equals("txtfldEditYearED"))
			return txtfldEditYearED;
		else if (type.equals("txtfldEditWeekSD"))
			return txtfldEditWeekSD;
		else if (type.equals("txtfldEditWeekED"))
			return txtfldEditWeekED;
		else if (type.equals("txtfldEditHourSD"))
			return txtfldEditHourSD;
		else if (type.equals("txtfldEditHourED"))
			return txtfldEditHourED;
		else if (type.equals("txtfldEditMinuteSD"))
			return txtfldEditMinuteSD;
		else if (type.equals("txtfldEditMinuteED"))
			return txtfldEditMinuteED;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JComboBox getComboBox(String type) {
		if (type.equals("cmbBoxWeekdaySD"))
			return cmbBoxWeekdaySD;
		else if (type.equals("cmbBoxWeekdayED"))
			return cmbBoxWeekdayED;
		else if (type.equals("cmbBoxEditWeekdaySD"))
			return cmbBoxEditWeekdaySD;
		else if (type.equals("cmbBoxEditWeekdayED"))
			return cmbBoxEditWeekdayED;
		else {
			System.out.println("Input may be written wrong");
			return null;
		}
	}

	public JLabel getLabel(String type) {
		if (type.equals("lblExplainationRTP"))
			return lblExplainationRTP;
		else if (type.equals("lblExplainationERTP"))
			return lblExplainationERTP;
		else if (type.equals("lblExplainationAFH"))
			return lblExplainationAFH;
		else
			return null;
	}

	public ProjectList getProjectList() {
		return projectList;
	}

	public ActivityList getActivityList() {
		return activityList;
	}

	public EmployeeList getEmployeeList() {
		return employeeList;
	}

	public IntervalList getIntervalList() {
		return intervalList;
	}

	public void resetTextFields() {
		lblExplainationRTP.setText("Please choose a project");
		lblExplainationERTP.setText("Please choose a project");
		lblExplainationAFH.setText("Please choose a project");
		btnConfirmRTP.setText("Next");
		btnConfirmERTP.setText("Next");
		this.setRegisterTimeTextPanelValuesToNow();
	}
}
