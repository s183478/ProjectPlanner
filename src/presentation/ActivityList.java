package presentation;

import javax.swing.JScrollPane;

import app.PlannerApp;
import domain.Activity;
import domain.Project;

public interface ActivityList {
	
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */

	public ObjectList newActivityListForCurrentUser(PlannerApp planner);
	
	public ObjectList newActivityListForProject(Project project);

	public JScrollPane getScroller();

	public String getChosenActivityName();

	public Activity getChosenActivity();

	public void filterForProject(PlannerApp planner, Project selectedProject);
}
