package presentation;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import app.Interval;
import app.PlannerApp;
import domain.Activity;
import domain.Employee;
import domain.Project;

public class ObjectList<T> implements ProjectList,
									  EmployeeList, 
									  ActivityList, 
									  IntervalList,
									  PropertyChangeListener {
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */

	private ArrayList<T> data;
	private DefaultListModel<T> model;

	private String LIST_TYPE;

	private JScrollPane listScroller;
	private JList<T> objectJList;

	/**
	 * Create the application.
	 */
	public ObjectList() { }


	/**
	 * Initialize the contents of the frame.
	 */

	private void initialize(ArrayList<T> data) {
		this.data = data;
		this.model = new DefaultListModel<T>();
		for (T t : data) {
			this.model.addElement(t);
		}
		this.objectJList = new JList<T>(this.model);
		this.listScroller = new JScrollPane();
		this.listScroller.setViewportView(objectJList);
		this.objectJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.objectJList.setSelectedIndex(0);
	}
	
	
	private void update(ArrayList<T> newData) {
		this.data = newData;
		this.model.clear();
		
		for (T t : data) {
			this.model.addElement(t);
		}

		this.objectJList.setSelectedIndex(0);
	}

	public JScrollPane getScroller() {
		return listScroller;
	}

	public JList<T> getList() {
		return objectJList;
	}


	@Override
	public String getChosenEmployeeId() {
		Employee employee = getChosenEmployee();
		return employee.getId();
	}

	@Override
	public Employee getChosenEmployee() {
		Employee employee = (Employee) objectJList.getSelectedValue();
		return employee;
	}


	@Override
	public String getChosenProjectId() {
		return getChosenProject().getId();
	}

	@Override
	public Project getChosenProject() {
		Project project = (Project) objectJList.getSelectedValue();
		return project;
	}


	@Override
	public String getChosenActivityName() {
		Activity activity = getChosenActivity();
		return activity.getName();
	}


	@Override
	public Activity getChosenActivity() {
		Activity activity = (Activity) objectJList.getSelectedValue();
		return activity;
	}
	
	@Override
	public Interval getChosenInterval() {
		Interval interval = (Interval) objectJList.getSelectedValue();
		return interval;
	}


	@Override
	public ObjectList<T> newEmployeeList(PlannerApp planner) {
		this.LIST_TYPE = "EMPLOYEE";
		ArrayList<T> data = (ArrayList<T>) planner.getEmployeeRepository().getAllEmployeesList().clone();
		
		if (planner.getCurrentUser() != null) {
			data.remove(planner.getCurrentUser());
		}
		
		initialize(data);
		planner.addObserver(this);
		return this;
	}


	@Override
	public ObjectList<T> newProjectList(PlannerApp planner) {
		this.LIST_TYPE = "PROJECT";
		initialize( (ArrayList<T>) planner.getProjectRepository().getAllProjectsList() );
		planner.addObserver(this);
		return this;
	}
	
	@Override
	public ObjectList newAssignedProjectsList(PlannerApp planner) {
		this.LIST_TYPE = "ASSIGNED PROJECTS";
		
		Employee currentEmployee = planner.getCurrentUser();
		
		Predicate<Project> userIsAssigned = p -> p.getActivities().stream()
													.anyMatch(a -> a.isEmployeeAssignedToActivity(currentEmployee) ||
																   a.isEmployeeHelpingOnActivity(currentEmployee));
		
		
		initialize( (ArrayList<T>) planner.getProjectRepository().getAllProjectsStream()
											.filter(userIsAssigned)
											.collect(Collectors.toList()));
		
		planner.addObserver(this);
		return this;
	}


	@Override
	public ObjectList<T> newActivityListForCurrentUser(PlannerApp planner) {
		this.LIST_TYPE = "ACTIVITY";
		ArrayList<T> activities = planner.getAllActivitesForCurrentUser();
		initialize(activities);
		planner.addObserver(this);
		return this;
	}


	@Override
	public ObjectList<T> newActivityListForProject(Project project) {
		this.LIST_TYPE = "ACTIVITY";
		ArrayList<T> activities = (ArrayList<T>) project.getActivities();
		initialize(activities);
		project.addObserver(this);
		return this;
	}

	public ObjectList newIntervalListForCurrentUser(PlannerApp plannerApp) {
		this.LIST_TYPE = "INTERVAL";
		
		ArrayList intervals = new ArrayList();
		
		HashMap<Activity, ArrayList<Interval>> timeCard = plannerApp.getCurrentUser().getTimeCard();
		for ( Map.Entry<Activity, ArrayList<Interval>> entry : timeCard.entrySet() ) {
			entry.getValue().stream()
					.forEach(i -> intervals.add(i));
		}
		
		initialize(intervals);
		return this;
	}
	
	@Override
	public void filterGetRegisteredIntervalsFromActivity(PlannerApp planner, Activity selectedActivity) {
		ArrayList intervals = new ArrayList();
		
		HashMap<Activity, ArrayList<Interval>> timeCard = planner.getCurrentUser().getTimeCard();
		for ( Map.Entry<Activity, ArrayList<Interval>> entry : timeCard.entrySet() ) {
			if (entry.getKey().equals(selectedActivity)) {
				entry.getValue().stream()
					.forEach(i -> intervals.add(i));
			}
		}
		
		update(intervals);
	}
	

	public void filterForProject(PlannerApp planner, Project selectedProject) {
		ArrayList activities = new ArrayList();
		Employee currentUser = planner.getCurrentUser();
		
		activities = (ArrayList) selectedProject.getActivities().stream()
										.filter(a -> a.isEmployeeAssignedToActivity(currentUser)
												 	 || a.isEmployeeHelpingOnActivity(currentUser))
										.collect(Collectors.toList());
		update(activities);
	}


	// Property Change Events
	@Override
	public void propertyChange(PropertyChangeEvent evt) {

		if (evt.getPropertyName() == "ALL") {
			this.model.removeAllElements();
			for (T t : data) {
				this.model.addElement(t);
			}
			return;
		}

		if (evt.getPropertyName() == "ACTIVITY" && this.LIST_TYPE == "ACTIVITY") {
			this.data = ((PlannerApp) evt.getSource()).getAllActivitesForCurrentUser();
		}
		
		if (evt.getPropertyName() == "NEW ACTIVITY" && this.LIST_TYPE == "ACTIVITY") {
			this.model.addElement((T) evt.getNewValue());
		}

		if (evt.getPropertyName() == "NEW PROJECT" && this.LIST_TYPE == "PROJECT") {
			this.model.addElement((T) evt.getNewValue());
		}
		
		if (evt.getPropertyName() == "DELETE PROJECT" && this.LIST_TYPE == "PROJECT") {
			this.model.removeElement((T) evt.getOldValue());
		}

		if (evt.getPropertyName() == "NEW EMPLOYEE" && this.LIST_TYPE == "EMPLOYEE") {
			this.model.addElement((T) evt.getNewValue());
		}
		
		if (evt.getPropertyName() == "DELETE EMPLOYEE" && this.LIST_TYPE == "EMPLOYEE") {
			this.model.removeElement((T) evt.getOldValue());
		}		
		
		if (evt.getPropertyName() == "EMPLOYEE LOGOUT" && this.LIST_TYPE == "EMPLOYEE") {
			System.out.print("noticed logout: ");
			
			Employee prevLoggedIn = (Employee) evt.getOldValue();

			System.out.println(prevLoggedIn);
			
			if (prevLoggedIn != null && (!this.model.contains(prevLoggedIn))) {
				this.model.addElement((T) prevLoggedIn);
			}
			
		this.data.forEach(o -> System.out.println(o));
		}
	}
}
