
package presentation;
 
import javax.swing.ImageIcon;
import javax.swing.JButton;
  
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import app.DateTime;
import app.Interval;
import app.OperationNotAllowedException;
import domain.Activity;
import domain.Employee;
import domain.Project;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.Calendar;
 

public class TimeCard extends JPanel
						  implements ActionListener{
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */
	
    static final private String PREVIOUS_WEEK = "previous";
    static final private String NEXT_WEEK = "next";
    
    private Employee employee;
    private TableModel tableModel;
    private JLabel yearLabel;
    private JLabel weekLabel;
    
     
    public TimeCard(Employee employee) {
   
        super(new BorderLayout());
    	this.employee = employee;
        
        //Create the timeCard table
        JTable table = new JTable(new TableModel(employee)) {
        	// Set tool tip
        	public String getToolTipText(MouseEvent e) {
                String tooltip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                
                Object value = this.getValueAt(rowIndex, colIndex);
                
                if (colIndex > 0 && value != null) {
	                Project project = ((Activity) value).getParentProject();
	                String s;
	                
	                if ( project == null) {
	                	s = "Planner - ";
	                } else {
	                	s = project.getName() + " - ";
	                }
                
                	tooltip = s + value.toString();
                }
                
                return tooltip;
        	}	
        };
        
        this.tableModel = (TableModel) table.getModel();
        
        //Set layout for column 0
        DefaultTableCellRenderer centerAlign = new DefaultTableCellRenderer();
        centerAlign.setHorizontalAlignment( JLabel.CENTER );
        table.getColumnModel().getColumn(0).setCellRenderer( centerAlign );
        table.getColumnModel().getColumn(0).setMaxWidth(50);
        
        table.setFillsViewportHeight(true);
        
        // Stop the user from editing the table
        table.getTableHeader().setResizingAllowed(false);
        table.getTableHeader().setReorderingAllowed(false);
        
        
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane, BorderLayout.CENTER);
 
        JToolBar toolBar = new JToolBar();
        addButtons(toolBar);
        toolBar.setFloatable(false);
        toolBar.setRollover(true);
        add(toolBar, BorderLayout.PAGE_START);
    }
    
    
    protected void addButtons(JToolBar toolBar) {
        JButton button = null;
        JLabel label = null;
 
        // Back button
        button = makeNavigationButton("Back24", PREVIOUS_WEEK,
                                      "Go to previous week",
                                      "Previous Week");
        toolBar.add(button);
        
        // Separator
        toolBar.addSeparator();
        
        // Week Label
        label = new JLabel("Week " + this.tableModel.getChosenWeek());
        toolBar.add(label);
        this.weekLabel = label;
        
        // Separator
        toolBar.addSeparator();
 
        // Forward button
        button = makeNavigationButton("Forward24", NEXT_WEEK,
                                      "Go to next week",
                                      "Next Week");
        toolBar.add(button);
 
        // Separator
        toolBar.addSeparator();
 
        // Year Label
        label = new JLabel("Year " + this.tableModel.getChosenYear());
        toolBar.add(label);
        this.yearLabel = label;
        
         
        // Separator
        toolBar.addSeparator(new Dimension(50, 50));
 
        
        // User Label
        label = new JLabel("Employee: " + this.employee.getId());
        toolBar.add(label);
        
    }
    
 
    protected JButton makeNavigationButton(String imageName,
                                           String actionCommand,
                                           String toolTipText,
                                           String altText) {
        //Look for the image.
        String imgLocation = "images/"
                             + imageName
                             + ".gif";
        
        URL imageURL = TimeCard.class.getResource(imgLocation);
 
        //Create and initialize the button.
        JButton button = new JButton();
        button.setActionCommand(actionCommand);
        button.setToolTipText(toolTipText);
        button.addActionListener(this);
        button.setIcon(new ImageIcon(imageURL, altText));
 
        return button;
    }
 
    
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        
        if (PREVIOUS_WEEK.equals(cmd)) {
            this.tableModel.goToPreviousWeek();
            this.weekLabel.setText("Week " + this.tableModel.getChosenWeek());
            this.yearLabel.setText("Year " + this.tableModel.getChosenYear());
       
        } else if (NEXT_WEEK.equals(cmd)) {
        	this.tableModel.goToNextWeek();
        	this.weekLabel.setText("Week " + this.tableModel.getChosenWeek());
        	this.yearLabel.setText("Year " + this.tableModel.getChosenYear());
        	
        }         

        this.tableModel.fireTableDataChanged();
    }
 
    
    class TableModel extends AbstractTableModel {
    	private int chosenYear = Calendar.getInstance().get(Calendar.YEAR);
		private int chosenWeek = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
		private Employee employee;
		
		public TableModel(Employee employee) {
			this.employee = employee;
		}
		
		public void goToPreviousWeek() {
			this.chosenWeek--;
			
			if ( this.chosenWeek == 0 ) {
				this.chosenWeek = 52;
				this.chosenYear--;
			}
		}
		
		public void goToNextWeek() {
			this.chosenWeek++;
			
			if ( this.chosenWeek == 53 ) {
				this.chosenWeek = 1;
				this.chosenYear++;
			}
		}

		public int getChosenYear() {
			return this.chosenYear;
		}
		
		public void setChosenYear(int year) {
			this.chosenYear = year;
		}
		
		public int getChosenWeek() {
			return this.chosenWeek;
		}
		
		public void setChosenWeek(int week) {
			this.chosenYear = week;
		}
		
		
		private String[] columnNames = {
				"",
                "MON",
                "TUE",
                "WED",
                "THU",
                "FRI",
                "SAT",
                "SUN"
         };
		
		public String getColumnName(int col) {
            return columnNames[col];
		}
		

		@Override
		public int getRowCount() {
			return 48;
		}

		@Override
		public int getColumnCount() {
			return 8;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			if (columnIndex == 0) {
				int startHour = rowIndex / 2;
				int startMinute = (rowIndex % 2) * 30;
				return String.format("%02d:%02d", startHour, startMinute);						
			} else {
				
				Interval interval = null;
				try {
					interval = getIntervalFromIndices(rowIndex, columnIndex);
				} catch (OperationNotAllowedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return employee.getActivityFromInterval(interval);	
			}	
		}
		
		
		private Interval getIntervalFromIndices(int rowIndex, int columnIndex) throws OperationNotAllowedException {
			int year = getChosenYear();
			int week = getChosenWeek();
			int dayOfWeek = columnIndex;
			int hour = rowIndex / 2;
			int minute = (rowIndex % 2) * 30;
					
			DateTime startTime = new DateTime.Builder()
											  .year(year)
											  .week(week)
											  .dayOfWeek(dayOfWeek)
											  .hour(hour)
											  .minute(minute)
											  .build();
			
			minute += 29;
			
			DateTime endTime = new DateTime.Builder()
												 .year(year)
												 .week(week)
												 .dayOfWeek(dayOfWeek)
												 .hour(hour)
												 .minute(minute)
												 .build();
			
			return new Interval(startTime, endTime);
		}		
	}
}
