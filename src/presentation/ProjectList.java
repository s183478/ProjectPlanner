package presentation;

import javax.swing.JScrollPane;

import app.PlannerApp;
import domain.Project;

public interface ProjectList {
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */

	public ObjectList newProjectList(PlannerApp planner);

	public JScrollPane getScroller();

	public String getChosenProjectId();

	public Project getChosenProject();

	public ObjectList newAssignedProjectsList(PlannerApp planner);
}
