package presentation;

import javax.swing.JScrollPane;

import app.Interval;
import app.PlannerApp;
import domain.Activity;

public interface IntervalList {
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */

	JScrollPane getScroller();

	void filterGetRegisteredIntervalsFromActivity(PlannerApp planner, Activity selectedActivity);

	Interval getChosenInterval();

}
