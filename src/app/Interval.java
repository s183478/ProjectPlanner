package app;

public class Interval {
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */
	@Override
	public String toString() {
		return startTime + "    -    " + endTime;
	}


	private DateTime endTime;
	private DateTime startTime;
	
	public Interval(DateTime startTime, DateTime endTime) throws OperationNotAllowedException {
		if( !startTime.isSmallerThanOrEqualTo(endTime) ) {
			throw new OperationNotAllowedException("The given start time is larger than the end time!");	
		}
		
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	// Getters
	public DateTime getEndTime() {
		return this.endTime;
	}
	
	public DateTime getStartTime() {
		return this.startTime;
	}
	
	
	// Setters
	public void setEndTime(DateTime endTime) {
		this.endTime = endTime;
	}
	
	public void setStartTime(DateTime startTime) {
		this.startTime = startTime;
	}
	
	
	// Methods
	public boolean Overlaps(Interval interval) {
		DateTime startTime = interval.getStartTime();
		DateTime endTime = interval.getEndTime();
		
		if ( this.startTime.isLargerThanOrEqualTo(startTime) && 
				this.startTime.isSmallerThanOrEqualTo(endTime) ) {
			return true;
		}
		
		if ( this.endTime.isLargerThanOrEqualTo(startTime) && 
						this.startTime.isSmallerThanOrEqualTo(endTime) ) {
			return true;
		}
		
		if ( startTime.isLargerThanOrEqualTo(this.startTime) && 
				startTime.isSmallerThanOrEqualTo(this.endTime) ) {
			return true;
		}
		
		if ( endTime.isLargerThanOrEqualTo(this.startTime) && 
						startTime.isSmallerThanOrEqualTo(this.endTime) ) {
			return true;
		}
		
		return false;
	}

	public int lengthInWholeHours() {
		int hours = endTime.getHour() - startTime.getHour();
		hours += (endTime.getDayOfWeek() - startTime.getDayOfWeek()) * 24;
		hours += (endTime.getWeek() - startTime.getWeek()) * 7 * 24;
		hours += (endTime.getYear() - startTime.getYear()) * 365 * 24;
		
		return hours;
	}
}
