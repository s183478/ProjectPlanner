package app;

import domain.Activity;
import domain.Employee;
import domain.Project;
import persistence.InMemoryRepository;
import presentation.MainPlannerView;

public class Launcher {
	private static PlannerApp plannerApp;
	private static MainPlannerView mainPlannerView;

	public static void main(String[] args) throws Exception {
		InMemoryRepository repo = new InMemoryRepository();
		
		plannerApp = new PlannerApp(repo, repo);
		plannerApp.clearDatabase();
		
		
		// TODO : For testing
		plannerApp.adminLogin();
		
		Employee leader = plannerApp.registerEmployee("NAWE");
		plannerApp.registerEmployee("JON");
		plannerApp.registerEmployee("KARL");
		Employee mia = plannerApp.registerEmployee("MIA");
		
		Project project1 = plannerApp.newProject("Exam Project");
		project1.appointLeader(leader);
		
		Project project2 = plannerApp.newProject("COVID19 Research");
		project2.appointLeader(leader);
		
		Activity activity1 = project1.addNewActivity("White Box Test");
		Activity activity2 = project1.addNewActivity("Write Report");
		Activity activity3 = project1.addNewActivity("Arrange Zoom Meeting");
		Activity activity4 = project1.addNewActivity("Sample Activity 4");
		
		Activity activity5 = project2.addNewActivity("Blood Samples");
		Activity activity6 = project2.addNewActivity("Lab Work");
		
		
		plannerApp.assignEmployeeToActivity(leader, activity1);
		plannerApp.assignEmployeeToActivity(mia, activity6);
		
		DateTime s1 = new DateTime.Builder().hour(8).minute(0).build();
		DateTime e1 = new DateTime.Builder().hour(16).minute(0).build();
		Interval i1 = new Interval(s1, e1);
		leader.registerTime(activity1, i1);
		
		DateTime s2 = new DateTime.Builder().dayOfWeek(1).hour(8).minute(0).build();
		DateTime e2 = new DateTime.Builder().dayOfWeek(1).hour(17).minute(0).build();
		Interval i2 = new Interval(s2, e2);
		mia.registerTime(activity6, i2);
		
		s2 = new DateTime.Builder().dayOfWeek(2).hour(8).minute(0).build();
		e2 = new DateTime.Builder().dayOfWeek(2).hour(17).minute(0).build();
		i2 = new Interval(s2, e2);
		mia.registerTime(activity6, i2);
		
		s2 = new DateTime.Builder().dayOfWeek(3).hour(8).minute(0).build();
		e2 = new DateTime.Builder().dayOfWeek(3).hour(17).minute(0).build();
		i2 = new Interval(s2, e2);
		mia.registerTime(activity6, i2);
		
		s2 = new DateTime.Builder().dayOfWeek(4).hour(8).minute(0).build();
		e2 = new DateTime.Builder().dayOfWeek(4).hour(17).minute(0).build();
		i2 = new Interval(s2, e2);
		mia.registerTime(activity6, i2);
		
		s2 = new DateTime.Builder().dayOfWeek(5).hour(8).minute(0).build();
		e2 = new DateTime.Builder().dayOfWeek(5).hour(17).minute(0).build();
		i2 = new Interval(s2, e2);
		mia.registerTime(activity6, i2);
		
		plannerApp.adminLogout();
		////////////////////////////
		
		
		
		mainPlannerView = new MainPlannerView(plannerApp);
		mainPlannerView.getFrame().setVisible(true);
		
	}

}
