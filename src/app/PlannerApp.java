package app;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.stream.Collectors;

import domain.Activity;
import domain.Employee;
import domain.Project;

public class PlannerApp {
	/*
	 * Written by:
	 * L.Noer s155490
	 * Oscar J�rgensen s173470
	 * s173906, Jonas Weile
	 */
	
	// Observer pattern
	PropertyChangeSupport support = new PropertyChangeSupport(this);
	public void addObserver(PropertyChangeListener l) {
		support.addPropertyChangeListener(l);
	}


	private boolean adminLoggedIn;
	private Employee currentlyLoggedIn;
	private DateServer dateServer = new DateServer();
	private EmployeeRepository employeeRepository;
	private ProjectRepository projectRepository;


	public PlannerApp(EmployeeRepository employeeRepository, ProjectRepository projectRepository) { // This constructor should be used in implementation
		this.employeeRepository = employeeRepository;
		this.projectRepository = projectRepository;
	}

	public PlannerApp() { // This constructor is used for testing

	}


	// Getters
	public EmployeeRepository getEmployeeRepository() {
		return employeeRepository;
	}

	public ProjectRepository getProjectRepository() {
		return projectRepository;

	}


	// Setters
	public void setDateServer(DateServer dateServer) {
		this.dateServer = dateServer;
	}

	public void setRepositories(EmployeeRepository employeeRepository, ProjectRepository projectRepository) {
		this.employeeRepository = employeeRepository;
		this.projectRepository = projectRepository;
	}



	// Methods
	public void adminLogin() {
		adminLoggedIn = true;
	}

	public void adminLogout() {
		adminLoggedIn = false;
	}

	public boolean adminLoggedIn() {
		return adminLoggedIn;
	}

	public void appointProjectLeader(Project project, Employee employee) throws OperationNotAllowedException {
		assert adminLoggedIn() == true : "Pre: Admin is not logged in";
		assert getEmployeeFromId(employee.getId()) != null : "Pre: Employee does not exist";
		assert getProjectFromId(project.getId()) != null : "Pre: Project does not exist";
		checkAdministratorLoggedIn();
		if (!employeeExist(employee.getId())) {
			throw new OperationNotAllowedException("Employee does not exist!");
		}
		project.appointLeader(employee);
		assert project.getProjectLeader() == employee: "Post: Leader not added successfully";
	}

	public void employeeLogin(Employee employee) {
		currentlyLoggedIn = employee;		

	}

	public void employeeLogOut() {
		Employee oldEmployee = currentlyLoggedIn;
		currentlyLoggedIn = null;
		
		support.firePropertyChange("EMPLOYEE LOGOUT", oldEmployee, null);
		
	}
	
	public void changeActivityInterval(Activity activity, Interval newInterval) throws OperationNotAllowedException {
		Project parentProject = activity.getParentProject();

		if (parentProject == null) { // the activity does not belong to a project
			checkAdministratorLoggedIn();
		} else {
			if (!adminLoggedIn) {
				boolean projectLeaderLoggedIn = currentlyLoggedIn.equals(parentProject.getProjectLeader());
				if(!projectLeaderLoggedIn) {
					throw new OperationNotAllowedException("Only the project leader can alter the activity!");
				}
			}

		}

		activity.setRunningTime(newInterval);
	}

	private void checkAdministratorLoggedIn() throws OperationNotAllowedException {
		if (!adminLoggedIn()) {
			throw new OperationNotAllowedException("Administrator login required");
		}
	}


	public ArrayList<Employee> checkForAvailableWorkers() {
		ArrayList<Employee> availableWokers =
				(ArrayList<Employee>) employeeRepository.getAllEmployeesStream()
														.filter(employee -> employee.isAvailable())
														.collect(Collectors.toList());

		return availableWokers;
	}

	public void clearDatabase() {
		employeeRepository.clearEmployeeDatabase();
		projectRepository.clearProjectDatabase();
		support.firePropertyChange("ALL", null, null);
	}

	public void deleteProject(Project p) throws OperationNotAllowedException {
		checkAdministratorLoggedIn();
		projectRepository.deleteProject(p);
		support.firePropertyChange("DELETE PROJECT", p, null);
	}

	public void deleteEmployee(Employee e) throws OperationNotAllowedException {
		this.checkAdministratorLoggedIn();
		this.employeeRepository.deleteEmployee(e);
		support.firePropertyChange("DELETE EMPLOYEE", e, null);

	}

	private boolean employeeExist(String employeeId) {
		return (employeeRepository.getEmployeeFromId(employeeId) != null);
	}

	public ArrayList getAllActivitesForCurrentUser() {
		ArrayList<Activity> activities = new ArrayList<Activity>();

		// TODO : add activities from planner

		for (Project p : getProjectRepository().getAllProjectsList()) {
			p.getActivities().stream()
				.filter( a -> a.isEmployeeAssignedToActivity(getCurrentUser()) ||
							  a.isEmployeeHelpingOnActivity(getCurrentUser()))
				.forEach(activities::add);
		}

		return activities;
	}

	public Employee getCurrentUser() {
		return currentlyLoggedIn;
	}

	public String getCurrentUserId() {
		return currentlyLoggedIn.getId();
	}

	public Employee getEmployeeFromId(String employeeId) {
		return employeeRepository.getEmployeeFromId(employeeId);
	}

	public Project getProjectFromId(String projectId) {
		return projectRepository.getProjectFromId(projectId);
	}

	public ArrayList<Project> getProjectsFromName(String name) {
		return projectRepository.getProjectsFromName(name);
	}

	public Project newProject() throws OperationNotAllowedException {
		return newProject(null);
	}

	public Project newProject(String projectName) throws OperationNotAllowedException {
		checkAdministratorLoggedIn();
		Project p = new Project(projectName, dateServer.getDate(), projectRepository.getNumberOfProjects());

		projectRepository.addProject(p);
		support.firePropertyChange("NEW PROJECT", null, p);

		return p;
	}

	public Employee registerEmployee(String id) throws OperationNotAllowedException {
		assert adminLoggedIn() == true : "Pre: administrator is not logged in";
		assert getEmployeeFromId(id) == null : "Pre: employee already exist";
		assert id.length() < 5: "Pre: Id is too long";
		assert id.replace(" ", "").length() > 0 : "Pre: Id is empty";
		checkAdministratorLoggedIn();

		if (employeeExist(id)) {
			throw new OperationNotAllowedException("The employee is already registered");
		}

		Employee e = new Employee(id);
		employeeRepository.addEmployee(e);
		support.firePropertyChange("NEW EMPLOYEE", null, e);
		assert getEmployeeFromId(id) != null : "Post: Employee not registered successfully";
		return e;
	}

	public void assignEmployeeToActivity(Employee employee, Activity activity) throws OperationNotAllowedException {
		Project parent = activity.getParentProject();

		if (parent == null) { // the activity belongs to the planner
			checkAdministratorLoggedIn();
		} else {	//
			if (!adminLoggedIn) {
				boolean projectLeaderLoggedIn = currentlyLoggedIn.equals(parent.getProjectLeader());
				if(!projectLeaderLoggedIn) {
					throw new OperationNotAllowedException("Only the project leader can assign employees to activites!");
				}
			}
		}

		activity.assignEmployee(employee);
	}


	public void assignHelperToActivity(Employee helper, Activity activity) throws OperationNotAllowedException {
		activity.assignHelper(currentlyLoggedIn, helper);
	}

}
