package app;

import java.util.ArrayList;
import java.util.stream.Stream;

import domain.Employee;

public interface EmployeeRepository {
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */
	
	void addEmployee(Employee e);
	
	void clearEmployeeDatabase();
	
	void deleteEmployee(Employee employee);

	Object[] getAllEmployeesArray();

	ArrayList<Employee> getAllEmployeesList();
	
	Stream<Employee> getAllEmployeesStream();
	
	Employee getEmployeeFromId(String employeeId);

	int getNumberOfEmployees();
	

}
