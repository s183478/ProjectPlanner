package app;

import java.util.Calendar;

public class DateTime {
	
	private int dayOfWeek;
	private int hour;
	private int minute;
	private int week;
	private int year;
	
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */
	private DateTime() { };
	
	// Implement builder pattern by using a builder class
	public static class Builder {
		
		// A value of -1 means it is not initialized
		private int year = -1;
		private int week = -1;
		private int dayOfWeek = -1;
		private int hour = -1;
		private int minute = -1;
		
		public Builder() { };
		
		public Builder year(int year) {
			this.year = year;
			return this;
		}
		
		public Builder week(int week) {
			this.week = week;
			return this;
		}
		
		public Builder dayOfWeek(int dayOfWeek) {
			this.dayOfWeek = dayOfWeek;
			return this;
		}
		
		public Builder hour(int hour) {
			this.hour = hour;
			return this;
		}
		
		public Builder minute(int minute) {
			this.minute = minute;
			return this;
		}
		
		public DateTime build() throws OperationNotAllowedException {
			Calendar c = Calendar.getInstance();
			c.setFirstDayOfWeek(Calendar.MONDAY);
			
			DateTime dateTime = new DateTime();
			
			// If field is uninitialized, set it to current calendar value
			dateTime.year = (this.year == -1) ? c.get(Calendar.YEAR) : this.year;
			dateTime.week = (this.week == -1) ? c.get(Calendar.WEEK_OF_YEAR) : this.week;
			dateTime.dayOfWeek = (this.dayOfWeek == -1) ? c.get(Calendar.DAY_OF_WEEK) : this.dayOfWeek;
			dateTime.hour = (this.hour == -1) ? c.get(Calendar.HOUR_OF_DAY) : this.hour;
			dateTime.minute = (this.minute == -1) ? c.get(Calendar.MINUTE) : this.minute;
			
			checkValidDateTime(dateTime);
			return dateTime;
		}
		
	}
	
	
	
	// Getters
	public int getDayOfWeek() {
		return dayOfWeek;
	}
	
	public static void checkValidDateTime(DateTime dateTime) throws OperationNotAllowedException {
		String errorMessage = null;
		
		if (dateTime.getWeek() < 1 || dateTime.getWeek() > 52) {
			errorMessage = "The week number " + dateTime.getWeek() + " is invalid.";
		} else if (dateTime.getDayOfWeek() < 1 || dateTime.getDayOfWeek() > 7) {
			errorMessage = "The day of week " + dateTime.getDayOfWeek() + " is invalid.";
		} else if (dateTime.getHour() < 0 || dateTime.getHour() > 23) {
			errorMessage = "The hour " + dateTime.getHour() + " is invalid";
		} else if (dateTime.getMinute() < 0 || dateTime.getMinute() > 59) {
			errorMessage = "The minute " + dateTime.getMinute() + " is invalid.";
		}
		
		if (errorMessage != null) {
			throw new OperationNotAllowedException(errorMessage);
		}
	}

	public int getHour() {
		return hour;
	}
	
	public int getMinute() {
		return minute;
	}

	public int getWeek() {
		return week;
	}

	public int getYear() {
		return year;
	}
	
	
	// Setters
	public void setDayOfWeek(int dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}
	
	public void setHour(int hour) {
		this.hour = hour;
	}
	
	public void setMinute(int minute) {
		this.minute = minute;
	}
	
	public void setWeek(int week) {
		this.week = week;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	
	// Methods
	public boolean equals(DateTime date) {
		if (
			this.year == date.getYear() &&
			this.week == date.getWeek() &&
			this.dayOfWeek == date.getDayOfWeek() &&
			this.hour == date.getHour() &&
			this.minute == date.getMinute()
		   ) 
		{
			return true;
		}
		
		return false;
	}

	public boolean isLargerThanOrEqualTo(DateTime date) {
		return ( !this.isSmallerThanOrEqualTo(date) || this.equals(date) );
	}
	
	public boolean isSmallerThanOrEqualTo(DateTime date) {
		if (date.getYear() < this.year) {
			return false;
		} else if (date.getYear() > this.year) {
			return true;
		} else if (date.getWeek() < this.week) {           // the years are equal
			return false;
		} else if (date.getWeek() > this.week) {
			return true;
		} else if (date.getDayOfWeek() < this.dayOfWeek) { // the weeks are equal
			return false;
		} else if (date.getDayOfWeek() > this.dayOfWeek) {
			return true;
		} else if (date.getHour() < this.hour) { 		   // the days are equal
			return false;
		} else if (date.getHour() > this.hour) {
			return true;
		} else if (date.getMinute() < this.minute) {	   // the hours are equal
			return false;
		} else if (date.getMinute() > this.minute) {
			return true;
		}
		
		// the times are equal
		return true;
	}
	public String toString() {
		String dateAsString = "";
		
		if (year != -1) {
			dateAsString += "| " + year + " | ";
		}
		if (week != -1) {
			dateAsString += "week " + String.format("%02d", week) + " | ";
		}
		if (dayOfWeek != -1) {
			dateAsString += String.format("%-9s", determineWeekday()) + " | ";
		}
		if (hour != -1) {
			dateAsString += String.format("%02d", hour) + ":";
		}
		if (minute != -1) {
			dateAsString += String.format("%02d", minute) + " |";
		}
		return dateAsString;
	}


	private String determineWeekday() {
		String[] weekdays = {null, "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
		return weekdays[dayOfWeek];
	}
	
}
