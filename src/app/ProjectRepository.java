package app;

import java.util.ArrayList;
import java.util.stream.Stream;

import domain.Project;

public interface ProjectRepository {
	/*
	 * Written by:
	 * s173906, Jonas Weile
	 */
	
	void addProject(Project project);
	
	void clearProjectDatabase();
	
	void deleteProject(Project project);
	
	Object[] getAllProjectsArray();

	ArrayList<Project> getAllProjectsList();

	Stream<Project> getAllProjectsStream();
	
	int getNumberOfProjects();
	
	Project getProjectFromId(String projectId);

	ArrayList<Project> getProjectsFromName(String name);
}
