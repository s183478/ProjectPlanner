Feature: Register time
	Description: An employee registers the time spent on different activities
	Actors: Employee
	# Written by:
    # Oscar Jørgensen s173470
    # And 
    # Jonas Weile s173906

# Main scenario
Scenario: an employee registers time spent on activities
	Given the employee "NAWE" is assigned to an activity
	When the user registers time spent between 8 and 10 on that activity on day 1 of week 20 of year 2020
	Then the user is registered as having worked between 8 and 10 on that activity on the given date

Scenario: A employee registers time spent on an activity to which he/she was asked to assist with:
	Given employee "HELP" is helping "NAWE" on an activity
	When the user registers time spent between 6 and 12 on that activity on day 1 of week 20 of year 2020
	Then the user is registered as having worked between 6 and 12 on that activity on the given date

Scenario: an employee changes registered time
	Given the employee "NAWE" is assigned to an activity
	And the user registers time spent between 8 and 10 on that activity on day 1 of week 20 of year 2020
	When the user alters the registered time spent to between 9 and 11 on that day
	Then the user is registered as having worked between 9 and 11 on that activity on the given date

# Fail scenarios
Scenario: An employee tries to register time in an already registered interval
  Given the employee "NAWE" is assigned to an activity
	And the user registers time spent between 8 and 10 on that activity on day 1 of week 20 of year 2020
	When the user registers time spent between 9 and 11 on another activity on the same day
	Then the error message "You have already registered time spent on another activity during this timeslot." is given

Scenario: An employee tries to register time on activities which she is not assigned to
  Given the employee "EM" is not assigned to an activity
	When the user registers time spent between 8 and 10 on that activity on day 1 of week 20 of year 2020
  Then the error message "You cannot register time on an activity which you are not registered to" is given

Scenario: An employee tries to register an interval of over 12 hours
	Given the employee "NAWE" is assigned to an activity
	When the user registers time spent between 6 and 20 on that activity on day 1 of week 20 of year 2020
	Then the error message "You cannot register more than 12 hours at once. Please split up the registration in smaller parts." is given


Scenario: An employee tries to register a non existing interval
	Given the employee "NAWE" is assigned to an activity
	When the user registers time spent between 6 and 28 on that activity on day 1 of week 20 of year 2020
	Then the error message "The hour 28 is invalid" is given
