Feature: Create Project
  Description: A new project is created in the planner
  Actors: Administrator
  # Written by:
  #
  #

# Main scenarios
Scenario: The administrator creates a new Project
  Given the administrator is logged in
  And the date is 03-04-2020
  When the user creates a new Project with name "EFI 2.0"
  Then a new project with name "EFI 2.0" and id "042001" is created


Scenario: The administrator creates a new Project with no name
  Given the administrator is logged in
  And the date is 03-04-2020
  When the user creates a new Project with no name
  Then a new project with id "042001" is created

# Fail scenario
Scenario: The administrator is not logged in
  Given the administrator is not logged in
  When the user creates a new Project with name "EFI 2.0"
  Then the error message "Administrator login required" is given
