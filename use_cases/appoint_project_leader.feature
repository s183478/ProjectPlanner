Feature: Appoint Project Leader
  Description: The administrator appoints a project leader
  Actor: Administrator
  # Written by:
  #
  #
  
# Main scenario
Scenario: Appoint project Leader
  Given that a project exists
  And an employee with id "NAWE" is already registered
  And the administrator is logged in
  When the user appoints the employee with id "NAWE" as project leader
  Then the employee with id "NAWE" is appointed as project leader

#Fail scenarios
Scenario: Employee does not exist
  Given that a project exists
  And an employee with id "NAWE" is not registered
  And the administrator is logged in
  When the user appoints the employee with id "NAWE" as project leader
  Then the error message "Employee does not exist!" is given


Scenario: The administrator is not logged in
  Given that a project exists
  And the administrator is not logged in
  And an employee with id "NAWE" is already registered
  When the user appoints the employee with id "NAWE" as project leader
  Then the error message "Administrator login required" is given
