Feature: Register employee
  Description: Add an employee to the planner application
  Actors: administrator
  # Written by:
  # Jonas Weile, s173906
  #

# Main scenario
Scenario: Register an employee
  Given the administrator is logged in
  When the user registers an employee with id "NAWE"
  Then the employee with id "NAWE" is registered


# Fail Scenarios
Scenario: The administrator is not logged in
  Given the administrator is not logged in
  When the user registers an employee with id "NAWE"
  Then the error message "Administrator login required" is given


Scenario: The employee is already registered
  Given an employee with id "NAWE" is already registered
  And the administrator is logged in
  When the user registers an employee with id "NAWE"
  Then the error message "The employee is already registered" is given


Scenario: Register an employee with empty id
  Given the administrator is logged in
  When the user registers an employee with id "   "
  Then the error message "The employee id is not valid" is given


Scenario: Register an employee with too long an id
  Given the administrator is logged in
  When the user registers an employee with id "JONAS WEILE"
  Then the error message "The employee id is not valid" is given
