Feature: Check available employees
		Description: The project leader checks for any available workers
		Actors: Project Leader
		# Written by:
  		# L.Noer s155490
	    #

	Scenario: There are employees that can be added
		Given that the list of employees is not empty
		And some are available
		When the project leader "alic" checks for available workers
		Then a list of available employees is returned
