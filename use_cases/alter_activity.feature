Feature: Alter activity
  Description: Alter an already created activity
  Actors: Project leader and administrator
  # Written by:
  # Jonas Weile, s173906
  #

Scenario: Change allocated hours
  Given a project with name "Exam Project" and id "042001" is registered in the planner
  And that the project with id "042001" has a registered activity with name "Write report"
  And that an employee "NAWE" who is also a project leader for the project with id "042001" is logged in
  And that the allocated hours for the activity "Write report" in project with id "042001" is 12 hours
  When the user updates the allocated hours for the activity to 50 hours
  Then the allocated hours for the activity is updated to 50 hours


Scenario: Change start- and end date
  Given a project with name "Exam Project" and id "042001" is registered in the planner
  And that the project with id "042001" has a registered activity with name "Write report"
  And that an employee "NAWE" who is also a project leader for the project with id "042001" is logged in
  And that the start and end week for the activity "Write report" in project with id "042001" is 20 and 24
  When the user updates the start and end week to 31 and 38
  Then the start and end week are updated to 31 and 38


# Fail Scenario 1
Scenario: Expected hours is negative
  Given a project with name "Exam Project" and id "042001" is registered in the planner
  And that the project with id "042001" has a registered activity with name "Write report"
  And that an employee "NAWE" who is also a project leader for the project with id "042001" is logged in
  And that the allocated hours for the activity "Write report" in project with id "042001" is 12 hours
  When the user updates the allocated hours for the activity to -10 hours
  Then the error message "The expected time cannot be negative!" is given

# Fail Scenario 2
Scenario: Start date is smaller than the end date
  Given a project with name "Exam Project" and id "042001" is registered in the planner
  And that the project with id "042001" has a registered activity with name "Write report"
  And that an employee "NAWE" who is also a project leader for the project with id "042001" is logged in
  And that the start and end week for the activity "Write report" in project with id "042001" is 20 and 24
  When the user updates the start and end week to 38 and 31
  Then the error message "The given start time is larger than the end time!" is given


# Fail Scenario
Scenario: User is not project leader
  Given a project with name "Exam Project" and id "042001" is registered in the planner
  And that the project with id "042001" has a registered activity with name "Write report"
  And that an employee "NAWE" who is not the project leader for the project with id "042001" is logged in
  And that the start and end week for the activity "Write report" in project with id "042001" is 20 and 24
  When the user updates the start and end week to 31 and 32
  Then the error message "Only the project leader can alter the activity!" is given
