Feature: Add an activity to a project
  Description:The project leader adds an activity to the project
  Actors:Project Leader
  # Written by:
  # s190187, Mads Kring Jakobsen
  #

# Main scenario
Scenario: An activity is added
  Given that there is a project with an assigned project leader
  When the project leader adds an activity with name "test"
  Then the activity is added to the project

 Scenario: An existing activity is added
  Given that there is a project with an assigned project leader
  And there is an activity called "test"
  When the project leader adds an activity with name "test"
  Then the error message "An activity with the same name already exists in this project" is given
