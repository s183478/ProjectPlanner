Feature: Create Time Report
	Description: The project leader generates a report showing the time spent on each activity for the project
	Actors: ProjectLeader
	# Written by:
    # L.Noer s155490
    #

Scenario: Generating a report for a project
 	Given a project with id "id" and name "bob" exists with a list of activities
 	When the project leader generates a report
 	Then a report containing total time spent on the project is returned.
 		