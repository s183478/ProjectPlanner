Feature: Add an employee to an activity
  Description: The administrator/project Leader adds an employee to an activity
	Actors: Administrator/project leader
	# Written by:
    # Oscar J�rgensen s173470
    # Jonas Weile, s173906

Scenario: The project leader adds an employee to an activity
	Given that a project with name "Test" and activity exist
	And that an employee "JON" who is also a project leader is logged in
	And there is an employee with ID "ABE"
	When the user adds the employee to an activity
	Then the employee with ID "ABE" is added to the activity

Scenario: An employee ask for help on an activity
	Given that a project with name "Test" and activity exist
	And an employee "2073" is already added to an activity
	When the employee adds another employee with ID "1000" as a helper to an activity
	Then the employee with ID "1000" is helping on the activity

Scenario: A regular employee tries to add another employee to an activity
	Given that a project with name "Test" and activity exist
	And that neither the administrator nor the relevant project leader is logged in
	And there is an employee with ID "2073"
	When the user adds the employee to an activity
	Then the error message "Only the project leader can assign employees to activites!" is given

Scenario: An employee is added to an activity twice
	Given that a project with name "Test" and activity exist
	And an employee "2073" is already added to an activity
	And that the project leader is logged in
	When the user adds the employee to an activity
	Then the error message "Employee is already assigned to this activity" is given

Scenario: an employee ask for help from someone already helping
	Given that a project with name "Test" and activity exist
	And there is an employee with ID "2073"
	And an employee with ID "1000" is already helping on activity
	When the employee is added as a helper again
	Then the error message "Employee is already helping on this activity" is given

Scenario: an employee who is not on the activity ask for help
	Given that a project with name "Test" and activity exist
	And there is an employee with ID "2073"
	When the employee adds another employee with ID "1000" as a helper to an activity
	Then the error message "You are not assigned to this activity, and thus cannot add helpers" is given                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
